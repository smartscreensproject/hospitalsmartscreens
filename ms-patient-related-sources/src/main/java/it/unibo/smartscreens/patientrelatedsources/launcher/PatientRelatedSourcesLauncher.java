package it.unibo.smartscreens.patientrelatedsources.launcher;

import io.vertx.core.Vertx;
import it.unibo.smartscreens.patientrelatedsources.verticle.PatientRelatedSourcesVerticle;

public class PatientRelatedSourcesLauncher {

    public static void main(String[] args) {

        Vertx vertx = Vertx.vertx();

        // Example of a verticle from the same module
        PatientRelatedSourcesVerticle patientRelatedSourcesVerticle = new PatientRelatedSourcesVerticle();
        vertx.deployVerticle(patientRelatedSourcesVerticle, (handler) -> {
            if (handler.succeeded()) {
                System.out.println("The verticle has been successfully deployed, with id: " + handler.result());
            } else {
                System.out.println("Error during deployment: " + handler.cause().getMessage());
            }
        });
    }
}
