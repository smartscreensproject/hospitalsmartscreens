package it.unibo.smartscreens.patientrelatedsources.verticle;

import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import it.unibo.smartscreens.common.model.PatientSources;
import it.unibo.smartscreens.common.constant.NameService;
import it.unibo.smartscreens.common.model.registry.MicroServiceEntity;
import it.unibo.smartscreens.common.msroute.PatientRelatedSourcesRoute;
import it.unibo.smartscreens.common.verticles.BaseVerticle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class PatientRelatedSourcesVerticle extends BaseVerticle {

    private static final String MONGODB_CONFIG = "/mongodb.json";
    private static final String PATIENTS_COLLECTION = "patientsources";
    private MicroServiceEntity patientReletedSource;
    private MongoClient mongoClient;


    @Override
    public void start(Future<Void> startFuture) throws Exception {

        vertx.executeBlocking( future -> {
            try (BufferedReader mongoConfigInputStream = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream(MONGODB_CONFIG), StandardCharsets.UTF_8))) {

                JsonObject mongoConfig = new JsonObject(mongoConfigInputStream.lines().reduce(String::concat).orElse(""));
                future.complete(mongoConfig);
            } catch (Exception e) {
                future.fail(e.getCause());
            }
        }, result -> {
            if (result.succeeded()) {
                mongoClient = MongoClient.createShared(vertx, (JsonObject) result.result());

                Router router = initRouter(PatientRelatedSourcesRoute.API_ROOT);

                router.get(PatientRelatedSourcesRoute.API_SOURCES + "/:patientId").handler(this::handleGetSources);
                router.post(PatientRelatedSourcesRoute.API_SOURCES + "/:patientId").handler(this::handleAddSources);
                router.delete(PatientRelatedSourcesRoute.API_SOURCES + "/:patientId").handler(this::handleRemoveSources);

                vertx.createHttpServer()
                    .requestHandler(router)
                    .listen(5001, res -> {
                        if (res.succeeded()) {
                            println("Verticle deployed successfully on localhost:" + res.result().actualPort());
                            subscribeToRegister(res.result().actualPort()).setHandler(subscription -> {
                                if (subscription.succeeded()) {
                                    startFuture.complete();
                                } else {
                                    startFuture.fail(subscription.cause());
                                }
                            });
                        } else {
                            println("Error in http server: " + res.cause().getMessage());
                            startFuture.fail(res.cause());
                        }
                    });
            } else {
                startFuture.fail(result.cause());
            }
        });

    }

    private Future<Void> subscribeToRegister(int port) {
        Future<Void> future = Future.future();
        getRegistryApiClient().postServices(port, NameService.PATIENT_RELATED_SOURCE, "foo bar foo")
            .setHandler(res -> {
                if (res.succeeded()) {
                    patientReletedSource = res.result();
                    println(Json.encodePrettily(patientReletedSource));
                    future.complete();
                } else {
                    println(res.cause().getMessage());
                    future.fail(res.cause());
                }
            });
        return future;
    }

    private Future<PatientSources> getPatient(String patientId) {
        Future<PatientSources> future = Future.future();
        JsonObject query = new JsonObject()
            .put("patientId", patientId);
        this.mongoClient.findOne(PATIENTS_COLLECTION, query, null, res -> {
            if (res.succeeded()) {
                PatientSources patientSources = (res.result() != null) ? res.result().mapTo(PatientSources.class) : null;
                future.complete(patientSources);
            } else {
                future.fail(res.cause());
            }
        });
        return future;
    }

    private void getPatientAndResponse(String patientId, RoutingContext rc) {
        getPatient(patientId).setHandler(res -> {
            if (res.succeeded()) {
                if(res.result() != null) {
                    rc.response().end(Json.encode(res.result()));
                } else {
                    rc.response().setStatusCode(404).setStatusMessage("patient not found").end();
                }
            } else {
                rc.response().setStatusCode(500).setStatusMessage(res.cause().getMessage()).end();
            }
        });
    }

    private void handleGetSources(RoutingContext rc) {
        String patientId = rc.request().getParam("patientId");
        if (patientId == null || patientId.isEmpty()) {
            rc.response()
                .setStatusCode(400)
                .setStatusMessage("missing path params patientId").end();
        }

        getPatientAndResponse(patientId, rc);
    }

    private void handleAddSources(RoutingContext rc) {
        JsonObject body = rc.getBodyAsJson();
        String patientId = rc.request().getParam("patientId");
        String source = body.getString("source");
        if(patientId == null || patientId.isEmpty() || source == null || source.isEmpty()){
            rc.response()
                .setStatusCode(400)
                .setStatusMessage("malformed body").end();
        }

        getPatient(patientId).setHandler(res -> {
            if (res.succeeded()) {
                PatientSources patientSources;
                if (res.result() == null ) {
                    //insert
                     patientSources = new PatientSources(patientId);
                     patientSources.getSources().add(source);
                     mongoClient.save(PATIENTS_COLLECTION, JsonObject.mapFrom(patientSources), resS -> {
                         if (resS.succeeded()) {
                             getPatientAndResponse(patientId, rc);
                         } else {
                             rc.response().setStatusCode(500).setStatusMessage(resS.cause().getMessage());
                         }
                     });
                } else {
                    //replace
                    patientSources = res.result();
                    patientSources.getSources().add(source);
                    JsonObject query = new JsonObject().put("patientId", patientId);
                    mongoClient.replaceDocuments(PATIENTS_COLLECTION, query, JsonObject.mapFrom(patientSources), resR -> {
                        if (resR.succeeded()) {
                            getPatientAndResponse(patientId, rc);
                        } else {
                            rc.response().setStatusCode(500).setStatusMessage(resR.cause().getMessage());
                        }
                    });
                }
            }
        });
    }

    private void handleRemoveSources(RoutingContext rc) {
        JsonObject body = rc.getBodyAsJson();
        String patientId = rc.request().getParam("patientId");
        String source = body.getString("source");
        if(patientId == null || patientId.isEmpty() || source == null || source.isEmpty()){
            rc.response()
                .setStatusCode(400)
                .setStatusMessage("malformed body").end();
        }

        JsonObject query = new JsonObject().put("patientId", patientId);
        JsonObject operator = new JsonObject().put("$pull",  new JsonObject().put("sources",  source));

        mongoClient.updateCollection(PATIENTS_COLLECTION, query, operator, res -> {
           if (res.succeeded()) {
               getPatientAndResponse(patientId, rc);
           } else {
               rc.response().setStatusCode(500).setStatusMessage("impossible modify patient").end();
           }
        });
    }
}
