@echo off 
:: Cd to script current directory
cd /D "%~dp0"

set var=%1
:: echo %var%
:: echo %var:~1,-1%

:: Runs the command in a new terminal 
start cmd.exe @cmd /k "%var:~1,-1%"
