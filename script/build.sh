#!/bin/bash

# current script directory
cd "$(dirname "$0")"
CURR_DIR=$PWD

# install hook dependencies
cd $CURR_DIR
cd '../mock/hook-mock'
npm install

# install input-mock dependencies
cd $CURR_DIR
cd '../mock/input-mock/'
npm install

# install vs-microservice-mock dependencies
cd $CURR_DIR
cd '../mock/vs-microservice-mock/'
npm install

# install ws-mock dependencies
cd $CURR_DIR
cd '../mock/ws-mock/'
npm install


cd $CURR_DIR
cd ..
# kotlin has problems, running the builds singularly should make everything work smoothely
./gradlew :ms-vital-signs:build
./gradlew :ms-video-streams:build
./gradlew build
