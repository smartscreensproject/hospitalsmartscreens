#!/bin/bash

# cd to the current script directory
cd "$(dirname "$0")"

KERNELNAME="$(uname -s)"

EXECBASH="; exec bash"
WAITENTER="; read" #"; read -p 'Press enter to continue' x"


case "${KERNELNAME}" in
    Linux*)     machine=Linux
        if [ -f $(which gnome-terminal) ]
        then
            gnome-terminal -- bash -c "$1 $EXECBASH"
        else
            x-terminal-emulator -e "$1 $EXECBASH"
        fi
        ;;
    Darwin*)    machine=Mac
        osascript -e "tell application \"Terminal\" to do script \"cd $PWD; clear; $1 $EXECBASH\""
        ;;
    #CYGWIN*)    machine=Cygwin;;
    #MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${KERNELNAME}"
esac
