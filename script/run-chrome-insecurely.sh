#!/bin/bash

KERNELNAME="$(uname -s)"
chrome=""

case "${KERNELNAME}" in
    Linux*)     machine=Linux
        chrome=google-chrome
        ;;
    Darwin*)    machine=Mac
        cd '/Applications/Google Chrome.app/Contents/MacOS'
        chrome=./"Google Chrome"
        ;;
    *)          machine="UNKNOWN:${KERNELNAME}"
esac

"$chrome" --user-data-dir=$HOME'/Desktop/chrome-no-web-security/' --disable-web-security &>/dev/null &

echo "running chrome with disabled security"

read -p "Press any key to quit... " -n1 -s
