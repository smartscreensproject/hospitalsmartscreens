@echo off

:: current script directory
cd /D "%~dp0"
set curr_dir=%cd%

:: install hook dependencies
cd %curr_dir%
cd "..\mock\hook-mock"
npm install

: install input-mock dependencies
cd %curr_dir%
cd "..\mock\input-mock"
npm install

: install vs-microservice-mock dependencies
cd %curr_dir%
cd "..\mock\vs-microservice-mock"
npm install

: install ws-mock dependencies
cd %curr_dir%
cd "..\mock\ws-mock"
npm install


cd %curr_dir%
cd ..
: kotlin has problems, running the builds singularly should make everything work smoothely
gradlew.bat :ms-vital-signs:build
gradlew.bat :ms-video-streams:build
gradlew.bat build
