let os = require('os');
let exec = require('child_process').exec;

// changes the script working directory to the script's location
process.chdir(__dirname);

let currentOs = os.platform();

function switchCommandForOs(windowsCmd, linuxCmd) {
    let command = '';
    switch (currentOs) {
        case 'win32':
            command = windowsCmd;
            break;
        case 'linux':
        case 'darwin':
            command = linuxCmd;
            break;
    }
    return command;
}

function execInNewTerminal(cmd) {
    let script = switchCommandForOs('exec-in-new-terminal.bat', './exec-in-new-terminal.sh');
    // wraps cmd between "" to make it interpreted as one command line argument
    let finalCmd = script + ' "' + cmd + '"';
    console.log(finalCmd);
    run(finalCmd);
}

function gradleCmd(cmd) {
    return switchCommandForOs('gradlew.bat', './gradlew') + ' ' + cmd;
}

function run(command) {
    exec(command, (error, stdout, stderr) => {
        console.log(`${stdout}`);
        console.log(`${stderr}`);
        if (error !== null) {
            console.log(`exec error: ${error}`);
        }
        let result = {
            'error': error,
            'stdout': stdout,
            'stderr': stderr
        };
        //console.log(result);
    });
}

function logInfoMessage(message) {
    console.log(`--- ${message} ---`);
}

// launching mocks
const mockDirName = 'mock';

logInfoMessage('launching vital sign microservice mock');
const vsMockDirName = 'vs-microservice-mock';
const vsWindowsCmd = `cd ..\\${mockDirName}\\${vsMockDirName} & launcher.bat`;
const vsLinuxCmd = `cd ../${mockDirName}/${vsMockDirName}; ./launcher.sh`;
execInNewTerminal(switchCommandForOs(vsWindowsCmd, vsLinuxCmd));

logInfoMessage('launching video streams service');
const videoStreamsDirName = 'video-streams-service';
const videoStreamsWindowsCmd = `cd ..\\${mockDirName}\\${videoStreamsDirName} & launcher.bat`;
const videoStreamsLinuxCmd = `cd ../${mockDirName}/${videoStreamsDirName}; ./launcher.sh`;
execInNewTerminal(switchCommandForOs(videoStreamsWindowsCmd, videoStreamsLinuxCmd));

// launching microservices
let windowsCmd = 'cd .. && ';
let linuxCmd = 'cd ..; ';
let precDirCmd = switchCommandForOs(windowsCmd, linuxCmd);

logInfoMessage('launching registry');
execInNewTerminal(precDirCmd + gradleCmd(':ms-registry:run'));

setTimeout(() => {
    logInfoMessage('launching smart screen');
    execInNewTerminal(precDirCmd + gradleCmd(':ms-smart-screen:run'));

    logInfoMessage('launching patient id resolver');
    execInNewTerminal(precDirCmd + gradleCmd(':ms-patient-id-resolver:run'));

    logInfoMessage('launching vital signs');
    execInNewTerminal(precDirCmd + gradleCmd(':ms-vital-signs:run'));

    logInfoMessage('launching video streams');
    execInNewTerminal(precDirCmd + gradleCmd(':ms-video-streams:run'));

    logInfoMessage('launching patient related sources');
    execInNewTerminal(precDirCmd + gradleCmd(':ms-patient-related-sources:run'));

    setTimeout(() => {
        logInfoMessage('launching input manager');
        execInNewTerminal(precDirCmd + gradleCmd(':ms-input-manager:run'));
    }, 10000);

    setTimeout(() => {
        logInfoMessage('launching web gui');
        //execInNewTerminal(precDirCmd + gradleCmd(':web-gui:run'));
        let webGuiDir = switchCommandForOs('cd ..\\web-gui & ', 'cd ../web-gui; ');
        execInNewTerminal(webGuiDir + 'npx ng serve -o --host 0.0.0.0 --poll=3000'); //--ssl=true --sslCert=cert/cert.pem --sslKey=cert/key.pem
    }, 6000);
}, 10000);
