package it.unibo.smartscreens.vitalsigns.source.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties("_id")
data class Monitor(@JsonProperty("id") var id: String,
                   @JsonProperty("ip_gateway") var gatewayHost: String?,
                   @JsonProperty("port_gateway") var gatewayPort: Int?,
                   @JsonProperty("patient_id") var patientId: String?)
