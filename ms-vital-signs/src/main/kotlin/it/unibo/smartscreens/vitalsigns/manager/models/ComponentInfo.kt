package it.unibo.smartscreens.vitalsigns.manager.models

data class ComponentInfo(var id: String, var inputManagerId: String)
