package it.unibo.smartscreens.vitalsigns.source

import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import io.vertx.core.Future
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServer
import io.vertx.core.http.ServerWebSocket
import io.vertx.core.json.JsonObject
import io.vertx.ext.mongo.FindOptions
import io.vertx.ext.mongo.MongoClient
import io.vertx.ext.mongo.MongoClientUpdateResult
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.LoggerHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.awaitResult
import it.unibo.smartscreens.common.model.ServiceAddress
import it.unibo.smartscreens.common.msroute.HeartBeatRoute
import it.unibo.smartscreens.sources.common.clients.RegistryClient
import it.unibo.smartscreens.sources.common.extensions.*
import it.unibo.smartscreens.sources.common.models.HookInfo
import it.unibo.smartscreens.sources.common.models.SubscriptionInfo
import it.unibo.smartscreens.sources.common.utils.ClientProvider
import it.unibo.smartscreens.sources.common.utils.ConfigReader
import it.unibo.smartscreens.sources.common.utils.Scheduler
import it.unibo.smartscreens.vitalsigns.source.models.Monitor
import it.unibo.smartscreens.vitalsigns.source.models.MonitorData
import it.unibo.smartscreens.vitalsigns.source.models.Patient
import it.unibo.smartscreens.vitalsigns.source.models.config.SourceServiceConfig
import it.unibo.smartscreens.vitalsigns.source.utils.DataFetcher
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.supervisorScope
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class VitalSignsSourceVerticle : CoroutineVerticle() {

    //constants
    companion object {
        //configuration file names
        private const val MONGODB_CONFIG = "source/mongodb.json"
        private const val SERVICE_CONFIG = "source/config.json"

        //path parameters
        private const val PATIENT_PARAM_KEY = "patientId"

        //query param
        private const val TREND_RANGE_PARAM_KEY = "range"

        //mongodb collections name
        private const val PATIENTS_COLLECTION = "patients"
        private const val VITAL_SIGNS_COLLECTION = "vitalsigns"
        private const val DEVICES_COLLECTION = "devices"
        private const val PATIENT_VITAL_SIGNS_COLLECTION = "patientvitalsigns"

        //mongodb collection fields
        private const val PATIENT_REF_KEY = "patientId"
        private const val DEVICE_REF_KEY = "deviceId"
        private const val TIMESTAMP_KEY = "timestamp"
        private const val DEVICE_ID_KEY = "id"
        private const val PATIENT_ID_KEY = "id"

        //mongodb extensions
        private const val GREATER_THAN_OR_EQUAL = "\$gte"

        //thresholds
        private const val TREND_RANGE_THRESHOLD = 60
    }

//region fields

    //config
    private lateinit var verticleConfig: SourceServiceConfig //the verticle configuration

    //clients
    private lateinit var registryClient: RegistryClient //registry client
    private lateinit var dataFetcher: DataFetcher //manages data fetching from the vital sign microservice
    private lateinit var mongoClient: MongoClient //handles mongodb connection and operations

    //websocket and endpoint subscribers
    private val subscribers = HashMap<String, MutableList<ServerWebSocket>>() //keep track of the subscribers via websocket
    private val hooks = HashMap<String, MutableList<String>>() //keep track of the subscribers via hooking
//endregion

//region main methods
    override suspend fun start() {
        //load mongodb config
        val mongoConfig = ConfigReader.readConfig(MONGODB_CONFIG)
        mongoClient = MongoClient.createShared(vertx, mongoConfig)

        //load service config
        verticleConfig = ConfigReader.readConfig(jacksonTypeRef(), SERVICE_CONFIG)

        //schedule data fetching
        dataFetcher = DataFetcher(verticleConfig.fetcherConfig)

        //the scheduler will execute a coroutine at each execution
        Scheduler.start(verticleConfig.schedulingTime) {
            runBlocking { //let spawn coroutine from a normal function (will block the current thread)
                supervisorScope { //will prevent the kill of all coroutine if an exception is thrown inside a child coroutine
                    try {
                        fetchData()
                    } catch (e: Throwable) { //all exception are handled to avoid to kill the parent coroutine
                        println("Error detected inside scheduled task:" + if (e.message != null) "\r\n" + e.message else "")
                        e.printStackTrace(System.err)
                    }
                }
            }
        }

        //create http server
        val http = awaitResult<HttpServer> {
            vertx
                .createHttpServer()
                .requestHandler(createRouter())
                .listen(verticleConfig.port, verticleConfig.host, it)
        }

        //subscribe to registry
        registryClient = RegistryClient()
        val result = registryClient.registerService(
            ServiceAddress(verticleConfig.host, http.actualPort()),
            verticleConfig.info.name, verticleConfig.info.descr).await()
        println("[VitalSignsSourceVerticle] Successfully registered to Registry (id: ${result.id})")

        println("[VitalSignsSourceVerticle] Started at: ${verticleConfig.host}:${http.actualPort()}")
    }

    override suspend fun stop() {
        subscribers.values.flatten().forEach { it.close() }
        Scheduler.stop()
        mongoClient.close()
        println("[VitalSignsSourceVerticle] Stopped.")
    }
//endregion

//region handlers
    private fun handleInfo(context: RoutingContext) {
        sendCollection(context, VITAL_SIGNS_COLLECTION)
    }

    private fun handleDevices(context: RoutingContext) {
        sendCollection(context, DEVICES_COLLECTION)
    }

    private fun handlePatients(context: RoutingContext) {
        sendCollection(context, PATIENTS_COLLECTION)
    }

    private suspend fun handleVitalSigns(context: RoutingContext) {
        val fetchTrend = context.queryParam(TREND_RANGE_PARAM_KEY)
        val patientId = context.pathParam(PATIENT_PARAM_KEY)

        if (!checkPatientExistence(patientId).await()) {
            context.response().notFound()
            return
        }

        if (fetchTrend.size > 0) {
            val rawRange = fetchTrend.first().toLong()
            if (rawRange <= TREND_RANGE_THRESHOLD) {
                val from = LocalDateTime.now().minusMinutes(rawRange)
                val trend = getPatientDataHistory(patientId, from).await()
                if (trend.isNotEmpty()) {
                    context.response().json(trend.map { JsonObject.mapFrom(it) })
                } else {
                    context.response().noContent()
                }
            } else {
                context.response().badRequest()
            }
        } else {
            val result = getPatientData(patientId).await()
            result?.let {
                context.response().json(JsonObject.mapFrom(result))
            } ?: context.response().noContent()
        }
    }

    private fun handleVitalSignsSubscribe(context: RoutingContext) {

        //reroute if http method is delete
        if (context.request().method() == HttpMethod.DELETE) {
            context.next()
            return
        }

        val patientId = context.pathParam(PATIENT_PARAM_KEY)
        checkPatientExistence(patientId).setHandler {
            if (it.success()) {
                if (it.result()) {
                    val ws = context.request().upgrade()
                    subscribers.getOrPut(patientId) { mutableListOf() }.add(ws)
                    println("[VitalSignsSourceVerticle] A new async subscription added for $patientId (connID: ${ws.textHandlerID()})")
                } else context.response().notFound()
            } else context.response().error(it.cause())
        }
    }

    private fun handleVitalSignsHooking(context: RoutingContext) {
        val patientId = context.pathParam(PATIENT_PARAM_KEY)
        checkPatientExistence(patientId).map {
            try {
                val hookInfo = context.bodyAsJson.mapTo(HookInfo::class.java)
                val item = hooks.getOrPut(patientId) { mutableListOf() }
                item.add(hookInfo.endpoint)
                context.response().success()
                println("[VitalSignsSourceVerticle] A new sync subscription added for $patientId (endpoint: ${hookInfo.endpoint})")
            } catch (e: Throwable) {
                context.response().badRequest()
            }
        } ?: context.response().notFound()
    }

    private fun handleVitalSignsUnsubscribe(context: RoutingContext) {
        val patientId = context.pathParam(PATIENT_PARAM_KEY)
        checkPatientExistence(patientId).map {
            try {
                val subscriptionInfo = context.bodyAsJson.mapTo(SubscriptionInfo::class.java)
                val item = subscribers.getValue(patientId)
                if (!item.removeIf { it.textHandlerID() == subscriptionInfo.connectionId }) {
                    context.response().error(Error("Unsubscription failed, the websocket id was not found"))
                } else {
                    context.response().success()
                    println("[VitalSignsSourceVerticle] Async subscription removed for $patientId (endpoint: ${subscriptionInfo.connectionId})")
                }
            } catch (e: Throwable) {
                context.response().badRequest()
            }
        } ?: context.response().notFound()
    }

    private fun handleVitalSignsUnhooking(context: RoutingContext) {
        val patientId = context.pathParam(PATIENT_PARAM_KEY)
        checkPatientExistence(patientId).map {
            try {
                val hookInfo = context.bodyAsJson.mapTo(HookInfo::class.java)
                val item = hooks.getValue(patientId)
                if (!item.remove(hookInfo.endpoint)) {
                    context.response().error(Error("Unhooking failed, the endpoint was not found"))
                } else {
                    context.response().success()
                    println("[VitalSignsSourceVerticle] Sync subscription removed for $patientId (endpoint: ${hookInfo.endpoint})")
                }
            } catch (e: Throwable) {
                context.response().badRequest()
            }
        } ?: context.response().notFound()
    }
//endregion

//region data fetching
    /**
     * Fetches data from the vital signs service, updates mongodb database and pushes data to the subscribers
     */
    private suspend fun fetchData() {

        println("[Fetcher] Fetching new data...")

        //region fetch new data
        //fetch new data [Monitor, MonitorData]
        val newMonitors = dataFetcher.getMonitors().await()
        val newData = newMonitors.map { Pair(it, dataFetcher.getMonitorData(it.id).await()) }
        //endregion

        println("[Fetcher] Fetching old data...")

        //region fetch old data
        //get old monitors [Monitor]
        val oldMonitors = awaitResult<List<JsonObject>> { mongoClient.find(DEVICES_COLLECTION, JsonObject(), it) }.map { it.mapTo(Monitor::class.java) }
        //get old patients
        val oldPatients = awaitResult<List<JsonObject>> { mongoClient.find(PATIENTS_COLLECTION, JsonObject(), it) }.map { it.mapTo(Patient::class.java) }
        //endregion

        println("[Fetcher] Inserting missing data...")

        //region insert missing data
        //find missing monitors
        val missingMonitors = newData.filter { i -> !oldMonitors.any { it.id == i.first.id } }.map { i -> i.first.patientId = i.second.patientId; i.first }
        //insert missing monitors
        missingMonitors.forEach {
            val result = awaitResult<String> { handler ->
                mongoClient.insert(DEVICES_COLLECTION, JsonObject.mapFrom(it), handler)
            }
            if (result.isEmpty()) {
                println("Missing monitors insert failed")
            }
        }

        //find missing patients
        val missingPatients = newData.filter { i -> !oldPatients.any {it.id == i.second.patientId } }
        //insert missing patients
        missingPatients.forEach { i ->
            val result = awaitResult<String> {
                mongoClient.insert(PATIENTS_COLLECTION, JsonObject.mapFrom(Patient(i.second.patientId, i.second.patientName)), it)
            }
            if (result.isEmpty()) {
                println("Missing patients insert failed")
            }
        }
        //endregion

        println("[Fetcher] Updating missing data...")

        //region update changed data
        //get changed monitors
        val changedMonitors = newData.filter { i ->
            !missingMonitors.any { it.id == i.first.id } && //if it's not a new one
                oldMonitors.any { it.id == i.first.id && it.patientId != i.second.patientId } //and the same monitor has a different patient_id
        }
        //update existing monitors
        changedMonitors.forEach { i ->
            val result = awaitResult<MongoClientUpdateResult> {
                mongoClient.updateCollection(DEVICES_COLLECTION, JsonObject().put(DEVICE_ID_KEY, i.first.id),
                    JsonObject().put(PATIENT_REF_KEY, i.second.patientId), it)
            }
            if (result.docModified.toInt() != changedMonitors.size) {
                println("Failed to update all monitors data (${result.docModified}/${changedMonitors.size})")
            }
        }
        //endregion

        println("[Fetcher] Adding new data...")

        //region add new data
        //add vital signs
        newData.forEach { i ->
            val result = awaitResult<String> {
                i.second.deviceId = i.first.id
                mongoClient.insert(PATIENT_VITAL_SIGNS_COLLECTION, JsonObject.mapFrom(i.second), it)
            }
            if (result.isEmpty()) {
                println("Patient vital signs insert failed")
            }
        }
        //endregion

        println("[Fetcher] Pushing data to subscribers...")

        //region push data
        //push data to all subscribers
        val removableSubscribers = mutableListOf<String>()
        subscribers.forEach { i ->
            newData.filter { it.second.patientId == i.key }.forEach {
                val message = JsonObject.mapFrom(it.second).put(DEVICE_REF_KEY, it.first.id).purgeId()
                i.value.forEach { ws ->
                    try {
                        ws.writeTextMessage(message.encode())
                    } catch (e: Exception) {
                        println("[Fetcher] Unable to send statusMessage to a subscriber via websocket: ${e.message}")
                        removableSubscribers.add(ws.textHandlerID())
                    }
                }
            }
        }

        //remove disconnected websockets
        subscribers.forEach {i -> i.value.removeIf { removableSubscribers.contains(it.textHandlerID()) } }

        //push data to all hooks
        val removableHooks = mutableListOf<String>()
        hooks.forEach { i ->
            newData.filter { it.second.patientId == i.key }.forEach {
                val message = JsonObject.mapFrom(it.second).put(DEVICE_REF_KEY, it.first.id).purgeId()
                i.value.forEach { hook ->
                    ClientProvider.webClient.postAbs(hook).sendJsonObject(message) { res ->
                        if (!res.success()) {
                            println("[Fetcher] Unable to send statusMessage to the subscribed hook: ${res.cause().message}")
                            removableHooks.add(hook)
                        }
                    }
                }
            }
        }

        //remove invalid hooks
        removableHooks.forEach { hooks.remove(it) }

        println("[Fetcher] All operations completed")
        //endregion
    }
//endregion

//region mongodb
    /**
     * Checks whether a patient id is correct or not.
     */
    private fun checkPatientExistence(patientId: String): Future<Boolean> {
        val future = Future.future<Boolean>()
        mongoClient.find(PATIENTS_COLLECTION, JsonObject().put(PATIENT_ID_KEY, patientId)) {
            if (it.success()) {
                future.complete(it.result().any())
            } else {
                future.fail(it.cause())
            }
        }
        return future
    }

    /**
     * Get last vital signs record for a specific patient.
     */
    private fun getPatientData(patientId: String): Future<MonitorData?> {
        val future = Future.future<MonitorData>()

        val options = FindOptions()
        options.limit = 1
        options.sort =  JsonObject().put(TIMESTAMP_KEY, -1)

        mongoClient.findWithOptions(PATIENT_VITAL_SIGNS_COLLECTION, JsonObject().put(PATIENT_REF_KEY, patientId), options) { res ->
            if (res.success()) {
                res.result().firstOrNull()?.let {
                    future.complete(it.mapTo(MonitorData::class.java))
                } ?: future.complete(null)
            } else {
                future.fail(res.cause())
            }
        }

        return future
    }

    /**
     * Get up to 300 last vital signs records for a specific patient.
     */
    private fun getPatientDataHistory(patientId: String, from: LocalDateTime): Future<List<MonitorData>> {
        val future = Future.future<List<MonitorData>>()

        val args = JsonObject()
            .put(PATIENT_REF_KEY, patientId)
            .put(TIMESTAMP_KEY, JsonObject()
                .put(GREATER_THAN_OR_EQUAL, DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(from)))

        val options = FindOptions()
        options.sort =  JsonObject().put(TIMESTAMP_KEY, 1)
        options.limit = 300

        mongoClient.findWithOptions(PATIENT_VITAL_SIGNS_COLLECTION, args, options) { res ->
            if (res.success()) {
                val results = res.result().map { it.mapTo(MonitorData::class.java) }
                future.complete(results)
            } else {
                future.fail(res.cause())
            }
        }

        return future
    }
//endregion

//region helpers
    /**
     * Creates the verticle http server router
     */
    private fun createRouter(): Router {
        val router = Router.router(vertx)
        router.route().handler(BodyHandler.create())
        router.route().handler(LoggerHandler.create())

        //default routes
        router.get(HeartBeatRoute.API_HEARTBEAT).handler { it.response().success() }

        router.get("${verticleConfig.baseApiUrl}/info").handler { handleInfo(it)}
        router.get("${verticleConfig.baseApiUrl}/devices").handler { handleDevices(it)}
        router.get("${verticleConfig.baseApiUrl}/patients").handler { handlePatients(it)}
        router.get("${verticleConfig.baseApiUrl}/patients/:$PATIENT_PARAM_KEY/vitalsigns").asyncHandler { handleVitalSigns(it)}

        //subscription routes
        router.route("${verticleConfig.baseApiUrl}/patients/:$PATIENT_PARAM_KEY/vitalsigns/subscribe").handler { handleVitalSignsSubscribe(it)}
        router.post("${verticleConfig.baseApiUrl}/patients/:$PATIENT_PARAM_KEY/vitalsigns/endpoint").handler { handleVitalSignsHooking(it)}
        router.delete("${verticleConfig.baseApiUrl}/patients/:$PATIENT_PARAM_KEY/vitalsigns/subscribe").handler { handleVitalSignsUnsubscribe(it)}
        router.delete("${verticleConfig.baseApiUrl}/patients/:$PATIENT_PARAM_KEY/vitalsigns/endpoint").handler { handleVitalSignsUnhooking(it)}

        return router
    }

    /**
     * Sends a mongodb collection as response in the input routing context
     */
    private fun sendCollection(context: RoutingContext, collectionName: String) {
        mongoClient.find(collectionName, JsonObject()) { res ->
            res.map {
                context.response().json( it.purgeIds() )
            } ?: context.response().error(res.cause())
        }
    }
//endregion
}
