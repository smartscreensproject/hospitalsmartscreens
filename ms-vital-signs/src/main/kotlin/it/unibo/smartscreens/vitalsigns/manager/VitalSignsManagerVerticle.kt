package it.unibo.smartscreens.vitalsigns.manager

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.convertValue
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.Future
import io.vertx.core.http.HttpServer
import io.vertx.core.http.WebSocket
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.LoggerHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.awaitResult
import io.vertx.kotlin.coroutines.dispatcher
import it.unibo.smartscreens.common.constant.NameService
import it.unibo.smartscreens.common.model.Action
import it.unibo.smartscreens.common.model.ActionParam
import it.unibo.smartscreens.common.model.ComponentInitParams
import it.unibo.smartscreens.common.model.ServiceRoute
import it.unibo.smartscreens.common.msroute.HeartBeatRoute
import it.unibo.smartscreens.common.verticles.BaseVerticle
import it.unibo.smartscreens.sources.common.clients.PatientResolverClient
import it.unibo.smartscreens.sources.common.clients.RegistryClient
import it.unibo.smartscreens.sources.common.extensions.*
import it.unibo.smartscreens.sources.common.models.UpdateDataContent
import it.unibo.smartscreens.sources.common.models.UpdateDataMessageFactory
import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.utils.ConfigReader
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper
import it.unibo.smartscreens.vitalsigns.common.models.VitalSignInfo
import it.unibo.smartscreens.vitalsigns.manager.models.ManagerSession
import it.unibo.smartscreens.vitalsigns.manager.models.ViewMode
import it.unibo.smartscreens.vitalsigns.manager.models.config.ManagerServiceConfig
import it.unibo.smartscreens.vitalsigns.manager.utils.SourceClient
import it.unibo.smartscreens.vitalsigns.source.models.MonitorData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.math.round

class VitalSignsManagerVerticle : CoroutineVerticle() {

    //constants
    companion object {
        //configuration file names
        private const val SERVICE_CONFIG = "manager/config.json"

        //default base api url
        private const val BASE_API_URL = "/api"

        //action params names
        private const val TREND_RANGE_PARAM_NAME = "trendRange"
        private const val SELECTED_VALUES_PARAM_NAME = "selectedValues"

        //patient id resolver code key
        private const val PATIENT_ID_RESOLVER_CODE_KEY = "pid"

        //update data viewer name
        private const val UPDATE_DATA_VIEWER_NAME = "vitalsigns-"

        private const val UPDATE_DATA_TREND_DATA_KEY = "trendData"
        private const val UPDATE_DATA_TREND_RECORDS_COUNT_KEY = "recordsCount"

        //the update time for vital signs data (minutes)
        private const val VITAL_SIGNS_UPDATE_TIME: Double = 5.0 / 60.0
    }

    //region fields

    //config
    private lateinit var verticleConfig: ManagerServiceConfig //the verticle configuration
    private lateinit var componentParams: ComponentInitParams //the service configuration

    //session
    private var currSession = ManagerSession()
    private var selectableVitalSigns: List<VitalSignInfo> = listOf() //the list of selectable vital signs

    //clients and connection
    private lateinit var inWebSocket: WebSocket //the websocket established with the source
    private lateinit var outWebSocket: WebSocket //the websocket established with the DT
    private lateinit var sourceClient: SourceClient //the http webClient to make requests to the source
    private lateinit var registryClient: RegistryClient //the webClient to contact the registry
    private lateinit var patientResolverClient: PatientResolverClient //the webclient to contact the patient id resolver
    //endregion

    //region main functions
    override suspend fun start() {

        //overwrite host and port if parent sent a configuration during deployment
        verticleConfig = if (!config.isEmpty) {
            if (config.containsKey(BaseVerticle.ADDRESS_REF) && config.containsKey(BaseVerticle.PORT_REF)) {
                ManagerServiceConfig(config.getString(BaseVerticle.ADDRESS_REF), config.getInteger(BaseVerticle.PORT_REF), BASE_API_URL)
            } else throw IllegalArgumentException("Input configuration is malformed or empty, unable to setup verticle")
        } else {
            //load verticle configuration from resources
            ConfigReader.readConfig(jacksonTypeRef(), SERVICE_CONFIG)
        }

        //initialize registry client
        try {
            registryClient = RegistryClient()
            //fetch vital signs source service info
            val sourceInfo = registryClient.getServicesByName(NameService.VITAL_SIGNS_SOURCE).await().first()
            //initialize source client
            sourceClient = SourceClient(ServiceConfig(sourceInfo.serviceAddress.address, sourceInfo.serviceAddress.port))
        } catch (e: Throwable) {
            throw IllegalStateException("Unable to retrieve vital signs source service information from registry", e)
        }

        //initialize patient id resolver client
        try {
            //fetch patient id resolver service info
            val resolverInfo = registryClient.getServicesByName(NameService.PATIENT_ID_RESOLVER).await().first()
            //initialize resolver client
            patientResolverClient = PatientResolverClient(ServiceConfig(resolverInfo.serviceAddress.address, resolverInfo.serviceAddress.port))
        } catch (e: Throwable) {
            throw IllegalStateException("Unable to retrieve patient id resolver service information from registry", e)
        }

        //start http server
        val http = awaitResult<HttpServer> { vertx
            .createHttpServer()
            .requestHandler(createRouter())
            .listen(verticleConfig.port, verticleConfig.host, it)
        }

        println("[VitalSignsManagerVerticle] Started at: ${verticleConfig.host}:${http.actualPort()}")
    }
    
    override suspend fun stop() {
        sourceClient.unsubscribe(componentParams.patientId, inWebSocket).await()
        inWebSocket.close()
        outWebSocket.close()
        println("[VitalSignsManagerVerticle] Stopped")
    }
    //endregion

    //region handlers
    private suspend fun handleInit(context: RoutingContext) {
        componentParams = context.bodyAsJson.mapTo(ComponentInitParams::class.java)

        //map input patient to vital signs pid
        val patientInfo = patientResolverClient.getPatientById(componentParams.patientId).await()
        patientInfo.codes[PATIENT_ID_RESOLVER_CODE_KEY]?.let { pid ->
            currSession.patientId = pid
        } ?: throw Error("Unable to retrieve patient information (pid code is not available")

        //establish a websocket with the DT
        outWebSocket = establishWebSocket(componentParams.updateDataEndpointAsync).await()

        //subscribe to data update for the parsed patient
        inWebSocket = sourceClient.subscribe(currSession.patientId).await()

        //listen for incoming statusMessage from the source and redirect them to the outWebSocket
        inWebSocket.textMessageHandler { GlobalScope.launch(vertx.dispatcher()) { handleMessage(it) } }

        //fetch selectableVitalSigns
        selectableVitalSigns = sourceClient.getVitalSignsInfo().await()

        //initialize the current selected values
        currSession.selectedValues = selectableVitalSigns.map { it.name }.toMutableList()

        //immediately send patient data to client (only if it's present)
        val patientData = sourceClient.getPatientData(currSession.patientId).await()
        patientData?.let { sendUpdateDataMessage(it) }

        //send the list of actions
        context.response().json(mapper.writeValueAsString(createActions()))
    }

    private fun handleDestroy(context: RoutingContext) {
        val stopFuture = Future.future<Void>()
        this.stop(stopFuture)
        if (stopFuture.success()) {
            context.response().success()
        } else {
            context.response().error(stopFuture.cause())
        }
    }

    private fun handleGetActions(context: RoutingContext) {
        context.response().json(mapper.writeValueAsString(createActions()))
    }

    private suspend fun handleShowInstant(context: RoutingContext) {
        currSession.viewMode = ViewMode.Instant

        //send instant data (only if it's present)
        val patientData = sourceClient.getPatientData(currSession.patientId).await()
        patientData?.let { sendUpdateDataMessage(it) }

        //reply with the new action
        context.response().json(mapper.writeValueAsString(createActions()))
    }

    private suspend fun handleShowTrend(context: RoutingContext) {
        currSession.viewMode = ViewMode.Trend

        //get trend range
        val trendRangeRaw = context.bodyAsJson.getString(TREND_RANGE_PARAM_NAME)
        val trend = sourceClient.getPatientDataTrend(currSession.patientId, trendRangeRaw).await()

        //cache current trend
        currSession.trendRange = trendRangeRaw.toInt()
        currSession.trendData = trend.toMutableList()

        //send history data
        sendUpdateDataMessage(createUpdateTrendDataMessage())

        //reply with the new action
        context.response().json(mapper.writeValueAsString(createActions()))
    }

    private fun handleSelectValues(context: RoutingContext) {
        try {
            val body = context.bodyAsJson
            if (!body.containsKey(SELECTED_VALUES_PARAM_NAME)) {
                context.response().badRequest()
                return
            }

            val rawParam = body.getString(SELECTED_VALUES_PARAM_NAME, "[]")
            val values = mapper.readValue<List<String>>(rawParam)

            //clear current trendData (the current data may be incompatible)
            currSession.trendData.clear()

            //update current values selection
            currSession.selectedValues.clear()
            currSession.selectedValues.addAll(values)

            //reply with the new action
            context.response().json(mapper.writeValueAsString(createActions()))

        } catch (e: Throwable) {
            context.response().error(e)
        }
    }
    //endregion

    //region actions
    /**
     * Creates possible actions relative to the current session state
     */
    private fun createActions(): List<Action> {
        val actions = ArrayList<Action>()

        //create action to select vital sign to show
        val vitalSigns = selectableVitalSigns.map { i -> i.name }.toMutableList()
        actions.add(Action("selectValues", "Choose vital signs", "Choose which vital signs to shown",
            "${verticleConfig.baseApiUrl}/actions/select-values", "post",
            listOf(ActionParam(SELECTED_VALUES_PARAM_NAME, "The vital signs to show", "string[]", currSession.selectedValues.getOrElse(0) { "" }, vitalSigns, true))))

        if (currSession.viewMode == ViewMode.Trend) {
            actions.add(Action("showInstant", "Change to instant view", "Show the instant value of the selected vital signs",
                "${verticleConfig.baseApiUrl}/actions/view-mode/instant", "post", null))
        } else {
            actions.add(Action("showTrend", "Change to trend view", "Show the trend of the selected vital signs as chart",
                "${verticleConfig.baseApiUrl}/actions/view-mode/trend", "post",
                listOf(ActionParam("trendRange","The range of the trend in minutes", "int", "10", true))))
        }

        return actions
    }
    //endregion

    //region websocket
    /**
     * Handles the incoming statusMessage from the input websocket
     */
    private fun handleMessage(message: String) {
        try {
            //deserialize statusMessage string into a monitor data pojo
            val data = mapper.readValue(message, MonitorData::class.java)

            //remove values that are not selected
            data.data = data.data.filter { i -> currSession.selectedValues.contains(i.type) }

            //create the message body
            val messageData = if (currSession.viewMode == ViewMode.Trend) {
                //update current trend data and create the message
                updateSessionTrendData(data)
                createUpdateTrendDataMessage()
            } else {
                //just serialize the current record
                mapper.convertValue(data)
            }

            //wrap the data into a well formatted statusMessage for the DT
            val content = UpdateDataContent(componentParams.componentId,
                "vitalsigns-${currSession.viewMode.name.toLowerCase()}", messageData)
            outWebSocket.writeTextMessage(UpdateDataMessageFactory.buildUpdateDataMessage(content))
        } catch (e: Throwable) {
            println("[VitalSignsManagerVerticle] Error occurred while handling a websocket message: ${e.message}")
            e.printStackTrace()
        }
    }

    /**
     * Sends a well formatted update data message to the client.
     */
    private fun sendUpdateDataMessage(message: Any) {
        val viewMode = "$UPDATE_DATA_VIEWER_NAME${currSession.viewMode.name.toLowerCase()}"
        val content = UpdateDataContent(componentParams.componentId, viewMode, mapper.convertValue(message))
        outWebSocket.writeTextMessage(UpdateDataMessageFactory.buildUpdateDataMessage(content))
    }

    /**
     * Establishes a websocket with the SmartScreen service
     */
    private fun establishWebSocket(endpointInfo: ServiceRoute): Future<WebSocket> {
        val future = Future.future<WebSocket>()
        val client = vertx.createHttpClient()
        client.websocket(endpointInfo.port, endpointInfo.address, endpointInfo.route, { ws ->
            future.complete(ws)
        }, { e ->
            future.fail(e)
        })
        return future
    }
    //endregion

    //region helpers
    /**
     * Creates the verticle http server router
     */
    private fun createRouter(): Router {
        val router = Router.router(vertx)
        router.route().handler(BodyHandler.create())
        router.route().handler(LoggerHandler.create())

        //default routes
        router.get(HeartBeatRoute.API_HEARTBEAT).handler { it.response().success() }

        //main routes
        router.post("${verticleConfig.baseApiUrl}/init").asyncHandler { handleInit(it) }
        router.post("${verticleConfig.baseApiUrl}/destroy").handler { handleDestroy(it) }

        //actions
        router.get("${verticleConfig.baseApiUrl}/actions").handler { handleGetActions(it) }
        router.post("${verticleConfig.baseApiUrl}/actions/view-mode/instant").asyncHandler { handleShowInstant(it) }
        router.post("${verticleConfig.baseApiUrl}/actions/view-mode/trend").asyncHandler { handleShowTrend(it) }
        router.post("${verticleConfig.baseApiUrl}/actions/select-values").handler { handleSelectValues(it) }

        return router
    }

    /**
     * Creates the update trend data message.
     */
    private fun createUpdateTrendDataMessage(): JsonNode {
        return mapper.createObjectNode()
            .put(UPDATE_DATA_TREND_RECORDS_COUNT_KEY, round(currSession.trendRange / VITAL_SIGNS_UPDATE_TIME).toInt())
            .set(UPDATE_DATA_TREND_DATA_KEY, mapper.createArrayNode()
                .addAll(
                    currSession.trendData.map {
                        it.data = it.data.filter { i -> currSession.selectedValues.contains(i.type) }
                        mapper.convertValue<JsonNode>(it)
                    }
                )
            )
    }

    /**
     * Updates the current session trend data (removes older records if the maximum records count is reached and pushes the new one).
     */
    private fun updateSessionTrendData(newRecord: MonitorData) {

        val expectedRecordsCount = round(currSession.trendRange / VITAL_SIGNS_UPDATE_TIME).toInt()

        //Drop oldest trend cache record
        if (currSession.trendData.count() >= expectedRecordsCount) {
            currSession.trendData.drop(0)
        }
        //Add newer record to cache
        currSession.trendData.add(newRecord)
    }
    //endregion
}
