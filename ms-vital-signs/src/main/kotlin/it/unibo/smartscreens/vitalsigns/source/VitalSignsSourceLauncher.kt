package it.unibo.smartscreens.vitalsigns.source

import io.vertx.core.Vertx

fun main() {
    Vertx.vertx().deployVerticle(VitalSignsSourceVerticle()) {
        if (it.succeeded()) {
            println("The verticle has been successfully deployed, with id: " + it.result())
        } else {
            println("Error during deployment: " + it.cause().message)
        }
    }
}
