package it.unibo.smartscreens.vitalsigns.manager.models

enum class ViewMode {
    Instant,
    Trend
}
