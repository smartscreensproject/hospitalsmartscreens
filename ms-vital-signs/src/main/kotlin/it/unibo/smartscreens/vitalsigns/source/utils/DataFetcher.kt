package it.unibo.smartscreens.vitalsigns.source.utils

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.Future
import it.unibo.smartscreens.sources.common.extensions.setFailure
import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.utils.ClientProvider.webClient
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper
import it.unibo.smartscreens.vitalsigns.source.models.Monitor
import it.unibo.smartscreens.vitalsigns.source.models.MonitorData
import it.unibo.smartscreens.vitalsigns.source.models.VitalSign
import java.util.*


class DataFetcher(private val config: ServiceConfig) {

    companion object {
        private const val MONITOR_ID_PARAM_KEY = "{monitorId}"
        private const val GET_MONITORS_API_URL = "/api/monitors"
        private const val GET_MONITOR_API_URL = "/api/monitors/$MONITOR_ID_PARAM_KEY"
        private const val GET_MONITOR_DATA_API_URL = "/api/monitors/$MONITOR_ID_PARAM_KEY/vitalsigns"
    }

    fun getMonitors(): Future<List<Monitor>> {
        val future = Future.future<List<Monitor>>()
        webClient.get(config.port, config.address, GET_MONITORS_API_URL).send { ar ->
            if (ar.succeeded()) {
                val monitors = mapper.readValue<List<RemoteMonitor>>(ar.result().bodyAsString())
                future.complete(monitors.map { it.toMonitor() })
            } else {
                ar.setFailure(future)
            }
        }
        return future
    }

    fun getMonitor(id: String): Future<Monitor> {
        val future = Future.future<Monitor>()
        webClient.get(config.port, config.address, GET_MONITOR_API_URL.replace(MONITOR_ID_PARAM_KEY, id)).send { ar ->
            if (ar.succeeded()) {
                val monitor = mapper.readValue<RemoteMonitor>(ar.result().bodyAsString())
                future.complete(monitor.toMonitor())
            } else {
                ar.setFailure(future)
            }
        }
        return future
    }

    fun getMonitorData(id: String): Future<MonitorData> {
        val future = Future.future<MonitorData>()
        webClient.get(config.port, config.address, GET_MONITOR_DATA_API_URL.replace(MONITOR_ID_PARAM_KEY, id)).send { ar ->
            if (ar.succeeded()) {
                val obj = mapper.readValue<RemoteMonitorData>(ar.result().bodyAsString())
                future.complete(obj.toMonitorData())
            } else {
                ar.setFailure(future)
            }
        }
        return future
    }

    //region Remote models
    //private models for incoming data
    private data class RemoteMonitorData(var pid: String,
                                         var pname: String,
                                         @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss") var timestamp: Date,
                                         var vitalsigns: List<VitalSign>) {
        fun toMonitorData(): MonitorData {
            return MonitorData(pid, pname, timestamp, null, vitalsigns)
        }
    }

    private data class RemoteMonitor(var id: String, var ip_gateway: String, var port_gateway: Int) {
        fun toMonitor(): Monitor {
            return Monitor(id, ip_gateway, port_gateway, null)
        }
    }
    //endregion
}
