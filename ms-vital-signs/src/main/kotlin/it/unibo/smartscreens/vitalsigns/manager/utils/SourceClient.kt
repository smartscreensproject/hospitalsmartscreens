package it.unibo.smartscreens.vitalsigns.manager.utils

import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.Future
import io.vertx.core.http.WebSocket
import it.unibo.smartscreens.sources.common.extensions.hasContent
import it.unibo.smartscreens.sources.common.extensions.setFailure
import it.unibo.smartscreens.sources.common.extensions.success
import it.unibo.smartscreens.sources.common.models.SubscriptionInfo
import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.utils.ClientProvider.httpClient
import it.unibo.smartscreens.sources.common.utils.ClientProvider.webClient
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper
import it.unibo.smartscreens.vitalsigns.common.models.VitalSignInfo
import it.unibo.smartscreens.vitalsigns.source.models.Monitor
import it.unibo.smartscreens.vitalsigns.source.models.MonitorData
import it.unibo.smartscreens.vitalsigns.source.models.Patient

class SourceClient(private val config: ServiceConfig) {

    companion object {
        //path parameters
        private const val PATIENT_PARAM_KEY = "{patientId}"

        //routes
        private const val GET_INFO_API_URL = "/api/info"
        private const val GET_MONITORS_API_URL = "/api/monitors"
        private const val GET_PATIENTS_API_URL = "/api/patients"
        private const val GET_PATIENT_DATA_API_URL = "/api/patients/$PATIENT_PARAM_KEY/vitalsigns"
        private const val SUBSCRIBE_PATIENT_DATA_API_URL = "/api/patients/$PATIENT_PARAM_KEY/vitalsigns/subscribe"

        //query param
        private const val TREND_RANGE_QUERY_PARAM_KEY = "range"
    }

    fun getVitalSignsInfo(): Future<List<VitalSignInfo>> {
        val future = Future.future<List<VitalSignInfo>>()
        webClient.get(config.port, config.address, GET_INFO_API_URL).send {
            if (it.success()) {
                val parsedList = mapper.readValue<List<VitalSignInfo>>(it.result().bodyAsString())
                future.complete(parsedList)
            } else {
                it.setFailure(future)
            }
        }
        return future
    }

    fun getMonitors(): Future<List<Monitor>> {
        val future = Future.future<List<Monitor>>()
        webClient.get(config.port, config.address, GET_MONITORS_API_URL).send {
            if (it.success()) {
                val parsedList = mapper.readValue<List<Monitor>>(it.result().bodyAsString())
                future.complete(parsedList)
            } else {
                it.setFailure(future)
            }
        }
        return future
    }

    fun getPatients(): Future<List<Patient>> {
        val future = Future.future<List<Patient>>()
        webClient.get(config.port, config.address, GET_PATIENTS_API_URL).send {
            if (it.success()) {
                val parsedList = mapper.readValue<List<Patient>>(it.result().bodyAsString())
                future.complete(parsedList)
            } else {
                it.setFailure(future)
            }
        }
        return future
    }

    fun getPatientData(patientId: String): Future<MonitorData?> {
        val future = Future.future<MonitorData>()
        webClient.get(config.port, config.address, GET_PATIENT_DATA_API_URL.replace(PATIENT_PARAM_KEY, patientId))
            .send {
                if (it.success()) {
                    if (it.result().hasContent()) {
                        future.complete(mapper.readValue<MonitorData>(it.result().bodyAsString()))
                    } else {
                        future.complete(null)
                    }
                } else {
                    it.setFailure(future)
                }
            }
        return future
    }

    fun getPatientDataTrend(patientId: String, range: String): Future<List<MonitorData>> {
        val future = Future.future<List<MonitorData>>()
        webClient.get(config.port, config.address, GET_PATIENT_DATA_API_URL.replace(PATIENT_PARAM_KEY, patientId))
            .setQueryParam(TREND_RANGE_QUERY_PARAM_KEY, range).send {
                if (it.success()) {
                    if (it.result().hasContent()) {
                        future.complete(mapper.readValue<List<MonitorData>>(it.result().bodyAsString()))
                    } else {
                        future.complete(listOf())
                    }
                } else {
                    it.setFailure(future)
                }
            }
        return future
    }

    fun subscribe(patientId: String): Future<WebSocket> {
        val future = Future.future<WebSocket>()

        val url = SUBSCRIBE_PATIENT_DATA_API_URL.replace(PATIENT_PARAM_KEY, patientId)
        httpClient.websocket(config.port, config.address, url, { ws ->
            future.complete(ws)
        }, {
            future.fail(it)
        })
        return future
    }

    fun unsubscribe(patientId: String, websocket: WebSocket): Future<Void> {
        val future = Future.future<Void>()
        webClient.delete(config.port, config.address, SUBSCRIBE_PATIENT_DATA_API_URL.replace(PATIENT_PARAM_KEY, patientId))
            .sendJson(mapper.writeValueAsString(SubscriptionInfo(websocket.textHandlerID()))) {
                if (it.success()) {
                    future.complete()
                } else {
                    it.setFailure(future)
                }
            }
        return future
    }
}
