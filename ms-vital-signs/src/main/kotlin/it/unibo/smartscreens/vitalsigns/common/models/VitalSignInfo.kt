package it.unibo.smartscreens.vitalsigns.common.models

data class VitalSignInfo(var name: String, var uom: String)
