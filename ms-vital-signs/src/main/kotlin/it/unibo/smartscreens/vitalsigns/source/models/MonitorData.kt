package it.unibo.smartscreens.vitalsigns.source.models

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonIgnoreProperties("_id")
data class MonitorData(@JsonProperty("patientId") var patientId: String,
                       @JsonProperty("patientName") var patientName: String,
                       @JsonProperty("timestamp") @JsonFormat(shape = JsonFormat.Shape.STRING) var timestamp: Date,
                       @JsonProperty("deviceId") var deviceId: String?,
                       @JsonProperty("data") var data: List<VitalSign>)

