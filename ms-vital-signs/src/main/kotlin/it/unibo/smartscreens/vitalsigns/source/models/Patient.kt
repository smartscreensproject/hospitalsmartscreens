package it.unibo.smartscreens.vitalsigns.source.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties("_id")
data class Patient(@JsonProperty("id") var id: String,
                   @JsonProperty("name") var name: String)
