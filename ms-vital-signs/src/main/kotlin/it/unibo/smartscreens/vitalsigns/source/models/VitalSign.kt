package it.unibo.smartscreens.vitalsigns.source.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties("_id")
data class VitalSign(@JsonProperty("type") var type: String,
                     @JsonProperty("value") var value: String,
                     @JsonProperty("uom") var uom: String)
