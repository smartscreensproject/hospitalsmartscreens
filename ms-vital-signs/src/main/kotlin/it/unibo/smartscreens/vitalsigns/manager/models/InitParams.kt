package it.unibo.smartscreens.vitalsigns.manager.models

data class InitParams(var patientId: String, var websocketEndpoint: String, var componentInfo: ComponentInfo)
