package it.unibo.smartscreens.vitalsigns.manager.models.config

data class ManagerServiceConfig(var host: String,
                                var port: Int,
                                var baseApiUrl: String)
