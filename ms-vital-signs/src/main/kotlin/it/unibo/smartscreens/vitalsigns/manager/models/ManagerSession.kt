package it.unibo.smartscreens.vitalsigns.manager.models

import it.unibo.smartscreens.vitalsigns.source.models.MonitorData

class ManagerSession {
    /**
     * the current selected patient id.
     */
    var patientId: String = ""
    /**
     * The current view mode.
     */
    var viewMode: ViewMode = ViewMode.Instant
    /**
     * The current selected vital signs
     */
    var selectedValues: MutableList<String> = mutableListOf()
    /**
     * The trend range (in minutes)
     */
    var trendRange: Int = -1
    /**
     * The trend data cache.
     */
    var trendData: MutableList<MonitorData> = mutableListOf()
}
