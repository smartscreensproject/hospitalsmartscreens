package it.unibo.smartscreens.sources.common.models

import com.fasterxml.jackson.annotation.JsonProperty

data class HookInfo(@JsonProperty("endpoint") var endpoint: String)
