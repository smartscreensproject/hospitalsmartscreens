package it.unibo.smartscreens.sources.common.utils

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

object Scheduler {

    private val executor = Executors.newScheduledThreadPool(1)
    private var currentTask: ScheduledFuture<*>? = null

    fun start(schedulingTime: Long, unit: TimeUnit = TimeUnit.MILLISECONDS, operation: () -> Unit) {
        currentTask = executor.scheduleAtFixedRate(Runnable(operation), 5000, schedulingTime, unit)
    }

    fun stop() {
        currentTask?.cancel(true)
        executor.shutdown()
        executor.awaitTermination(20, TimeUnit.SECONDS)
    }
}
