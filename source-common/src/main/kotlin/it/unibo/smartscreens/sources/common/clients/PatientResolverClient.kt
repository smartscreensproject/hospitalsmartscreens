package it.unibo.smartscreens.sources.common.clients

import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import it.unibo.smartscreens.common.model.patient.Patient
import it.unibo.smartscreens.common.msroute.PatientIdResolverRoute
import it.unibo.smartscreens.sources.common.extensions.setFailure
import it.unibo.smartscreens.sources.common.extensions.success
import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.utils.ClientProvider.webClient
import it.unibo.smartscreens.sources.common.utils.MapperProvider

class PatientResolverClient(private val config: ServiceConfig) {

    /**
     * Gets a patient by its id.
     */
    fun getPatientById(id: String): Future<Patient> {
        val future = Future.future<Patient>()

        webClient.get(config.port, config.address, "${PatientIdResolverRoute.API_PATIENT}/$id").send {
                if (it.success()) {
                    future.complete(it.result().bodyAsJsonObject().mapTo(Patient::class.java))
                } else {
                    it.setFailure(future)
                }
            }
        return future
    }

    /**
     * Gets all the patients that matches the input codes
     */
    fun getPatientsByCodes(queryCodes: Map<String, String>): Future<List<Patient>> {
        val future = Future.future<List<Patient>>()
        val body = JsonObject.mapFrom(queryCodes)
        webClient.post(config.port, config.address, PatientIdResolverRoute.API_SEARCH_PATIENT)
            .sendJsonObject(body) {
                if (it.success()) {
                    val results = MapperProvider.mapper.readValue<List<Patient>>(it.result().bodyAsString())
                    future.complete(results)
                } else {
                    it.setFailure(future)
                }
            }
        return future
    }
}
