package it.unibo.smartscreens.sources.common.extensions

import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Route
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.HttpResponse
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.*

private const val CONTENT_TYPE_HEADER_KEY = "Content-Type"
private const val APPLICATION_JSON_MIME = "application/json"
private const val MONGODB_ID_KEY = "_id"

//region HttpServerResponse
/**
 * Sends the response to the client by placing the input json-encoded string in the body.
 */
fun HttpServerResponse.json(value: String) {
    this.putHeader(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_MIME)
    this.end(value)
}

/**
 * Sends the response to the client by placing the serialized input json object in the body.
 */
fun HttpServerResponse.json(obj: JsonObject) {
    this.putHeader(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_MIME)
    this.end(obj.toString())
}

/**
 * Sends the response to the client by placing a serialized json array that contains the input list of json objects in the body.
 */
fun HttpServerResponse.json(objs: List<JsonObject>) {
    this.putHeader(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_MIME)
    this.end(JsonArray(objs).toString())
}

/**
 * Ends the response with a 200 OK and writes the input json object in the body, if provided.
 */
fun HttpServerResponse.success(body: JsonObject? = null) {
    this.statusCode = 200
    this.statusMessage = "OK"

    body?.let {
        this.end(it.encode())
    } ?: this.end()
}

/**
 * Ends the response with a 400 Bad Request.
 */
fun HttpServerResponse.noContent() {
    this.statusCode = 204
    this.statusMessage = "No Content"
    this.end()
}

/**
 * Ends the response with a 400 Bad Request.
 */
fun HttpServerResponse.badRequest() {
    this.statusCode = 400
    this.statusMessage = "Bad Request"
    this.end()
}

/**
 * Ends the response with a 404 Not Found.
 */
fun HttpServerResponse.notFound() {
    this.statusCode = 404
    this.statusMessage = "Not Found"
    this.end()
}

/**
 * Ends the response with a 500 Internal Server error with the message of the input throwable in the body, if declared.
 */
fun HttpServerResponse.error(throwable: Throwable? = null) {
    this.statusCode = 500
    this.statusMessage = "Internal Server Error"
    throwable?.let {
        this.end(throwable.message)
    } ?: this.end()
}
//endregion

//region HttpResponse
/**
 * Determines whether an HttpResponse is succeeded or not.
 */
fun HttpResponse<out Any>.success(): Boolean {
    return this.statusCode() < 300
}

/**
 * Determines whether an HttpResponse has body content or not.
 */
fun HttpResponse<out Any>.hasContent(): Boolean {
    return this.statusCode() != 204 && this.success() && this.body() != null
}
//endregion

//region JsonObject
/**
 * Removes the mongodb _id property inside the json object if it's present.
 */
fun JsonObject.purgeId(): JsonObject {
    this.remove(MONGODB_ID_KEY)
    return this
}

/**
 * Removes the mongodb _id property inside all json objects if it's present.
 */
fun List<JsonObject>.purgeIds(): List<JsonObject> {
    this.forEach { it.purgeId() }
    return this
}
//endregion

//region Route
/**
 * An extension method for simplifying coroutines usage with Vert.x Web routers
 */
fun Route.asyncHandler(fn: suspend (RoutingContext) -> Unit) {
    handler { ctx ->
        GlobalScope.launch(ctx.vertx().dispatcher()) {
            try {
                fn(ctx)
            } catch (ex: HttpError) {
                println("Error detected inside async route handler: ${ex.message}")
                ex.printStackTrace(System.err)
                ctx.response().setStatusCode(ex.statusCode).setStatusMessage(ex.statusMessage).end()
            } catch (e: Throwable) {
                println("Error detected inside async route handler: ${e.message}")
                e.printStackTrace(System.err)
                ctx.fail(e)
            }
        }
    }
}
//endregion

//region AsyncResult
/**
 * Determines whether an AsyncResult has succeeded or not.
 */
fun AsyncResult<out Any>.success(): Boolean {
    return if (this.result() is HttpResponse<*>) {
        val response = this.result() as HttpResponse<*>
        this.succeeded() && !this.failed() && response.statusCode() < 300
    } else {
        this.succeeded() && !this.failed()
    }
}

/**
 * Determines whether an AsyncResult has succeeded or not.
 */
fun AsyncResult<out HttpResponse<*>>.setFailure(future: Future<*>) {
    return if (this.failed()) {
        future.fail(this.cause())
    } else {
        val result = this.result()
        future.fail(HttpError(result.statusCode(), result.statusMessage()))
    }
}
//endregion
