package it.unibo.smartscreens.sources.common.models.config

data class ServiceInfo(var name: String, var descr: String)
