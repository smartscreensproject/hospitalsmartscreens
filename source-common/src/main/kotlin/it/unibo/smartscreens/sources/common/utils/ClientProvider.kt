package it.unibo.smartscreens.sources.common.utils

import io.vertx.core.Vertx
import io.vertx.core.http.HttpClient
import io.vertx.ext.web.client.WebClient

object ClientProvider {
    private val vertx: Vertx = Vertx.vertx()
    val httpClient: HttpClient = vertx.createHttpClient()
    val webClient: WebClient = WebClient.wrap(httpClient)

    fun close() {
        vertx.close()
    }
}
