package it.unibo.smartscreens.sources.common.models

import com.fasterxml.jackson.databind.JsonNode
import it.unibo.smartscreens.common.enums.SourceSocketMessageType
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper

data class UpdateData(var type: SourceSocketMessageType, var msg: String)
data class UpdateDataContent(var componentId: String, var viewer: String, var data: JsonNode)
/**
 * Update data message factory.
 */
object UpdateDataMessageFactory {
    /**
     * Builds and serializes a new update data message.
     */
    fun buildUpdateDataMessage(content: UpdateDataContent): String {
        val messageContent = mapper.writeValueAsString(content)
        return mapper.writeValueAsString(UpdateData(SourceSocketMessageType.UPDATE_DATA, messageContent))
    }
}
