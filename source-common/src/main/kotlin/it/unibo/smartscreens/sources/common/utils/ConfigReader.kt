package it.unibo.smartscreens.sources.common.utils

import com.fasterxml.jackson.core.type.TypeReference
import io.vertx.core.json.JsonObject
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper
import java.io.FileNotFoundException

object ConfigReader {

    fun readConfig(path: String): JsonObject {
        val configStream = this::class.java.classLoader.getResourceAsStream(path)
        return JsonObject(configStream.bufferedReader().use { it.readText() })
    }

    fun <T> readConfig(typeReference: TypeReference<T>, path: String): T {
        try {
            val configStream = this::class.java.classLoader.getResourceAsStream(path)
            val configString = configStream.bufferedReader().use { it.readText() }
            return mapper.readValue(configString, typeReference)
        } catch (e: Throwable) {
            throw FileNotFoundException("Resource configuration file '$path' could be found in the resources (${e.message})")
        }
    }

    fun <T> readConfig(typeReference: TypeReference<T>, path: String, handler: (T) -> Unit) {
        try {
            val configStream = this::class.java.classLoader.getResourceAsStream(path)
            val configString = configStream.bufferedReader().use { it.readText() }
            val parsedConfig : T = mapper.readValue<T>(configString, typeReference)
            handler(parsedConfig)
        } catch (e: Throwable) {
            throw FileNotFoundException("Resource configuration file '$path' could be found in the resources (${e.message})")
        }
    }
}
