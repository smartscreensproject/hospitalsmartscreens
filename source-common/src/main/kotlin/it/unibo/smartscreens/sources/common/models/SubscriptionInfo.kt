package it.unibo.smartscreens.sources.common.models

import com.fasterxml.jackson.annotation.JsonProperty

data class SubscriptionInfo(@JsonProperty("connectionId") var connectionId: String)
