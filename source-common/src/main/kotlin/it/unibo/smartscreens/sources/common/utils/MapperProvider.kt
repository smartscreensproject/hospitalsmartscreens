package it.unibo.smartscreens.sources.common.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

/**
 * The Jackson object mapper provider
 */
object MapperProvider {
    val mapper: ObjectMapper = jacksonObjectMapper().findAndRegisterModules()
}
