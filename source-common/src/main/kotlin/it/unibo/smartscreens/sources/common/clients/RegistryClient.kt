package it.unibo.smartscreens.sources.common.clients

import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import it.unibo.smartscreens.common.model.ServiceAddress
import it.unibo.smartscreens.common.model.registry.MicroServiceEntity
import it.unibo.smartscreens.common.msroute.RegistryRoute
import it.unibo.smartscreens.sources.common.extensions.setFailure
import it.unibo.smartscreens.sources.common.extensions.success
import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.utils.ClientProvider.webClient
import it.unibo.smartscreens.sources.common.utils.ConfigReader
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper

class RegistryClient {

    companion object {
        private const val REGISTRY_CONFIG = "registryConfig.json"

        //query params key
        private const val NAME_QUERY_PARAM_KEY = "nameService"

        //register body properties
        private const val ADDRESS_KEY = "serviceAddress"
        private const val NAME_KEY = NAME_QUERY_PARAM_KEY
        private const val DESCRIPTION_KEY = "description"
    }

    private val config: ServiceConfig = ConfigReader.readConfig(jacksonTypeRef(), REGISTRY_CONFIG)

    /**
     * Gets a service information based on its id.
     */
    fun getServiceById(id: String): Future<MicroServiceEntity> {
        val future = Future.future<MicroServiceEntity>()
        webClient.get(config.port, config.address, "${RegistryRoute.API_SERVICES}/$id")
            .send {
                if (it.success()) {
                    val serviceEntity = it.result().bodyAsJson(MicroServiceEntity::class.java)
                    future.complete(serviceEntity)
                } else {
                    it.setFailure(future)
                }
            }
        return future
    }

    /**
     * Gets a service information based on its id.
     */
    fun getServicesByName(name: String): Future<List<MicroServiceEntity>> {
        val future = Future.future<List<MicroServiceEntity>>()
        webClient.get(config.port, config.address, RegistryRoute.API_SERVICES)
            .addQueryParam(NAME_QUERY_PARAM_KEY, name)
            .send {
                if (it.success()) {
                    val serviceEntity = mapper.readValue<List<MicroServiceEntity>>(it.result().bodyAsString())
                    future.complete(serviceEntity)
                } else {
                    it.setFailure(future)
                }
            }
        return future
    }

    /**
     * Registers a new service.
     */
    fun registerService(serviceAddress: ServiceAddress, name: String, desc: String): Future<MicroServiceEntity> {
        val future = Future.future<MicroServiceEntity>()

        val body = JsonObject()
            .put(ADDRESS_KEY, JsonObject.mapFrom(serviceAddress))
            .put(NAME_KEY, name)
            .put(DESCRIPTION_KEY, desc)

        webClient.post(config.port, config.address, RegistryRoute.API_SERVICES)
            .sendJsonObject(body) {
                if (it.success()) {
                    val serviceEntity = it.result().bodyAsJson(MicroServiceEntity::class.java)
                    future.complete(serviceEntity)
                } else {
                    it.setFailure(future)
                }
            }

        return future
    }
}
