package it.unibo.smartscreens.sources.common.models.config

data class ServiceConfig(var address: String, var port: Int)
