package it.unibo.smartscreens.sources.common.extensions

class HttpError(val statusCode: Int, val statusMessage: String) : Error()
