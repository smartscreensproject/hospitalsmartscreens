package it.unibo.smartscreens.registry.model;

import it.unibo.smartscreens.common.model.registry.AbstractEntity;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Registry {

    private final Map<RegistryEntityType, List<AbstractEntity>> registry;

    public Registry() {
        this.registry = new HashMap<>();
    }

    public List<AbstractEntity> getEntityForType(RegistryEntityType type) {
        return registry.getOrDefault(type, new LinkedList<>());
    }

    public Stream<AbstractEntity> getEntityForTypeAsStream(RegistryEntityType type) {
        return registry.getOrDefault(type, new LinkedList<>()).stream();
    }

    public List<AbstractEntity> getEntities() {
        return getEntitiesAsStream().collect(Collectors.toList());
    }

    public Stream<AbstractEntity> getEntitiesAsStream() {
        return registry.values()
                        .stream()
                        .flatMap(Collection::stream);
    }

    public boolean addEntity(RegistryEntityType type, AbstractEntity entity) {

        if (getEntitiesAsStream().anyMatch(e -> e.equals(entity))) {
           return false;
        }
        // Add the list to the registry if its empty
        registry.putIfAbsent(type, new LinkedList<>());
        registry.get(type).add(entity);
        return true;
    }

    public boolean removeEntity(String id) {
        for (List<AbstractEntity> value : registry.values()) {
            if (value.removeIf(e -> e.getId().equals(id))) {
                return true;
            }
        }
        return false;
    }
}
