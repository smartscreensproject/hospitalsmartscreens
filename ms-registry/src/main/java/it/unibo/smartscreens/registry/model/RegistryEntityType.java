package it.unibo.smartscreens.registry.model;

public enum RegistryEntityType {

    MICRO_SERVICE,
    SMART_SCREEN,
    INPUT_MANAGER;
}
