package it.unibo.smartscreens.registry.verticle;

import it.unibo.smartscreens.registry.model.Registry;
import it.unibo.smartscreens.registry.model.RegistryEntityType;
import it.unibo.smartscreens.common.apiclient.impl.HeartBeatApiClientImpl;
import it.unibo.smartscreens.common.apiclient.interfaces.HeartBeatApiClient;
import it.unibo.smartscreens.common.verticles.BaseVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.model.registry.AbstractEntity;
import it.unibo.smartscreens.common.model.registry.MicroServiceEntity;
import it.unibo.smartscreens.common.model.registry.InputManagerEntity;
import it.unibo.smartscreens.common.model.registry.SmartScreenEntity;
import it.unibo.smartscreens.common.msroute.RegistryRoute;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RegistryVerticle extends BaseVerticle {

    private final Registry registry;
    private HeartBeatApiClient heartBeatApiClient;
    private final long delayHeartBeat = 10000;

    public RegistryVerticle() {
        this.registry = new Registry();
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        Router router = initRouter(RegistryRoute.API_ROOT);

        router.get(RegistryRoute.API_SERVICES).handler(this::handleGetServices);
        router.get(RegistryRoute.API_SERVICES + "/:servicesId").handler(this::handleGetServicesById);
        router.post(RegistryRoute.API_SERVICES).handler(this::handlePostServices);

        router.get(RegistryRoute.API_SMART_SCREEN).handler(this::handleGetSmartScreens);
        router.get(RegistryRoute.API_SMART_SCREEN + "/:smartScreenId").handler(this::handleGetSmartScreensById);
        router.post(RegistryRoute.API_SMART_SCREEN).handler(this::handlePostSmartScreens);

        router.get(RegistryRoute.API_INPUT_MANAGER).handler(this::handleGetInputManagers);
        router.get(RegistryRoute.API_INPUT_MANAGER + "/:inputManagerId").handler(this::handleGetInputManagersById);
        router.post(RegistryRoute.API_INPUT_MANAGER).handler(this::handlePostInputManagers);


        vertx.createHttpServer()
            .requestHandler(router)
            .listen(super.getRegistryAddress().getPort(), super.getRegistryAddress().getAddress(), (res) -> {
                if (res.succeeded()) {
                    println("Registry service listen on port: " + res.result().actualPort());
                } else {
                    println("Error in http server: " + res.cause().getMessage());
                }
            });

        heartBeatApiClient = new HeartBeatApiClientImpl(null, vertx);
        vertx.setPeriodic(delayHeartBeat, this::heartBeat);

        startFuture.complete();
    }

    private void heartBeat(long timerId) {
        registry.getEntities().forEach(e -> heartBeatApiClient.heartBeat(e.getServiceAddress())
            .setHandler(res -> {
               if (res.failed()) {
                   println(e.getServiceAddress() + " no respond to heartbeat, will be removed");
                   if (registry.removeEntity(e.getId())) {
                       println(e.getServiceAddress() + " removed successfully");
                   }
               }
            })
        );
    }

    private Optional<ServiceAddress> getServiceAddress(JsonObject body, String remoteAddress) {
        ServiceAddress serviceAddress = body.getJsonObject("serviceAddress").mapTo(ServiceAddress.class);
        if (serviceAddress.getPort() >= 0 && serviceAddress.getPort() <= 65535) {
            if (serviceAddress.getAddress() == null || serviceAddress.getAddress().isEmpty()) {
                serviceAddress.setAddress(remoteAddress);
            }
            return Optional.of(serviceAddress);
        }
        return Optional.empty();
    }

    private void handleGetServices(RoutingContext rc) {

        List<String> nameServices = rc.queryParam("nameService");
        List<MicroServiceEntity> entities = this.registry.getEntityForType(RegistryEntityType.MICRO_SERVICE)
            .stream()
            .map(ms -> (MicroServiceEntity)ms)
            .filter(ms -> nameServices.isEmpty() || nameServices.contains(ms.getName()))
            .collect(Collectors.toList());
        rc.response().end(Json.encode(entities));
    }

    private void handleGetServicesById(RoutingContext rc) {
        String servicesId = rc.request().getParam("servicesId");
        if (servicesId == null) {
            rc.response().setStatusCode(400).setStatusMessage("id service missing").end();
            return;
        }
        Optional<JsonObject> response = this.registry.getEntityForTypeAsStream(RegistryEntityType.MICRO_SERVICE)
            .filter(e -> e.getId().equals(servicesId))
            .findFirst()
            .map(JsonObject::mapFrom);
        if (response.isPresent()) {
            rc.response().end(response.get().encode());
        } else {
            rc.response().setStatusCode(404).setStatusMessage("service required not found").end();
        }
    }

    private void handlePostServices(RoutingContext rc) {
        JsonObject body = rc.getBodyAsJson();
        Optional<ServiceAddress> serviceAddress = getServiceAddress(body, rc.request().remoteAddress().host());
        String name = body.getString("nameService");
        String description = body.getString("description");

        if (!serviceAddress.isPresent() || description == null || name == null) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        AbstractEntity entity = new MicroServiceEntity(serviceAddress.get(), name, description);
        if (this.registry.addEntity(RegistryEntityType.MICRO_SERVICE, entity)) {
            rc.response().end(JsonObject.mapFrom(entity).encode());
        } else {
            rc.response().setStatusCode(409).setStatusMessage("services already present at: " + serviceAddress.get()).end();
        }
    }

    private void handleGetSmartScreens(RoutingContext rc) {

        List<String> rooms = rc.queryParam("roomId");
        List<String> devices = rc.queryParam("deviceIdInRoom");
        List<SmartScreenEntity> smartScreenEntities =
            this.registry.getEntityForTypeAsStream(RegistryEntityType.SMART_SCREEN)
            .map(ss -> (SmartScreenEntity) ss)
            .filter(ss -> rooms.isEmpty() || rooms.contains(ss.getRoomId()))
            .filter(ss -> devices.isEmpty() || devices.contains(ss.getDeviceIdInRoom()))
            .collect(Collectors.toList());

        rc.response().end(Json.encode(smartScreenEntities));
    }

    private void handleGetSmartScreensById(RoutingContext rc) {
        String smartScreenId = rc.request().getParam("smartScreenId");
        if (smartScreenId == null) {
            rc.response().setStatusCode(400).setStatusMessage("id smart screen missing").end();
            return;
        }
        Optional<JsonObject> response = this.registry.getEntityForTypeAsStream(RegistryEntityType.SMART_SCREEN)
            .filter(e -> e.getId().equals(smartScreenId))
            .findFirst()
            .map(JsonObject::mapFrom);
        if (response.isPresent()) {
            rc.response().end(response.get().encode());
        } else {
            rc.response().setStatusCode(404).setStatusMessage("smart screen required not found").end();
        }
    }

    private void handlePostSmartScreens(RoutingContext rc) {
        JsonObject body = rc.getBodyAsJson();
        Optional<ServiceAddress> serviceAddress = getServiceAddress(body, rc.request().remoteAddress().host());

        String roomId = body.getString("roomId");
        String deviceIdInRoom = body.getString("deviceIdInRoom");

        if (!serviceAddress.isPresent() || roomId == null || deviceIdInRoom == null) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        AbstractEntity entity = new SmartScreenEntity(serviceAddress.get(), roomId, deviceIdInRoom);
        if (this.registry.addEntity(RegistryEntityType.SMART_SCREEN, entity)) {
            rc.response().end(JsonObject.mapFrom(entity).encode());
        } else {
            rc.response().setStatusCode(409).setStatusMessage("services already present at: " + serviceAddress.get()).end();
        }
    }

    private void handleGetInputManagers(RoutingContext rc) {

        List<String> rooms = rc.queryParam("roomId");
        List<String> devices = rc.queryParam("deviceIdInRoom");
        List<InputManagerEntity> smartScreenSubscribers =
            this.registry.getEntityForTypeAsStream(RegistryEntityType.INPUT_MANAGER)
                .map(ss -> (InputManagerEntity) ss)
                .filter(ss -> rooms.isEmpty() || rooms.contains(ss.getRoomId()))
                .filter(ss -> devices.isEmpty() || devices.contains(ss.getDeviceIdInRoom()))
                .collect(Collectors.toList());

        rc.response().end(Json.encode(smartScreenSubscribers));
    }

    private void handleGetInputManagersById(RoutingContext rc) {
        String inputManagerId = rc.request().getParam("inputManagerId");
        if (inputManagerId == null) {
            rc.response().setStatusCode(400).setStatusMessage("id input manager missing").end();
            return;
        }
        Optional<JsonObject> response = this.registry.getEntityForTypeAsStream(RegistryEntityType.INPUT_MANAGER)
            .filter(e -> e.getId().equals(inputManagerId))
            .findFirst()
            .map(JsonObject::mapFrom);
        if (response.isPresent()) {
            rc.response().end(response.get().encode());
        } else {
            rc.response().setStatusCode(404).setStatusMessage("input manager required not found").end();
        }
    }

    private void handlePostInputManagers(RoutingContext rc) {
        JsonObject body = rc.getBodyAsJson();
        Optional<ServiceAddress> serviceAddress = getServiceAddress(body, rc.request().remoteAddress().host());

        String roomId = body.getString("roomId");
        String deviceIdInRoom = body.getString("deviceIdInRoom");

        if (!serviceAddress.isPresent() || roomId == null || deviceIdInRoom == null) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        AbstractEntity entity = new InputManagerEntity(serviceAddress.get(), roomId, deviceIdInRoom);
        if (this.registry.addEntity(RegistryEntityType.INPUT_MANAGER, entity)) {
            rc.response().end(JsonObject.mapFrom(entity).encode());
        } else {
            rc.response().setStatusCode(409).setStatusMessage("services already present at: " + serviceAddress.get()).end();
        }
    }
}
