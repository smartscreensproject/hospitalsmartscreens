package it.unibo.smartscreens.registry.launcher;

import io.vertx.core.Vertx;
import it.unibo.smartscreens.registry.verticle.RegistryVerticle;

public class RegistryLauncher {

    public static void main(String[] args) {

        Vertx vertx = Vertx.vertx();

        // Example of a verticle from the same module
        RegistryVerticle registryVerticle = new RegistryVerticle();
        vertx.deployVerticle(registryVerticle, (handler) -> {
            if (handler.succeeded()) {
                System.out.println("The verticle has been successfully deployed, with id: " + handler.result());
            } else {
                System.out.println("Error during deployment: " + handler.cause().getMessage());
            }
        });
    }
}
