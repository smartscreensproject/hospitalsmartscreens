package it.unibo.smartscreens.patientidresolver.verticle;

import it.unibo.smartscreens.common.verticles.BaseVerticle;
import it.unibo.smartscreens.common.constant.NameService;
import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import it.unibo.smartscreens.common.msroute.PatientIdResolverRoute;
import it.unibo.smartscreens.common.model.patient.Patient;
import it.unibo.smartscreens.common.model.registry.MicroServiceEntity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Verticle representing a microservice whose job is to retrieve a patient codes
 */
public class PatientIdResolverVerticle extends BaseVerticle {

    private static final String MONGODB_CONFIG = "/mongodb.json";
    private static final String PATIENTS_COLLECTION = "patients";
    private MicroServiceEntity patientIdResolver;
    private MongoClient mongoClient;

    public PatientIdResolverVerticle() {

    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        vertx.executeBlocking(future -> {
            try (BufferedReader mongoConfigInputStream = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream(MONGODB_CONFIG), StandardCharsets.UTF_8))) {

                JsonObject mongoConfig = new JsonObject(mongoConfigInputStream.lines().reduce(String::concat).orElse(""));
                future.complete(mongoConfig);
            } catch (Exception e) {
                future.fail(e.getCause());
            }

        }, result -> {
            if (result.succeeded()) {
                mongoClient = MongoClient.createShared(vertx, (JsonObject) result.result());

                Router router = initRouter(PatientIdResolverRoute.API_ROOT, true);
                router.get(PatientIdResolverRoute.API_PATIENT + "/:patientId").handler(this::handleGetPatientById);
                router.get(PatientIdResolverRoute.API_PATIENT).handler(this::handleGetPatients);
                router.post(PatientIdResolverRoute.API_PATIENT).handler(this::handlePostPatients);
                router.put(PatientIdResolverRoute.API_PATIENT + "/:patientId").handler(this::handleUpdatePatient);
                router.post(PatientIdResolverRoute.API_SEARCH_PATIENT).handler(this::handleSearchPatients);

                vertx.createHttpServer()
                    .requestHandler(router)
                    .listen(5000, res -> {
                        if (res.succeeded()) {
                            println("Verticle deployed successfully on localhost:"+ res.result().actualPort());
                            subscribeToRegister(res.result().actualPort()).setHandler(subscription -> {
                               if (subscription.succeeded()) {
                                   startFuture.complete();
                               } else {
                                   startFuture.fail(subscription.cause());
                               }
                            });
                        } else {
                            println("Error in http server: " + res.cause().getMessage());
                            startFuture.fail(res.cause());
                        }
                    });
            } else {
                startFuture.fail(result.cause());
            }
        });


    }

    private Future<Void> subscribeToRegister(int port) {
        Future<Void> future = Future.future();
        getRegistryApiClient().postServices(port, NameService.PATIENT_ID_RESOLVER, "bla bla bla")
            .setHandler(res -> {
                if (res.succeeded()) {
                    patientIdResolver = res.result();
                    println(Json.encodePrettily(patientIdResolver));
                    future.complete();
                } else {
                    println(res.cause().getMessage());
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public void stop(Future<Void> stopFuture) throws Exception {
        println("Verticle stopped successfully");

        stopFuture.complete();
    }

    private void handleGetPatientById(RoutingContext rc) {
        String patientId = rc.pathParam("patientId");
        if (patientId == null || patientId.isEmpty()) {
            rc.response()
                .setStatusCode(400)
                .setStatusMessage("Missing patient id")
                .end();
            return;
        }
        findPatientAndResponse(rc, patientId);
    }

    private Future<List<Patient>> findPatients(JsonObject query) {
        Future<List<Patient>> future = Future.future();
        this.mongoClient.find(PATIENTS_COLLECTION, query, res -> {
            if (res.succeeded()) {
                List<Patient> patients = res.result().stream()
                    .map(pat -> pat.mapTo(Patient.class))
                    .collect(Collectors.toList());
                future.complete(patients);
            } else {
                future.fail(res.cause());
            }
        });
        return future;
    }

    private void findPatientsAndResponse(RoutingContext rc, JsonObject query) {
        findPatients(query).setHandler(res -> {
            if (res.succeeded()) {
                rc.response().end(Json.encode(res.result()));
            } else {
                rc.response().setStatusCode(500).setStatusMessage(res.cause().getMessage()).end();
            }
        });
    }

    private void findPatientAndResponse(RoutingContext rc, String patientId) {
        JsonObject query = new JsonObject()
            .put("patientId", patientId);
        this.mongoClient.findOne(PATIENTS_COLLECTION, query, null, res -> {
            if (res.succeeded()) {
                if (res.result() != null) {
                    Patient patient = res.result().mapTo(Patient.class);
                    rc.response().end(JsonObject.mapFrom(patient).encode());
                } else {
                    rc.response().setStatusCode(404).setStatusMessage("Patient not found").end();
                }
            } else {
                rc.response().setStatusCode(500).end();
            }
        });
    }

    private void handleGetPatients(RoutingContext rc) {
        findPatientsAndResponse(rc, new JsonObject());
    }

    private void handleSearchPatients(RoutingContext rc) {
        JsonObject body = rc.getBodyAsJson();
        JsonArray clauses = new JsonArray(body.stream()
            .map(e -> new JsonObject().put("codes." + e.getKey(), e.getValue()))
            .collect(Collectors.toList()));
        JsonObject query = new JsonObject()
            .put("$or", clauses);

        findPatientsAndResponse(rc, query);
    }

    private void handlePostPatients(RoutingContext rc) {
        JsonObject body = rc.getBodyAsJson();
        JsonObject query = new JsonObject()
            .put("patientId", UUID.randomUUID().toString().replace("-", "_"))
            .put("firstName", body.getString("firstName"))
            .put("lastName", body.getString("lastName"))
            .put("codes", body.getValue("codes"))
            .put("location", body.getJsonObject("location"));

        mongoClient.insert(PATIENTS_COLLECTION, query, res -> {
           if (res.succeeded()) {
               Patient newPatient = query.mapTo(Patient.class);
               rc.response().end(JsonObject.mapFrom(newPatient).encode());
           } else {
               rc.response().setStatusCode(500).setStatusMessage(res.cause().getMessage()).end();
           }
        });
    }

    private void handleUpdatePatient(RoutingContext rc) {
        Patient patient = rc.getBodyAsJson().mapTo(Patient.class);

        String patientId = rc.pathParam("patientId");
        if (patientId == null || patientId.isEmpty()) {
            rc.response()
                .setStatusCode(400)
                .setStatusMessage("Missing patient id")
                .end();
            return;
        }

        patient.setPatientId(patientId);
        JsonObject query = new JsonObject().put("patientId", patientId);
        mongoClient.replaceDocuments(PATIENTS_COLLECTION, query, JsonObject.mapFrom(patient), res -> {
           if (res.succeeded()) {
               if (res.result().getDocModified() == 1L) {
                   findPatientAndResponse(rc, patientId);
               } else {
                   rc.response()
                       .setStatusCode(404)
                       .setStatusMessage("Patient not found")
                       .end();
               }
           } else {
               rc.response()
                   .setStatusCode(500)
                   .setStatusMessage(res.cause().getMessage())
                   .end();
           }
        });

    }
}
