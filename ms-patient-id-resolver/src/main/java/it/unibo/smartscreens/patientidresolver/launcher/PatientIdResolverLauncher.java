package it.unibo.smartscreens.patientidresolver.launcher;

import io.vertx.core.Vertx;
import it.unibo.smartscreens.patientidresolver.verticle.PatientIdResolverVerticle;

public class PatientIdResolverLauncher {

    public static void main(String[] args) {

        Vertx vertx = Vertx.vertx();

        // Example of a verticle from the same module
        PatientIdResolverVerticle patientIdResolverVerticle = new PatientIdResolverVerticle();
        vertx.deployVerticle(patientIdResolverVerticle, (handler) -> {
            if (handler.succeeded()) {
                System.out.println("The verticle has been successfully deployed, with id: " + handler.result());
            } else {
                System.out.println("Error during deployment: " + handler.cause().getMessage());
            }
        });
    }
}
