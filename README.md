# Hospital Smart Screen

The aim of the project is to display datas emitted from hospital devices on a smart screen.

This project was developed for the course "Pervasive Computing" academic year 2018-2019. 


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

The project was built with the following languages:

- Java
- Kotlin
- JavaScript
- TypeScript

Make sure to have installed on your system at least a Java 8.X Runtime Environment and a Node.js interpreter


### Installing

To build and install all the project dependencies run `build.sh` or `build.bat` inside script directory:

```
cd script; ./build.sh
```

To easily run the microservices go to script directory and run:

```
cd script; node launcher.js
```

or if you are lazy just double-click:

- `launcher-for-lazy-people.sh` if you are on a Unix-like enviroment
- `launcher-for-lazy-people.bat` if you are on a Windows enviroment

The script will open many terminals, one for each microservice. The system should be ready when a web page will be opened in you favourite browser.

Temporarily to use the web application you should run an insecure instance of your browser. The script run-chrome-insecurely will setup an insecure chrome istance.

```
cd script; ./run-chrome-insecurely.sh
```


## Running the tests

To test the system:

```
cd mock/input-mock; node app.js
```

A visual text feedback should be produced and the output web UI should change accordingly to the operations executed.


## Deployment

To deploy the systems, perform in order the following steps:

1. based on the device address where the registry will be executed, change the hardcoded address in the following files:
    - `common/src/main/resources/registryConfig.json`
    - `source-common/src/main/resources/registryConfig.json`
    - `web-gui/src/app/utils/server-communication/services-rest-api.ts`
2. based on the device address where the video streams will be executed, change the hardcoded address in the following files:
    - `mock/video-streams-service/config.json`
    - `ms-video-streams/src/main/resources/source/config.json`
3. run mock sources
    - `cd mocks/vs-microservice-mock; ./launcher.sh`
    - `cd mocks/video-streams-service; ./launcher.sh`
4. launch registry and wait for its initialization
    - `./gradlew :ms-registry:run`
5. launch patient id resolver
    - `./gradlew :ms-patient-id-resolver:run`
6. launch vital signs
    - `./gradlew :ms-vital-signs:run`
7. launch video streams
    - `./gradlew :ms-video-streams:run`
8. launch patient related sources
    - `./gradlew :ms-patient-related-sources:run`
9. after the initialization of the previous steps launch input manager
    - `./gradlew :ms-input-manager:run`
10. launch web gui
    - `cd web-gui; npx ng serve -o --host 0.0.0.0 --poll=3000`, the app will be available at `http://your.ip.address:4200`


## Built With

- [Vert.x](https://vertx.io/) - Used to generate RSS Feeds
- [Angular](https://angular.io/) - The web framework used
- [Gradle](https://gradle.org/) - Dependency Management
- [npm](https://www.npmjs.com/) - Dependency Management


## Authors

- **Marco Galassi** - [marco.galassi6@studio.unibo.it](mailto:marco.galassi6@studio.unibo.it)
- **Davide Giacomini** - [davide.giacomini2@studio.unibo.it](mailto:davide.giacomini2@studio.unibo.it)
- **Andrea Placuzzi** - [andrea.placuzzi@studio.unibo.it](mailto:andrea.placuzzi@studio.unibo.it)
- **Giacomo Venturini** - [giacomo.venturini2@studio.unibo.it](mailto:giacomo.venturini2@studio.unibo.it)


## License

This project is licensed under the GNU License - see the [LICENSE.md](LICENSE.md) file for details
