#!/usr/bin/env node
const WebSocketClient = require('websocket').client;
 
const client = new WebSocketClient();
 
client.on('connectFailed', function(error) {
    console.log('Connection error: ' + error.toString());
});
 
client.on('connect', function(connection) {
    console.log('Websocket mock: connected');

    connection.on('error', function(error) {
        console.log("Error detected: " + error.toString());
    });

    connection.on('close', function() {
        console.log('Connection closed');
    });

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log("received: " + JSON.stringify(JSON.parse(message.utf8Data))); 
        }
    });
});
 
client.connect('ws://localhost:65000/api/patients/patient2/vitalsigns/subscribe');