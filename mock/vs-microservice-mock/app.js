const express = require('express');
const logger = require('morgan');

const monitorsRouter = require('./routes/monitors');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/monitors', monitorsRouter);

module.exports = app;
