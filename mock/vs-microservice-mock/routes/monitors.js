var express = require('express');
var moment = require('moment');
var router = express.Router();

/**
 * vs types
 */
var vstype = [
  "DIA",
  "SYS",
  "EtCO2",
  "SpO2",
  "Temp",
  "NBP-S",
  "NBP-D",
  "HR"
];

/**
 *  vs units of measurements 
 */
var vsuoms = [
  "mmHg",
  "mmHg",
  "mmHg",
  "%",
  "°C",
  "mmHg",
  "mmHg",
  "bpm"
];

/**
 * Available monitors
 */
var monitors = [
  {
    id: "reparto1_monitor0",
    ip_gateway: "localhost",
    port_gateway: 1000
  },
  {
    id: "reparto1_monitor1",
    ip_gateway: "localhost",
    port_gateway: 1001
  },
  {
    id: "reparto2_monitor0",
    ip_gateway: "localhost",
    port_gateway: 1002
  },
  {
    id: "reparto3_monitor0",
    ip_gateway: "localhost",
    port_gateway: 1003
  },
  {
    id: "reparto3_monitor1",
    ip_gateway: "localhost",
    port_gateway: 1004
  },
  {
    id: "reparto3_monitor2",
    ip_gateway: "localhost",
    port_gateway: 1005
  }
];

/**
 * Patients per monitor
 */
var patient_per_monitor = [
  {
    monitorId: "reparto1_monitor0",
    pid: "patient1",
    pname: "Mario Rossi"
  },
  {
    monitorId: "reparto1_monitor1",
    pid: "patient2",
    pname: "Carlo Verdi"
  },
  {
    monitorId: "reparto2_monitor0",
    pid: "patient3",
    pname: "Pippo Gialli"
  },
  {
    monitorId: "reparto3_monitor0",
    pid: "patient4",
    pname: "Pluto Neri"
  },
  {
    monitorId: "reparto3_monitor1",
    pid: "patient5",
    pname: "Paperino Rossini"
  },
  {
    monitorId: "reparto3_monitor2",
    pid: "patient6",
    pname: "Minnie Verdini"
  }
];


//get all monitors
router.get('/', (req, res) => {
  res.json(monitors);
});

//get a monitor
router.get('/:id', (req, res) => {

  var monitor = monitors.find(i => i.id == req.params.id)
  if (monitor) {
    res.json(monitor);
  } else {
    res.status(404).end();
  }
});

//get monitor vital signs
router.get('/:id/vitalsigns', (req, res) => {

  var patient = patient_per_monitor.find(i => i.monitorId == req.params.id);

  if (!patient) {
    res.status(404).end();
    return;
  }

  var vs = [];
  for (var vsIdx = 0; vsIdx < vstype.length; vsIdx++) {
    var vsType = vstype[vsIdx];
    var vsUom = vsuoms[vsIdx];
    var vsValue = 0;
  
    if (vsIdx < 2) {
      vsValue = getRandomInt(70, 190) + "/" + getRandomInt(40, 100);
    } else if (vsIdx != 4 && vsIdx != 7) {
      vsValue = getRandomInt(10, 100);
    } else if (vsIdx == 4) {
      vsValue = 36 + (38 - 36) * Math.random().toFixed(2);
    } else if (vsIdx == 7) {
      vsValue = getRandomInt(40, 120);
    }

    vs.push({
        type: vsType,
        value: vsValue,
        uom: vsUom
    });
  }
  
  res.json({
    pid: patient.pid,
    pname: patient.pname,
    timestamp: moment().format("YYYYMMDDHHmmss"),
    vitalsigns: vs
  });
});

const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

module.exports = router;
