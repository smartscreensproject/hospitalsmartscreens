const axios = require('axios');

const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
};

function printResponseStatus(status, successMessage, failureMessage) {
    if (status === 200) {
        console.log(successMessage);
    } else {
        console.log(failureMessage);
    }
}

function printException(debugMessage, e) {
    console.log(debugMessage);
    console.error(e.message);
    console.error(e.stack);
}

//select patient
let patientId = "882d06f1_7850_4061_93e8_258646c295e1";
let inputManagerId;

const selectInputManager = async () => {
    console.log("Requesting input manager id from registry");

    let services = await client.get('http://localhost:8083/api/registry/input-managers?roomId=room1&deviceIdInRoom=2');

    if (services.status !== 200 || services.data.length <= 0) {
        console.log("Unable to get input manager id");
        return;
    }

    inputManagerId = services.data[0].id;

    console.log(`Testing input manager with id: ${inputManagerId}`);
};

const testTac = async () => {
    console.log("########################### TAC ############################");
 
    let componentId = false;

    try {
        console.log("Creating a new tac component...");

        let componentInfo = await client.post(`/api/input-manager/${inputManagerId}/components`, {
            componentId: null,
            positions: [1],
            sourceType: "tac",
            patientId: patientId
        });

        componentId = componentInfo.data.component.componentId;
        let actionSearch = componentInfo.data.actions[0];

        await sleep(1000);

        console.log("Searching patient tac...");

        let selectTacActions = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${actionSearch.id}`, actionSearch);

        let getTac = selectTacActions.data[2];

        await sleep(1000);

        console.log("Requesting tac...");

        let nextTacAction = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${getTac.id}`, getTac);

        getTac = nextTacAction.data[2];

        await sleep(1000);

        console.log("Requesting next tac...");

        let result = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${getTac.id}`, getTac);

        printResponseStatus(result.status, "Tac test completed!", "Tac test failed!");
    } catch (e) {
        printException("Tac test failed!", e);
    } finally {

    }

    if (componentId) {
        let removeResult = await client.delete(`/api/input-manager/${inputManagerId}/components/${componentId}`);

        printResponseStatus(removeResult.status, "Tac component removed successfully.", "Unable to remove tac component, please do it manually.");
    }
};

const testVitalSigns = async () => {
    console.log("########################### VITAL SIGNS ############################");

    let componentId = false;

    try {
        console.log("Creating a new vital signs component...");

        let componentInfo = await client.post(`/api/input-manager/${inputManagerId}/components`, {
            componentId: null,
            positions: [2],
            sourceType: "vitalsigns",
            patientId: patientId
        });

        componentId = componentInfo.data.component.componentId;
        let selectAction = componentInfo.data.actions[0]; //0 select values, 1 show trend

        let selected = [];
        selected.push(selectAction.params[0].items[2]);
        selected.push(selectAction.params[0].items[4]);
        selected.push(selectAction.params[0].items[6]);

        selectAction.params[0].value = JSON.stringify(selected);

        await sleep(1000);

        console.log("Selecting specific values...");

        selectAction = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${selectAction.id}`, selectAction);

        let showTrendAction = selectAction.data[1]; //show trend
        showTrendAction.params[0].value = "20";

        await sleep(1000);

        console.log("Requesting trend...");

        let alternativeActions = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${showTrendAction.id}`, showTrendAction);

        let showInstantAction = alternativeActions.data[1]; //show instant

        await sleep(20000);

        console.log("Requesting instant...");

        let result = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${showInstantAction.id}`, showInstantAction);

        console.log("Waiting a while to see if the client receives data from the websocket... (20 sec)");

        await sleep(20000);

        printResponseStatus(result.status, "Vital signs test completed!", "Vital signs test failed!")
    } catch (e) {
        printException("Vital signs test failed!", e);
    }

    if (componentId) {
        let removeResult = await client.delete(`/api/input-manager/${inputManagerId}/components/${componentId}`);

        printResponseStatus(removeResult.status, "Vital signs component removed successfully.", "Unable to remove vital signs component, please do it manually.");
    }
};

const testVideoStreams = async () => {
    console.log("########################### VIDEO STREAMS ############################");

    let componentId = false;

    try {
        console.log("Creating a new video streams component...");

        let componentInfo = await client.post(`/api/input-manager/${inputManagerId}/components`, {
            componentId: null,
            positions: [3],
            sourceType: "videostreams",
            patientId: patientId
        });

        componentId = componentInfo.data.component.componentId;
        let selectAction = componentInfo.data.actions[0]; //0 select stream

        if (selectAction.params[0].items.length > 0) {
            selectAction.params[0].value = selectAction.params[0].items[0];
        }

        await sleep(1000);

        console.log("Selecting stream #0...");

        let alternativeActions = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${selectAction.id}`, selectAction);

        let playAction = alternativeActions.data[1]; //play

        await sleep(1000);

        console.log("Playing selected stream...");

        alternativeActions = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${playAction.id}`, playAction);

        let pauseAction = alternativeActions.data[1]; //pause

        console.log("Waiting a while to see if the client is able to play the stream... (20 sec)");

        await sleep(10000);

        console.log("Pausing selected stream...");

        let result = await client.post(`/api/input-manager/${inputManagerId}/components/${componentId}/actions/${pauseAction.id}`, pauseAction);

        await sleep(2000);

        printResponseStatus(result.status, "Video streams test completed!", "Video streams test failed!");

    } catch (e) {
        printException("Video streams test failed!", e);
    }

    if (componentId) {
        let removeResult = await client.delete(`/api/input-manager/${inputManagerId}/components/${componentId}`);

        printResponseStatus(removeResult.status, "Video streams component removed successfully.", "Unable to remove video streams component, please do it manually.");
    }
};

//main
console.log("input-mock started");
const client = axios.create({baseURL: 'http://localhost:8080'});

(async () => {

    await selectInputManager();

    await testTac();
    await sleep(1000);
    await testVitalSigns();
    await sleep(1000);
    await testVideoStreams();
})();
