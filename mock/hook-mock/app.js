const express = require('express');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

var router = express.Router();
router.post('/hook', i => {
    console.log("received: " + JSON.stringify(i.body));
})

app.use('/', router);

app.listen(8080, i => {
    console.log("Hook mock: initialized");
})

module.exports = app;
