package it.unibo.smartscreens.videostreams.source

import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import io.vertx.core.http.HttpServer
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.LoggerHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.awaitResult
import it.unibo.smartscreens.common.model.ServiceAddress
import it.unibo.smartscreens.common.msroute.HeartBeatRoute
import it.unibo.smartscreens.sources.common.clients.RegistryClient
import it.unibo.smartscreens.sources.common.extensions.asyncHandler
import it.unibo.smartscreens.sources.common.extensions.error
import it.unibo.smartscreens.sources.common.extensions.json
import it.unibo.smartscreens.sources.common.extensions.success
import it.unibo.smartscreens.sources.common.utils.ClientProvider
import it.unibo.smartscreens.sources.common.utils.ConfigReader
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper
import it.unibo.smartscreens.sources.common.utils.Scheduler
import it.unibo.smartscreens.videostreams.source.models.SourceServiceConfig
import it.unibo.smartscreens.videostreams.source.utils.DataFetcher

class VideoStreamsSourceVerticle : CoroutineVerticle() {

    //constants
    companion object {
        //configuration file names
        private const val SERVICE_CONFIG = "source/config.json"

        //path parameters
        private const val STREAM_PARAM_KEY = "streamId"
    }

//region fields
    //config
    private lateinit var verticleConfig: SourceServiceConfig //the verticle configuration

    //clients
    private lateinit var registryClient: RegistryClient //registry client
    private lateinit var dataFetcher: DataFetcher //manages data fetching from the vital sign microservice
//endregion

//region main methods
    override suspend fun start() {
        //load service config
        verticleConfig = ConfigReader.readConfig(jacksonTypeRef(), SERVICE_CONFIG)

        //schedule data fetching
        dataFetcher = DataFetcher(verticleConfig.fetcherConfig)

        //start http server
        val http = awaitResult<HttpServer> { vertx
            .createHttpServer()
            .requestHandler(createRouter())
            .listen(verticleConfig.port, verticleConfig.host, it)
        }

        //subscribe to registry
        registryClient = RegistryClient()
        val result = registryClient.registerService(
            ServiceAddress(verticleConfig.host, http.actualPort()),
            verticleConfig.info.name, verticleConfig.info.descr).await()

        println("[VideoStreamsSourceVerticle] Successfully registered to Registry (id: ${result.id})")

        println("[VideoStreamsSourceVerticle] Started at: ${verticleConfig.host}:${http.actualPort()}")
    }

    override suspend fun stop() {
        println("[VideoStreamsSourceVerticle] Stopped.")
    }
//endregion

//region handlers

    private suspend fun handleStreams(context: RoutingContext) {
        val streams = dataFetcher.getStreams().await()
        context.response().json(mapper.writeValueAsString(streams))
    }

    private suspend fun handleStream(context: RoutingContext) {
        val streamId = context.pathParam(STREAM_PARAM_KEY)
        val streamInfo = dataFetcher.getStream(streamId).await()
        context.response().json(mapper.writeValueAsString(streamInfo))
    }
//endregion

//region helpers
    /**
     * Creates the verticle http server router
     */
    private fun createRouter(): Router {
        val router = Router.router(vertx)
        router.route().handler(BodyHandler.create())
        router.route().handler(LoggerHandler.create())

        //default routes
        router.get(HeartBeatRoute.API_HEARTBEAT).handler { it.response().success() }

        //stream routes
        router.get("${verticleConfig.baseApiUrl}/streams").asyncHandler { handleStreams(it)}
        router.get("${verticleConfig.baseApiUrl}/streams/:$STREAM_PARAM_KEY").asyncHandler { handleStream(it)}

        return router
    }
//endregion
}
