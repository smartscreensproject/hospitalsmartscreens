package it.unibo.smartscreens.videostreams.manager.models

data class ManagerServiceConfig(var host: String,
                                var port: Int,
                                var baseApiUrl: String)
