package it.unibo.smartscreens.videostreams.manager.models

import it.unibo.smartscreens.videostreams.common.models.StreamInfo

class ManagerSession {
    /**
     * The current selected stream information.
     */
    var selectedStream: StreamInfo? = null
    /**
     * The current player status.
     */
    var playerStatus: PlayerStatus = PlayerStatus.PLAYING
}
