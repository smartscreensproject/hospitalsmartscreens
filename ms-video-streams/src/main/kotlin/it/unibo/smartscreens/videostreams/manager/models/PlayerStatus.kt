package it.unibo.smartscreens.videostreams.manager.models

/**
 * The enumeration of the possible stream status.
 */
enum class PlayerStatus {
    PLAYING,
    PAUSED
}
