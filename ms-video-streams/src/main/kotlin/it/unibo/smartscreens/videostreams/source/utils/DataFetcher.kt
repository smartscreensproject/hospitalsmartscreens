package it.unibo.smartscreens.videostreams.source.utils

import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.Future
import it.unibo.smartscreens.sources.common.extensions.setFailure
import it.unibo.smartscreens.sources.common.extensions.success
import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.utils.ClientProvider.webClient
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper
import it.unibo.smartscreens.videostreams.common.models.StreamInfo

class DataFetcher(private val config: ServiceConfig) {

    companion object {
        private const val STREAM_ID_PARAM_KEY = "{monitorId}"
        private const val GET_STREAMS_API_URL = "/streams"
        private const val GET_STREAM_API_URL = "$GET_STREAMS_API_URL/$STREAM_ID_PARAM_KEY"

        //remote service root url
        private const val SERVICE_ROOT_URL = "http://%s:%d%s"
    }

    /**
     * Get the available streams.
     */
    fun getStreams(): Future<List<StreamInfo>> {
        val future = Future.future<List<StreamInfo>>()
        webClient.get(config.port, config.address, GET_STREAMS_API_URL).send {
            if (it.success()) {
                val streams = mapper.readValue<List<StreamInfo>>(it.result().bodyAsString())
                streams.forEach { i -> i.manifest = generateAbsoluteManifestUrl(i.manifest) }
                future.complete(streams)
            } else {
                it.setFailure(future)
            }
        }
        return future
    }

    /**
     * Gets a specific stream.
     */
    fun getStream(id: String): Future<StreamInfo> {
       val future = Future.future<StreamInfo>()
        webClient.get(config.port, config.address, GET_STREAM_API_URL.replace(STREAM_ID_PARAM_KEY, id)).send {
           if (it.success()) {
               val stream = mapper.readValue<StreamInfo>(it.result().bodyAsString())
               stream.manifest = generateAbsoluteManifestUrl(stream.manifest)
               future.complete(stream)
           } else {
               it.setFailure(future)
           }
       }
       return future
   }

    /**
     * Generates the absolute manifest url from the relative url.
     */
    private fun generateAbsoluteManifestUrl(url: String): String {
        return SERVICE_ROOT_URL.format(config.address, config.port, url)
    }
}
