package it.unibo.smartscreens.videostreams.source.models

import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.models.config.ServiceInfo

data class SourceServiceConfig(var host: String,
                               var port: Int,
                               var baseApiUrl: String,
                               var info: ServiceInfo,
                               var fetcherConfig: ServiceConfig)
