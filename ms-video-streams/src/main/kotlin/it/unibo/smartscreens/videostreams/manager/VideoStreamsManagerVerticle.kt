package it.unibo.smartscreens.videostreams.manager

import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.convertValue
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServer
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.LoggerHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.awaitResult
import it.unibo.smartscreens.common.constant.NameService
import it.unibo.smartscreens.common.model.Action
import it.unibo.smartscreens.common.model.ActionParam
import it.unibo.smartscreens.common.model.ComponentInitParams
import it.unibo.smartscreens.common.msroute.HeartBeatRoute
import it.unibo.smartscreens.common.verticles.BaseVerticle
import it.unibo.smartscreens.sources.common.clients.RegistryClient
import it.unibo.smartscreens.sources.common.extensions.*
import it.unibo.smartscreens.sources.common.models.UpdateDataContent
import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.utils.ClientProvider
import it.unibo.smartscreens.sources.common.utils.ConfigReader
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper
import it.unibo.smartscreens.videostreams.manager.models.ManagerServiceConfig
import it.unibo.smartscreens.videostreams.manager.models.ManagerSession
import it.unibo.smartscreens.videostreams.manager.models.PlayerStatus
import it.unibo.smartscreens.videostreams.manager.utils.SourceClient

class VideoStreamsManagerVerticle : CoroutineVerticle() {

    //constants
    companion object {
        //configuration file names
        private const val SERVICE_CONFIG = "manager/config.json"

        //default base api url
        private const val BASE_API_URL = "/api"

        //action params names
        private const val SELECTED_STREAM_PARAM_NAME = "selectStream"

        //update data viewer name
        private const val STREAMS_LIST_VIEWER_NAME = "videostreams-list"
        private const val SELECT_STREAM_VIEWER_NAME = "videostreams-select"
        private const val PLAYER_ACTION_VIEWER_NAME = "videostreams-player"

        //the stream status property name
        private const val STREAM_STATUS_PROPERTY_NAME = "playing"
    }

    //region fields
    //config
    private lateinit var verticleConfig: ManagerServiceConfig //the verticle configuration
    private lateinit var componentParams: ComponentInitParams //the service configuration

    //session
    private var currSession: ManagerSession = ManagerSession()

    //clients and connection
    private lateinit var sourceClient: SourceClient //the http webClient to make requests to the source
    private lateinit var registryClient: RegistryClient //the webClient to contact the registry
    //endregion

    //region main functions
    override suspend fun start() {

        //overwrite host and port if parent sent a configuration during deployment
        verticleConfig = if (!config.isEmpty) {
            if (config.containsKey(BaseVerticle.ADDRESS_REF) && config.containsKey(BaseVerticle.PORT_REF)) {
                ManagerServiceConfig(config.getString(BaseVerticle.ADDRESS_REF), config.getInteger(BaseVerticle.PORT_REF), BASE_API_URL)
            } else throw IllegalArgumentException("Input configuration is malformed or empty, unable to setup verticle")
        } else {
            //load verticle configuration from resources
            ConfigReader.readConfig(jacksonTypeRef(), SERVICE_CONFIG)
        }

        //initialize registry client
        try {
            registryClient = RegistryClient()
            //fetch vital signs source service info
            val sourceInfo = registryClient.getServicesByName(NameService.VIDEO_STREAMS_SOURCE).await().first()
            //initialize source client
            sourceClient = SourceClient(ServiceConfig(sourceInfo.serviceAddress.address, sourceInfo.serviceAddress.port))
        } catch (e: Throwable) {
            throw IllegalStateException("Unable to retrieve video streams source service information from registry", e)
        }

        //start http server
        val http = awaitResult<HttpServer> { vertx
            .createHttpServer()
            .requestHandler(createRouter())
            .listen(verticleConfig.port, verticleConfig.host, it)
        }

        println("[VideoStreamsManagerVerticle] Started at: ${verticleConfig.host}:${http.actualPort()}")
    }

    override suspend fun stop() {
        println("[VideoStreamsManagerVerticle] Stopped.")
    }
    //endregion

    //region handlers
    private suspend fun handleInit(context: RoutingContext) {
        componentParams = mapper.readValue(context.bodyAsString)

        //TODO: the patient is not currently mapped to any stream, it will be ignored for now

        //fetch available streams
        val availableStreams = sourceClient.getStreams().await()

        //send streams info to client
        val response = sendUpdateDataMessage(availableStreams)

        //check response
        if (response.success()) {
            //reply with the new action
            context.response().json(mapper.writeValueAsString(createActions()))
        } else {
            //fail
            context.response().error()
        }
    }

    private fun handleDestroy(context: RoutingContext) {
        val stopFuture = Future.future<Void>()
        this.stop(stopFuture)
        if (stopFuture.success()) {
            context.response().success()
        } else {
            context.response().error(stopFuture.cause())
        }
    }

    private suspend fun handleGetActions(context: RoutingContext) {
        context.response().json(mapper.writeValueAsString(createActions()))
    }

    private suspend fun handleSelectStream(context: RoutingContext) {
        val body = context.bodyAsJson
        if (!body.containsKey(SELECTED_STREAM_PARAM_NAME)) {
            context.response().badRequest()
            return
        }

        //parse selected stream
        val selectedStreamId = body.getString(SELECTED_STREAM_PARAM_NAME, "-1")
        currSession.selectedStream = sourceClient.getStream(selectedStreamId.toInt()).await()
        currSession.playerStatus = PlayerStatus.PLAYING //reset player status

        //send stream info to client
        val response = sendUpdateDataMessage(currSession.selectedStream!!, PLAYER_ACTION_VIEWER_NAME)

        //check response
        if (response.success()) {
            //reply with the new action
            context.response().json(mapper.writeValueAsString(createActions()))
        } else {
            //fail
            context.response().error()
        }
    }

    private suspend fun handlePlayStream(context: RoutingContext) {
        currSession.selectedStream?.let {
            executePlayerAction(context, PlayerStatus.PLAYING)
        } ?: context.response().error(Error("Cannot execute player 'play' action if no stream is selected"))
    }

    private suspend fun handlePauseStream(context: RoutingContext) {
        currSession.selectedStream?.let {
            executePlayerAction(context, PlayerStatus.PAUSED)
        } ?: context.response().error(Error("Cannot execute player 'pause' action if no stream is selected"))
    }
    //endregion

    //region actions
    /**
     * Creates possible actions relative to the current session state
     */
    private suspend fun createActions(): List<Action> {
        val actions = ArrayList<Action>()

        //fetch available streams
        val availableStreams = sourceClient.getStreams().await()

        //create action to select video stream to play
        val selectableStreams = availableStreams.filter { i -> i.id != currSession.selectedStream?.id }.map { it.id.toString() }
        actions.add(Action("selectStream", "Choose video stream", "Choose the video stream to play",
            "${verticleConfig.baseApiUrl}/actions/select-stream", "post",
            listOf(ActionParam(SELECTED_STREAM_PARAM_NAME, "The video stream to play", "int", "", selectableStreams, true))))

        //create actions to play or pause the stream
        currSession.selectedStream?.let {
            val playAction = Action("playStream", "Play stream", "Play the current selected stream",
                "${verticleConfig.baseApiUrl}/actions/play-stream", "post", null)
            val pauseAction = Action("pauseStream", "Pause stream", "Pauses the current selected stream",
                    "${verticleConfig.baseApiUrl}/actions/pause-stream", "post", null)

            when(currSession.playerStatus) {
                PlayerStatus.PLAYING -> actions.add(pauseAction)
                PlayerStatus.PAUSED -> actions.add(playAction)
            }
        }

        return actions
    }
    //endregion

    //region helpers
    /**
     * Creates the verticle http server router
     */
    private fun createRouter(): Router {
        val router = Router.router(vertx)
        router.route().handler(BodyHandler.create())
        router.route().handler(LoggerHandler.create())

        //default routes
        router.get(HeartBeatRoute.API_HEARTBEAT).handler { it.response().success() }

        //main routes
        router.post("${verticleConfig.baseApiUrl}/init").asyncHandler { handleInit(it) }
        router.post("${verticleConfig.baseApiUrl}/destroy").handler { handleDestroy(it) }

        //actions
        router.get("${verticleConfig.baseApiUrl}/actions").asyncHandler { handleGetActions(it) }
        router.post("${verticleConfig.baseApiUrl}/actions/select-stream").asyncHandler { handleSelectStream(it) }
        router.post("${verticleConfig.baseApiUrl}/actions/play-stream").asyncHandler { handlePlayStream(it) }
        router.post("${verticleConfig.baseApiUrl}/actions/pause-stream").asyncHandler { handlePauseStream(it) }

        return router
    }

    /**
     * Sends a well formatted update data message to the client.
     */
    private suspend fun sendUpdateDataMessage(data: Any, viewer: String? = null): HttpResponse<Buffer> {
        val endpoint = componentParams.updateDataEndpoint

        //set viewer name accordingly
        var viewerName = if (currSession.selectedStream == null) STREAMS_LIST_VIEWER_NAME else PLAYER_ACTION_VIEWER_NAME
        viewer?.let {
            viewerName = it
        }

        //TODO: temp fix because smart screen do not understand arrays
        val json = mapper.createObjectNode()
        json.set("streams", mapper.convertValue(data))

        val content = UpdateDataContent(componentParams.componentId, viewerName, json/*mapper.convertValue(data)*/)
        return awaitResult { handler ->
            ClientProvider.webClient
                .post(endpoint.port, endpoint.address, endpoint.route)
                .sendJson(content, handler)
        }
    }

    /**
     * Executes the requested player action.
     */
    private suspend fun executePlayerAction(context: RoutingContext, newStatus: PlayerStatus) {
        //update stream status
        currSession.playerStatus = newStatus

        //generate stream info object and set stream status property
        val streamInfo: ObjectNode = mapper.convertValue(currSession.selectedStream!!)
        streamInfo.put(STREAM_STATUS_PROPERTY_NAME, newStatus == PlayerStatus.PLAYING)

        //send stream info to client
        val response = sendUpdateDataMessage(streamInfo)

        //check response
        if (response.success()) {
            //reply with the new action
            context.response().json(mapper.writeValueAsString(createActions()))
        } else {
            //fail
            context.response().error()
        }
    }
    //endregion
}
