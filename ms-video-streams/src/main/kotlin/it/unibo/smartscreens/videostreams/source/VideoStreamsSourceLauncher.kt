package it.unibo.smartscreens.videostreams.source

import io.vertx.core.Vertx

fun main() {
    Vertx.vertx().deployVerticle(VideoStreamsSourceVerticle()) {
        if (it.succeeded()) {
            println("The verticle has been successfully deployed, with id: " + it.result())
        } else {
            println("Error during deployment: " + it.cause().message)
        }
    }
}
