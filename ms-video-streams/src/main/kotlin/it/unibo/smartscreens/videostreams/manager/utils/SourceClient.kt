package it.unibo.smartscreens.videostreams.manager.utils

import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.Future
import it.unibo.smartscreens.sources.common.extensions.setFailure
import it.unibo.smartscreens.sources.common.extensions.success
import it.unibo.smartscreens.sources.common.models.config.ServiceConfig
import it.unibo.smartscreens.sources.common.utils.ClientProvider.webClient
import it.unibo.smartscreens.sources.common.utils.MapperProvider.mapper
import it.unibo.smartscreens.videostreams.common.models.StreamInfo

class SourceClient(private val config: ServiceConfig) {

    companion object {
        //path parameters
        private const val STREAM_PARAM_KEY = "{patientId}"

        //routes
        private const val GET_STREAMS_API_URL = "/api/streams"
        private const val GET_STREAM_API_URL = "/api/streams/$STREAM_PARAM_KEY"
    }

    fun getStreams(): Future<List<StreamInfo>> {
        val future = Future.future<List<StreamInfo>>()
        webClient.get(config.port, config.address, GET_STREAMS_API_URL).send {
            if (it.success()) {
                future.complete(mapper.readValue<List<StreamInfo>>(it.result().bodyAsString()))
            } else {
                it.setFailure(future)
            }
        }
        return future
    }

    fun getStream(streamId: Int): Future<StreamInfo> {
        val future = Future.future<StreamInfo>()
        webClient.get(config.port, config.address, GET_STREAM_API_URL.replace(STREAM_PARAM_KEY, streamId.toString())).send {
            if (it.success()) {
                future.complete(mapper.readValue<StreamInfo>(it.result().bodyAsString()))
            } else {
                it.setFailure(future)
            }
        }
        return future
    }
}
