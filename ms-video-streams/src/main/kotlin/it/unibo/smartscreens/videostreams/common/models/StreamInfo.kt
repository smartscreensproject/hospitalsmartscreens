package it.unibo.smartscreens.videostreams.common.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.Duration
import java.util.*

data class StreamInfo(var id: Int,
                      var title: String,
                      var descr: String,
                      @JsonFormat(shape=JsonFormat.Shape.STRING) var duration: Duration,
                      var streamType: String,
                      var manifest: String,
                      var live: Boolean,
                      var currentTime: Int,
                      @JsonFormat(shape=JsonFormat.Shape.STRING) var creationDate: Date)
