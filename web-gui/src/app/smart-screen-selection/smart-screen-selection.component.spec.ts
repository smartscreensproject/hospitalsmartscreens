import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartScreenSelectionComponent } from './smart-screen-selection.component';

describe('SmartScreenSelectionComponent', () => {
  let component: SmartScreenSelectionComponent;
  let fixture: ComponentFixture<SmartScreenSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartScreenSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartScreenSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
