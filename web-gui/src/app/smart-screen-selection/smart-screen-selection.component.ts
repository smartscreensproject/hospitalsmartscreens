import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service';
import { ApiGatewayCommunicationService } from '../input/api-gateway-communication.service';
import { Router } from '@angular/router';
import { Paths } from '../routes-paths';

@Component({
  selector: 'app-smart-screen-selection',
  templateUrl: './smart-screen-selection.component.html',
  styleUrls: ['./smart-screen-selection.component.scss']
})
export class SmartScreenSelectionComponent implements OnInit {

  displayedColumns: string[] = ['roomId', 'deviceIdInRoom', 'selection'];
  availableSmartScreens: Service[];

  constructor(private agcService: ApiGatewayCommunicationService,
              private router: Router) {
  }

  ngOnInit() {
    this.agcService.getAvailableSmartScreens().then(response => {
      console.log(response);
      this.availableSmartScreens = response;
      const compareFun = (a: Service, b: Service) => {
        const aRoomId = a.roomId;
        const bRoomId = b.roomId;
        if (aRoomId < bRoomId) {
          return 1;
        } else if (aRoomId > bRoomId) {
          return -1;
        } else {
          return 0;
        }
      };
      this.availableSmartScreens.sort((a, b) => compareFun(a, b));
    });
  }

  selectSmartScreen(idx: number) {
    const selectedOutputManager = this.availableSmartScreens[idx];
    const serviceAddress = selectedOutputManager.serviceAddress;
    const extras = {
      'id': selectedOutputManager.id,
      'port': serviceAddress.port,
      'address': serviceAddress.address
    };
    this.router.navigate([`/${Paths.DataDisplay.valueOf()}`, extras]).catch(console.log);
  }
}
