import { ElementRef, Injectable } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WindowResizeListenerService {

  private resizeSubject: Subject<Window>;

  constructor(private eventManager: EventManager) {
    this.resizeSubject = new Subject<Window>();
    this.eventManager.addGlobalEventListener('window', 'resize', this.onResize.bind(this));
  }

  private onResize(event: UIEvent) {
    this.resizeSubject.next(event.target as Window);
  }

  /**
   * Gets the current window width
   */
  get currentWindowWidth(): number {
    return window.innerWidth;
  }

  /**
   * Returns true if the width is <= 600
   */
  get isMobile(): boolean {
    return window.innerWidth <= 600;
  }

  /**
   * Returns true if the width is <= 768
   */
  get isTablet(): boolean {
    return window.innerWidth <= 768;
  }

  /**
   * Returns a observable listening to window resize event
   */
  get onResize$(): Observable<Window> {
    return this.resizeSubject.asObservable();
  }

  /**
   *
   * @param element
   * @param debug
   */
  getElementHeigth(element, debug = false): number {
    const elementComputedStyle = getComputedStyle(element);
    const height = parseFloat(elementComputedStyle.height);
    const marginTop = parseFloat(elementComputedStyle.marginTop);
    const marginBottom = parseFloat(elementComputedStyle.marginBottom);
    if (debug) {
      console.log('Height ' + height);
      console.log('Margin top ' + marginTop);
      console.log('Margin bottom ' + marginBottom);
    }
    return height + marginTop + marginBottom;
  }

  /**
   *
   * @param element
   * @param debug
   */
  getElementWidth(element, debug = false): number {
    const elementComputedStyle = getComputedStyle(element);
    const width = parseFloat(elementComputedStyle.width);
    const marginLeft = parseFloat(elementComputedStyle.marginLeft);
    const marginRight = parseFloat(elementComputedStyle.marginRight);
    if (debug) {
      console.log('Width ' + width);
      console.log('Margin left ' + marginLeft);
      console.log('Margin right ' + marginRight);
    }
    // TODO: check if works correctly
    return width + marginLeft + marginRight;
  }

  /**
   * Gets the specified html selector height.
   * Use only after the component content has been rendered
   * @param selector the html selector
   * @param debug set to true to log the computed values
   */
  getHtmlSelectorHeight(selector: string, debug: boolean = false): number {
    const element = document.querySelector(selector);
    if (debug) {
      console.log(selector);
    }
    return this.getElementHeigth(element, debug);
  }

  /**
   * Gets the specified html selector width.
   * Use only after the component content has been rendered
   * @param selector the html selector
   * @param debug set to true to log the computed values
   */
  getHtmlSelectorWidth(selector: string, debug: boolean = false): number {
    const element = document.querySelector(selector);
    if (debug) {
      console.log(selector);
    }
    return this.getElementWidth(element, debug);
  }

  /**
   * Gets the height of the closest html selector to the specified elementRef.
   * Use only after the component content has been rendered
   * @param elementRef
   * @param selector
   * @param debug
   */
  getClosestElementHeight(elementRef: ElementRef, selector: string, debug: boolean = false): number {
    const element = elementRef.nativeElement.closest(selector);
    if (debug) {
      console.log(selector);
    }
    return this.getElementHeigth(element, debug);
  }

  /**
   * Gets the width of the closest html selector to the specified elementRef.
   * Use only after the component content has been rendered
   * @param elementRef
   * @param selector
   * @param debug
   */
  getClosestElementWidth(elementRef: ElementRef, selector: string, debug: boolean = false): number {
    const element = elementRef.nativeElement.closest(selector);
    if (debug) {
      console.log(selector);
    }
    return this.getElementWidth(element, debug);
  }
}
