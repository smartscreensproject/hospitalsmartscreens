import { AfterViewInit, Component, Input } from '@angular/core';
import { Chart } from 'chart.js';
import { ChartElement } from '../../models/chart-element';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements AfterViewInit {

  @Input() chartId: string;
  @Input() ariaLabel: string;
  @Input() yAxisLabel: string[] = [];
  chartComputedWidth = '400px';
  chartComputedHeight = '400px';
  dataSetMaxSize: number = 20;
  chart: Chart;
  dataSetList: ChartElement[] = [];
  // should have the same size of the dataSetList,
  // otherwise the chart wont show all it's values
  xAxisLabels: String[] = [];

  ngAfterViewInit() {
    this.chart = new Chart(this.chartId, {
      type: 'line',
      data: {
        labels: this.xAxisLabels,
        datasets: this.dataSetList
      },
      options: {
        maintainAspectRatio: false,
        // removes dots from chart
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: this.yAxisLabel
            }
          }]
        }
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    });
  }

  /**
   * Adds a new dataset to the chart
   * @param dataSet the dataset to add
   */
  addDataSet(dataSet: ChartElement) {
    if (this.dataSetList.find(ce => ce.label === dataSet.label)) {
      console.log('Can\'t add a dataset with the same label of an existing one');
    } else {
      this.dataSetList.push(dataSet);
      this.removeOldestValues(this.dataSetList[this.dataSetList.length - 1]);
      this.levelXAxisLabels();
    }
    this.updateChart();
  }

  /**
   *
   * @param label
   */
  removeDataSet(label: string) {
    this.dataSetList = this.dataSetList.filter(ce => ce.label !== label);
    this.updateChart();
  }

  /**
   * Updates the charts values
   */
  updateChart() {
    this.chart.update();
  }

  /**
   * Updates the dataset at the specified index with a new value,
   * removing the oldest one if its length exceeds the max one
   * @param idx the dataset index
   * @param value the new value to add
   */
  updateDataSetAt(idx: number, value: number) {
    if (idx < this.dataSetList.length) {
      this.addValue(this.dataSetList[idx], value);
    } else {
      console.log('warning: the index ' + idx + ' does not exist in the dataSetList');
    }
    this.updateChart();
  }

  /**
   * Updates the dataset with the specified label with a new value,
   * removing the oldest one if its length exceeds the max one
   * @param label the dataset label
   * @param value the new value to add
   */
  updateDataSetWithLabel(label: string, value: number) {
    const dataSet = this.dataSetList.find(ce => ce.label === label);
    if (dataSet) {
      this.addValue(dataSet, value);
    } else {
      console.log('warning: the dataset with label ' + label + ' does not exist in the dataSetList');
    }
    this.updateChart();
  }

  /**
   * Updates the specified dataset with a new value,
   * removing the oldest one if its length exceeds the max one
   * @param dataSet the dataset to update
   * @param value the new value to add
   */
  private addValue(dataSet: ChartElement, value: number) {
    dataSet.data.push(value);
    this.removeOldestValues(dataSet);
    this.levelXAxisLabels();
  }

  /**
   * Removes the oldest dataset values when its data field exceeds the dataSetMaxSize
   * @param dataSet the dataset
   */
  private removeOldestValues(dataSet: ChartElement) {
    while (dataSet.data.length > this.dataSetMaxSize && dataSet.data.length > 0) {
      dataSet.data.shift();
    }
  }

  private levelXAxisLabels() {
    // TODO: this gets computed each time, a smarter way would be storing the max value and updating it when necessary
    const chartDataLengthList = this.dataSetList.map(ce => ce.data.length);
    const maxLength = Math.max(...chartDataLengthList);
    while (this.xAxisLabels.length != maxLength) {
      if (this.xAxisLabels.length > maxLength) {
        this.xAxisLabels.pop();
      } else {
        this.xAxisLabels.push('');
      }
    }
  }

  setChartSize(width: number, height: number) {
    this.chartComputedWidth = this.toPixel(width);
    this.chartComputedHeight = this.toPixel(height);
  }

  toPixel(value: number): string {
    return value + 'px';
  }
}
