import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ComponentType } from '@angular/cdk/portal';
import { DefaultDialogConfigImpl, DialogConfig, DialogConfigImpl } from '../models/dialog-config';


@Injectable({
  providedIn: 'root'
})
export class DialogService {

  defaultConfig: DialogConfig = new DefaultDialogConfigImpl();

  constructor(public dialog: MatDialog) {
  }

  /**
   * Opens a dialog using the specified componentRef template
   * @param componentRef the component ref template to display
   * @param config the config used by the dialog
   */
  open<T>(componentRef: ComponentType<T>, config: DialogConfig = this.defaultConfig): MatDialogRef<T> {
    return this.dialog.open(componentRef, config);
  }

  /**
   * Shortcut to get a dialog close event as a promise from a dialog ref
   * @param dialogRef the dialog ref
   */
  getClosureEvent<T>(dialogRef: MatDialogRef<T>): Promise<any> {
    return dialogRef.afterClosed().toPromise();
  }

  /**
   * Checks that the dialog result id != null and != by all the specified not valid values
   * @param result the result to check
   * @param notValidValues the values it should be differ to be valid
   */
  checkResult(result, ...notValidValues): boolean {
    return result != null && notValidValues.every(notValid => result !== notValid);
  }
}
