import { Injectable, OnDestroy } from '@angular/core';
import { WebSocketService } from './web-socket.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SockJsService extends WebSocketService implements OnDestroy {

  constructor() {
    super();
  }

  /**
   * Connects to SockJs websocket using standard websocket
   * Note: if the SockJs url is "http://127.0.0.1:9999/echo"
   * the url to connect will be "ws://127.0.0.1:9999/echo"
   *
   * @param url the SockJs url
   */
  connect(url: string): Subject<MessageEvent> {
    return super.connect(url + '/websocket');
  }
}
