import { Uri, UriImpl } from './uri';

export class ServicesRestApi {
  // TODO: remember to change the registry url address
  registryUrl: Uri = new UriImpl('http://localhost:8083');
  smartScreenUri: Uri = this.registryUrl.addPath('api/registry/smart-screens');
  inputManagersUri: Uri = this.registryUrl.addPath('api/registry/input-managers');
  //inputStatePath: string = 'api/input-manager/inputManagerId/input-state';
  availableServicesPath: string = 'api/registry/services';
}
