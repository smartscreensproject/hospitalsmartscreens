import { TestBed } from '@angular/core/testing';

import { SockJsService } from './sock-js.service';

describe('SockJsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SockJsService = TestBed.get(SockJsService);
    expect(service).toBeTruthy();
  });
});
