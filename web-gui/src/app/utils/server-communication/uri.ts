export interface Uri {

  /**
   * Returns the string value of the path
   */
  getPath(): string;

  /**
   * Adds the specified path to the current one.
   * N.B. it will be preceded by /
   * @param path
   */
  addPath(path: string): Uri;
}

export class UriImpl implements Uri {
  private readonly path: string;

  constructor(path: string) {
    this.path = path;
  }

  getPath(): string {
    return this.path;
  }

  addPath(path: string): Uri {
    return new UriImpl(this.path + '/' + path);
  }
}

export class AddressPortUriImpl extends UriImpl {

  constructor(address: string, port: string) {
    super(`http://${address}:${port}`);
  }

}
