import { TestBed } from '@angular/core/testing';

import { WindowResizeListenerService } from './window-resize-listener.service';

describe('WindowResizeListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WindowResizeListenerService = TestBed.get(WindowResizeListenerService);
    expect(service).toBeTruthy();
  });
});
