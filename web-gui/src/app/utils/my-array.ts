declare global {
  interface Array<T> {
    sortNumerically(): Array<T>;
  }
}

Array.prototype.sortNumerically = function () {
  return this.sort((a, b) => a - b);
};

export {};
