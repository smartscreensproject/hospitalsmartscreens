import { ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { WindowResizeListenerService } from './window-resize-listener.service';

export class ResizeListener implements OnDestroy {

  protected resizeSubscription: Subscription;

  /**
   * Shortcut to set a function to call each time the window is resized.
   * This method should not be called before the AfterViewInit lifecycle hook
   * @param wrlService the window resize listener service instance
   * @param changeDetectorRef the change detector ref instance necessary to avoid ExpressionChangedAfterItHasBeenCheckedError
   * @param functionToCallOnResize function to call on resize
   * @param args the arguments necessary to functionToCallOnResize
   */
  protected onResize(wrlService: WindowResizeListenerService,
                     changeDetectorRef: ChangeDetectorRef,
                     functionToCallOnResize: (wrsService: WindowResizeListenerService,
                                              ...args: any[]) => void,
                     ...args: any[]) {
    // subscribes to the window resize event and calls each time the functionToCallOnResize
    this.resizeSubscription = wrlService.onResize$.subscribe(() => {
      functionToCallOnResize(wrlService, ...args);
    });
    // calls the functionToCallOnResize since the resize event
    // is not triggered when loading the page the first time
    functionToCallOnResize(wrlService, ...args);
    // avoids ExpressionChangedAfterItHasBeenCheckedError
    changeDetectorRef.detectChanges();
  }

  ngOnDestroy() {
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
  }
}
