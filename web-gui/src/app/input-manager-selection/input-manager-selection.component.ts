import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service';
import { Router } from '@angular/router';
import { ApiGatewayCommunicationService } from '../input/api-gateway-communication.service';
import { Paths } from '../routes-paths';

@Component({
  selector: 'app-output-manager-selection',
  templateUrl: './input-manager-selection.component.html',
  styleUrls: ['./input-manager-selection.component.scss']
})
export class InputManagerSelectionComponent implements OnInit {

  displayedColumns: string[] = ['roomId', 'deviceIdInRoom', 'selection'];
  availableInputManagers: Service[];

  constructor(private agcService: ApiGatewayCommunicationService,
              private router: Router) {
  }

  ngOnInit() {
    this.agcService.getAvailableInputManagers().then(response => {
      this.availableInputManagers = response;
      const compareFun = (a: Service, b: Service) => {
        const aRoomId = a.roomId;
        const bRoomId = b.roomId;
        if (aRoomId < bRoomId) {
          return 1;
        } else if (aRoomId > bRoomId) {
          return -1;
        } else {
          return 0;
        }
      };
      this.availableInputManagers.sort((a, b) => compareFun(a, b));
    });
  }

  selectOutputManager(idx: number) {
    const selectedInputManager = this.availableInputManagers[idx];
    const serviceAddress = selectedInputManager.serviceAddress;
    const extras = {
      'id': selectedInputManager.id,
      'port': serviceAddress.port,
      'address': serviceAddress.address
    };
    this.router.navigate([`/${Paths.DataSelection.valueOf()}`, extras]).catch(console.log);
  }

}
