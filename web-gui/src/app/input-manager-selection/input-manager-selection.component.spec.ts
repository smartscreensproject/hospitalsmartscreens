import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputManagerSelectionComponent } from './input-manager-selection.component';

describe('InputManagerSelectionComponent', () => {
  let component: InputManagerSelectionComponent;
  let fixture: ComponentFixture<InputManagerSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputManagerSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputManagerSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
