import { Type, ViewChild } from '@angular/core';
import { DataComponent } from './grid-layout/data.component';
import { GridLayoutComponent } from './grid-layout/grid-layout.component';
import { Tile } from './models/tile';

export abstract class GridContainerComponent {

  @ViewChild(GridLayoutComponent)
  protected gridLayoutComponent: GridLayoutComponent;

  /**
   * Used to retrieve the correct component to dynamically load inside
   * a grid tile. Remember to add the Component declaration inside the
   * entryComponents field of app.module.ts
   * @param tile the tile which infos will be used to determine the correct component to load
   */
  abstract componentResolver(tile: Tile): Type<DataComponent>;

}
