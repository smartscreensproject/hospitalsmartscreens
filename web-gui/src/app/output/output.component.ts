import { AfterViewInit, Component, OnDestroy, Type } from '@angular/core';
import { SocketMessageType } from '../models/socket-message-type';
import { NotificationSnackbarComponent } from '../utils/notification-snackbar/notification-snackbar.component';
import { SmartScreenEventsListenerService } from './smart-screen-events-listener.service';
import { MatSnackBar } from '@angular/material';
import { InstantVitalSignsComponent } from './instant-vital-signs/instant-vital-signs.component';
import { TacDetailComponent } from './tac-detail/tac-detail.component';
import { SelectDataSourceComponent } from './select-data-source/select-data-source.component';
import { DataComponent } from '../grid-layout/data.component';
import { GridContainerComponent } from '../grid-container.component';
import { ChartVitalSignsComponent } from './chart-vital-signs/chart-vital-signs.component';
import { OutputVisualizers } from './output-visualizers';
import { TacSelectionComponent } from './tac-selection/tac-selection.component';
import { VideoStreamPlayerComponent } from './video-stream-player/video-stream-player.component';
import { VideoStreamSelectionComponent } from './video-stream-selection/video-stream-selection.component';
import { ScreenState } from '../models/screen-state';
import { Tile } from '../models/tile';
import { ActivatedRoute } from '@angular/router';
import { DataEmitted } from '../models/data-emitted';
import { GridLayoutComponent } from '../grid-layout/grid-layout.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.scss']
})
export class OutputComponent extends GridContainerComponent implements AfterViewInit, OnDestroy {

  socketMessagesSubscription: Subscription;
  socketErrorsSubscription: Subscription;

  constructor(private smartScreenEventsListenerService: SmartScreenEventsListenerService,
              private snackBar: MatSnackBar,
              public route: ActivatedRoute) {
    super();
  }

  ngAfterViewInit() {
    // connects to the smart screen ws
    this.smartScreenEventsListenerService.setupAndConnect(this.route);
    // listens the socket messages
    this.socketMessagesSubscription = this.smartScreenEventsListenerService.socketMessageReceived$.subscribe(data => {
      this.switchMessageType(data);
    });
    // listens for socket errors and shows a popup
    this.socketErrorsSubscription = this.smartScreenEventsListenerService.socketError$.subscribe(() => {
      this.snackBar.openFromComponent(NotificationSnackbarComponent, {
        duration: 1500,
        data: 'websocket connection lost',
        // to define style this way it should be defined in global style.css
        // panelClass: ['custom-mat-snackbar'],
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });
    });
  }

  ngOnDestroy() {
    this.smartScreenEventsListenerService.ngOnDestroy();
    this.socketMessagesSubscription.unsubscribe();
    this.socketErrorsSubscription.unsubscribe();
  }

  componentResolver(tile: Tile): Type<DataComponent> {
    const viewer = tile.cell.viewer;
    let component;
    console.log('viewer: ' + viewer);
    switch (viewer) {
      case OutputVisualizers.VitalSignsInstant.valueOf():
        component = InstantVitalSignsComponent;
        break;
      case OutputVisualizers.VitalSignsTrend.valueOf():
        component = ChartVitalSignsComponent;
        break;
      case OutputVisualizers.TacSelection.valueOf():
        component = TacSelectionComponent;
        break;
      case OutputVisualizers.TacDetail.valueOf():
        component = TacDetailComponent;
        break;
      case OutputVisualizers.VideoStreamsSelection.valueOf():
        component = VideoStreamSelectionComponent;
        break;
      case OutputVisualizers.VideoStreamsPlayer.valueOf():
        component = VideoStreamPlayerComponent;
        break;
      default:
        component = SelectDataSourceComponent;
        break;
    }
    return component;
  }

  switchMessageType(data) {
    const jsonData = JSON.parse(data);
    const type = jsonData.type;
    let message;
    console.log('websocket received');
    console.log(jsonData);
    switch (type) {
      case SocketMessageType.ScreenState:
        const screenState: ScreenState = jsonData.screenState;
        this.gridLayoutComponent.resizeGrid(screenState);
        break;
      case SocketMessageType.AddComponent:
        console.log('add component');
        this.gridLayoutComponent.addComponent(jsonData.message);
        break;
      case SocketMessageType.RemoveComponent:
        console.log('remove component');
        message = jsonData.message;
        this.gridLayoutComponent.removeComponent(message.componentId, message.newAreas);
        break;
      case SocketMessageType.Clear:
        message = jsonData.message;
        console.log('clear');
        this.gridLayoutComponent.clear(message.newAreas);
        break;
      case SocketMessageType.UpdateData:
        console.log('update data');
        console.log(jsonData.message);
        message = jsonData.message;
        this.gridLayoutComponent.updateData(message.componentId, message.data);
        break;
      case SocketMessageType.ModifyViewer:
        console.log('update viewer');
        message = jsonData.message;
        console.log(message);
        console.log(message.viewer);
        this.gridLayoutComponent.modifyViewer(message.componentId, message.viewer, message.data);
        break;
      case SocketMessageType.Resize:
        console.log('resize');
        message = jsonData.message;
        this.gridLayoutComponent.resizeComponent(message.screenAreaUpdated, message.newScreenAreas);
        break;
      case SocketMessageType.Move:
        // TODO
        console.log('move');
        message = jsonData.message;
        break;
      case SocketMessageType.ChangeGrid:
        console.log('change grid size');
        console.log(jsonData.message);
        this.gridLayoutComponent.resizeGrid(jsonData.message);
        break;
    }
  }

  onEventEmitted(gridLayoutComponent: GridLayoutComponent, dataEmitted: DataEmitted) {
    // at the moment it does nothing since it should not be called
    console.log('unexpected call, implement the method');
    // throw new UnsupportedOperationError();
  };
}
