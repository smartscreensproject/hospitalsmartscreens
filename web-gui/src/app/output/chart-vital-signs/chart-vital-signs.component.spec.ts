import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartVitalSignsComponent } from './chart-vital-signs.component';

describe('ChartVitalSignsComponent', () => {
  let component: ChartVitalSignsComponent;
  let fixture: ComponentFixture<ChartVitalSignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartVitalSignsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartVitalSignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
