import { ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { VitalSign } from '../../models/vital-sign';
import { PatientData } from '../../models/patient-data';
import { ChartComponent } from '../../utils/chart/chart.component';
import { NoClickActionDataComponent } from '../../grid-layout/no-click-action-data.component';
import { ChartElementImpl } from '../../models/chart-element';
import { InitInfo } from '../../models/init-info';
import { WindowResizeListenerService } from '../../utils/window-resize-listener.service';

@Component({
  selector: 'app-chart-vital-signs',
  templateUrl: './chart-vital-signs.component.html',
  styleUrls: ['./chart-vital-signs.component.scss']
})
export class ChartVitalSignsComponent extends NoClickActionDataComponent {

  @ViewChild(ChartComponent) private chartComponent: ChartComponent;
  lineChartId = this.uuid;
  ariaLabel = 'Line chart';
  yAxisLabel = [''];
  representedDataTypes: Set<string> = new Set<string>();
  resizeArgs: any[];

  constructor(public elementRef: ElementRef,
              private wrsService: WindowResizeListenerService,
              private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.resizeArgs = [this.elementRef, this.matGridTileSelector, -15, this.chartComponent];
    this.onResize(this.wrsService, this.changeDetectorRef, this.updateChartSize, ...this.resizeArgs);
  }

  updateChartSize(wrsService: WindowResizeListenerService, ...args: any[]) {
    const elementRef: ElementRef = args[0];
    const selector = args[1];
    const adjustingFactor = args[2];
    const chartComponent: ChartComponent = args[3];
    // the elementRef is necessary to take the current area,
    // using getHtmlSelectorHeight is wrong since it will always
    // take the first selector (mat-grid-tile)
    const width = wrsService.getClosestElementWidth(elementRef, selector) + adjustingFactor;
    const height = wrsService.getClosestElementHeight(elementRef, selector) + adjustingFactor;
    chartComponent.setChartSize(width, height);
  }

  init(info: InitInfo) {
    console.log(info);
    let patientData: PatientData[] = info.data.trendData;
    this.chartComponent.dataSetMaxSize = info.data.recordsCount;

    if (patientData.length > 0) {
      const lastDataUpdate: VitalSign[] = patientData[patientData.length - 1].data;
      // creates an empty chart for each new type
      lastDataUpdate.forEach(vs => {
        if (!this.representedDataTypes.has(vs.type)) {
          this.representedDataTypes.add(vs.type);
        }
      });
      console.log(this.representedDataTypes);

      this.representedDataTypes.forEach(dt => {
        // gets an array of the specified type
        let data = patientData.flatMap(pd => {
          // finds patient data of the specified type
          const v = pd.data.find(vs => vs.type == dt).value;
          // converting the value to int
          const vToInt = parseInt(v);
          // if the parse result is a valid int returns it, otherwise returns 0
          return vToInt ? vToInt : 0;
        });
        if (data.length < 1) {
          data = [];
        }
        this.chartComponent.addDataSet(new ChartElementImpl(dt, data));
      });
    }
  }

  update(data) {
    this.chartComponent.dataSetMaxSize = data.recordsCount;
    const trendData: PatientData[] = data.trendData;
    // only the last update is considered
    const lastDataUpdate: VitalSign[] = trendData[trendData.length - 1].data;
    // removes non existing data types in the last data update
    this.representedDataTypes.forEach(dt => {
      if (!lastDataUpdate.map(vs => vs.type).includes(dt)) {
        this.chartComponent.removeDataSet(dt);
      }
    });
    // creates an empty chart for each new type
    lastDataUpdate.forEach(vs => {
      if (!this.representedDataTypes.has(vs.type)) {
        this.chartComponent.addDataSet(new ChartElementImpl(vs.type, []));
        this.representedDataTypes.add(vs.type);
      }
    });
    // TODO: add all the previous sent data for the new
    //  inserted type (not only the last sent value)
    // updates the chart values with the last data update
    this.representedDataTypes.forEach(type => {
      const vs = lastDataUpdate.find(v => v.type === type);
      this.chartComponent.updateDataSetWithLabel(type, parseInt(vs.value));
    });
  }

  layoutChanged() {
    this.updateChartSize(this.wrsService, ...this.resizeArgs);
  }
}
