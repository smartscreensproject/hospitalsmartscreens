import { TestBed } from '@angular/core/testing';

import { SmartScreenEventsListenerService } from './smart-screen-events-listener.service';

describe('SmartScreenEventsListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SmartScreenEventsListenerService = TestBed.get(SmartScreenEventsListenerService);
    expect(service).toBeTruthy();
  });
});
