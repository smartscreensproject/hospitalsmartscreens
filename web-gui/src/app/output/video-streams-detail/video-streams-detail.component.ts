import { Component } from '@angular/core';
import { Video } from '../../models/video';
import { log } from 'util';
import { NoClickActionDataComponent } from '../../grid-layout/no-click-action-data.component';
import { InitInfo } from '../../models/init-info';

@Component({
  selector: 'app-video-streams-detail',
  templateUrl: './video-streams-detail.component.html',
  styleUrls: ['./video-streams-detail.component.scss']
})
export class VideoStreamsDetailComponent extends NoClickActionDataComponent {

  videoDetail: Video;

  constructor() {
    super();
  }

  init(info: InitInfo) {
    log('init video stream select', info);
    this.videoDetail = info.data.streams;
  }

  update(data) {
    log('update video stream select', data);
    this.videoDetail = data.streams;
  }
}
