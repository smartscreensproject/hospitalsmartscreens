import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoStreamsDetailComponent } from './video-streams-detail.component';

describe('VideoStreamsDetailComponent', () => {
  let component: VideoStreamsDetailComponent;
  let fixture: ComponentFixture<VideoStreamsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoStreamsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoStreamsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
