export const enum OutputVisualizers {
  VitalSignsInstant = 'vitalsigns-instant',
  VitalSignsTrend = 'vitalsigns-trend',
  TacSelection = 'tac-selection',
  TacDetail = 'tac-detail',
  VideoStreamsSelection = 'videostreams-list',
  VideoStreamsDetail = 'videostreams-list',
  VideoStreamsPlayer = 'videostreams-player'
}
