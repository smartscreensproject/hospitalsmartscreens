import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantVitalSignsComponent } from './instant-vital-signs.component';

describe('InstantVitalSignsComponent', () => {
  let component: InstantVitalSignsComponent;
  let fixture: ComponentFixture<InstantVitalSignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantVitalSignsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantVitalSignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
