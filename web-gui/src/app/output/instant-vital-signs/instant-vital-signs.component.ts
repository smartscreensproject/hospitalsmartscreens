import { Component } from '@angular/core';
import { PatientData, PatientDataImpl } from '../../models/patient-data';
import { DataTimeComponent } from '../../grid-layout/data-time.component';
import { Tile } from '../../models/tile';
import { InitInfo } from '../../models/init-info';

@Component({
  selector: 'app-instant-data',
  templateUrl: './instant-vital-signs.component.html',
  styleUrls: ['./instant-vital-signs.component.scss']
})
export class InstantVitalSignsComponent extends DataTimeComponent {

  displayedColumns: string[] = ['type', 'value', 'uom'];
  patientData: PatientData = new PatientDataImpl();

  constructor() {
    super();
  }

  init(info: InitInfo) {
    console.log('initialized instant data');
    this.patientData = info.data;
  }

  update(data) {
    console.log('updating instant data');
    this.patientData.data = data.data;
  }

  onTileClick(tile: Tile) {
    // do nothing
  }
}
