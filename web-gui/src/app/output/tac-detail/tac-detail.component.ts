import { AfterViewInit, ChangeDetectorRef, Component, ElementRef } from '@angular/core';
import { TacData, TacDataImpl } from '../../models/tac-data';
import { WindowResizeListenerService } from '../../utils/window-resize-listener.service';
import { NoClickActionDataComponent } from '../../grid-layout/no-click-action-data.component';
import { InitInfo } from '../../models/init-info';

@Component({
  selector: 'app-list-data',
  templateUrl: './tac-detail.component.html',
  styleUrls: ['./tac-detail.component.scss']
})
export class TacDetailComponent extends NoClickActionDataComponent implements AfterViewInit {

  data: TacData = new TacDataImpl();
  imgSizeAdjustingFactor: number = 5;
  imgSize = {
    imgWidth: 100,
    imgHeight: 100
  };

  constructor(public elementRef: ElementRef,
              private wrsService: WindowResizeListenerService,
              private changeDetectorRef: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.onResize(this.wrsService, this.changeDetectorRef, this.updateImageMaxSize,
      this.elementRef, this.matGridTileSelector, this.imgSizeAdjustingFactor, this.imgSize);
  }

  init(info: InitInfo) {
    console.log('init tac component');
    console.log(info.data);
    this.data = info.data;
  }

  update(data) {
    console.log('update tac component');
    console.log(data);
    this.data = data;
  }

  updateImageMaxSize(wrsService: WindowResizeListenerService, ...args: any[]) {
    const elementRef = args[0];
    const selector = args[1];
    const imgSizeAdjustingFactor = args[2];
    const img = args[3];
    // the elementRef is necessary to take the current area,
    // using getHtmlSelectorHeight is wrong since it will always
    // take the first selector (mat-grid-tile)
    const containerHeight = wrsService.getClosestElementHeight(elementRef, selector);
    const containerWidth = wrsService.getClosestElementWidth(elementRef, selector);
    // TODO: the image scaling is not preserved, the image dimensions should be post processed
    img.imgHeight = containerHeight - imgSizeAdjustingFactor;
    img.imgWidth = containerWidth - imgSizeAdjustingFactor;
  }

}
