import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TacDetailComponent } from './tac-detail.component';

describe('TacDetailComponent', () => {
  let component: TacDetailComponent;
  let fixture: ComponentFixture<TacDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TacDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TacDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
