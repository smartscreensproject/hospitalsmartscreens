import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoStreamSelectionComponent } from './video-stream-selection.component';

describe('VideoStreamSelectionComponent', () => {
  let component: VideoStreamSelectionComponent;
  let fixture: ComponentFixture<VideoStreamSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoStreamSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoStreamSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
