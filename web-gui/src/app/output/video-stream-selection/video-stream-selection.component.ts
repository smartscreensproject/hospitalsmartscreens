import { Component, Input } from '@angular/core';
import { Video } from '../../models/video';
import { NoClickActionDataComponent } from '../../grid-layout/no-click-action-data.component';
import { InitInfo } from '../../models/init-info';

@Component({
  selector: 'app-video-stream-selection',
  templateUrl: './video-stream-selection.component.html',
  styleUrls: ['./video-stream-selection.component.scss']
})
export class VideoStreamSelectionComponent extends NoClickActionDataComponent {

  videoStreamsList: Video[];
  displayedColumns: string[] = ['title', 'duration', 'streamType', 'live', 'creationDate'];

  constructor() {
    super();
  }

  init(info: InitInfo) {
    this.log('init video stream selection', info);
    this.videoStreamsList = info.data.streams;
  }

  update(data) {
    this.log('update video stream selection', data);
    this.videoStreamsList = data.streams;
  }
}
