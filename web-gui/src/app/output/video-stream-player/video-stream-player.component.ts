import { AfterViewInit, ChangeDetectorRef, Component, ElementRef } from '@angular/core';
import { Video } from '../../models/video';
import { WindowResizeListenerService } from '../../utils/window-resize-listener.service';
import { NoClickActionDataComponent } from '../../grid-layout/no-click-action-data.component';
import { InitInfo } from '../../models/init-info';
//import {shaka} from 'shaka-player'

// imported at angular.json "script": [./node_modules/shaka-player/dist/shaka-player.compiled.js"]
declare var shaka: any;

@Component({
  selector: 'app-video-stream-player',
  templateUrl: './video-stream-player.component.html',
  styleUrls: ['./video-stream-player.component.scss']
})
export class VideoStreamPlayerComponent extends NoClickActionDataComponent implements AfterViewInit {

  videoId: string = this.uuid;
  video: Video;
  size = {
    width: 200
  };
  player;
  videoElement: HTMLVideoElement;

  constructor(private wrlService: WindowResizeListenerService,
              private changeDetectorRef: ChangeDetectorRef,
              private elementRef: ElementRef) {
    super();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.onResize(this.wrlService, this.changeDetectorRef, this.setVideoWidth,
      this.elementRef, this.matGridTileSelector, this.size);
  }

  runBeforePromiseResolve() {
    this.setupPlayer();
  }

  setVideoWidth(wrlService: WindowResizeListenerService, ...args: any[]) {
    const elementRef: ElementRef = args[0];
    const selector: string = args[1];
    const size = args[2];
    // the elementRef is necessary to take the current area,
    // using getHtmlSelectorHeight is wrong since it will always
    // take the first selector (mat-grid-tile)
    // TODO: the new width should be assigned only if the new player height
    //  would not "exit" from the cell bounds
    size.width = wrlService.getClosestElementWidth(elementRef, selector);
  }

  setupPlayer() {
    // Install built-in polyfills to patch browser incompatibilities.
    shaka.polyfill.installAll();

    // Check to see if the browser supports the basic APIs Shaka needs.
    if (shaka.Player.isBrowserSupported()) {
      // Everything looks good!
      this.initPlayer();
    } else {
      // This browser does not have the minimum set of APIs we need.
      console.error('Browser not supported!');
    }
  }

  initPlayer() {
    // Create a Player instance.
    this.videoElement = document.getElementById(this.videoId) as HTMLVideoElement;
    this.player = new shaka.Player(this.videoElement);

    // Attach player to the window to make it easy to access in the JS console.
    //window.player = player;

    // Listen for error events.
    this.player.addEventListener('error', this.onErrorEvent);
  }

  load(manifest: string) {
    // Try to load a manifest.
    // This is an asynchronous process.
    this.player.load(manifest).then(function() {
      // This runs if the asynchronous load is successful.
      console.log('The video has now been loaded!');
    }).catch(error => {
      this.onError(error);
    });  // onError is executed if the asynchronous load fails.
  }

  onErrorEvent(event) {
    // Extract the shaka.util.Error object from the event.
    this.onError(event.detail);
  }

  onError(error) {
    // Log the error.
    console.error('Error code', error.code, 'object', error);
  }

  init(info: InitInfo) {
    this.log('init video stream player', info);
    this.video = info.data.streams;
    this.load(this.getManifest(this.video));
    // starts the video if video.playing property is set to true
    if (this.isPlaying(this.video)) {
      console.log('play');
      this.playVideo();
    }
  }

  update(data) {
    this.log('update video stream player', data);
    const received: Video = data.streams;
    // loads the new manifest if it differs from the current one
    if (this.getManifest(received) !== this.getManifest(this.video)) {
      this.pauseVideo();
      this.load(this.getManifest(received));
    }
    this.video = received;
    // starts/stops the video if video.playing property is set to true/false
    this.isPlaying(this.video) ? this.playVideo() : this.pauseVideo();
  }

  getManifest(video: Video): string {
    return video.manifest;
  }

  isPlaying(video: Video): boolean {
    return video.playing;
  }

  playVideo() {
    //TODO: sometimes it is thrown a domException, needs further investigation
    this.videoElement.play()
      .then(() => console.log('video started playing'),
          reason => console.log(reason));
  }

  pauseVideo() {
    this.videoElement.pause();
  }
}
