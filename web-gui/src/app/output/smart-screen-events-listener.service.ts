import { Injectable, OnDestroy } from '@angular/core';
import { SockJsService } from '../utils/server-communication/sock-js.service';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SmartScreenEventsListenerService implements OnDestroy {

  private socketMessageReceivedSource = new Subject<any>();
  private socketErrorSource = new Subject();
  socketMessageReceived$ = this.socketMessageReceivedSource.asObservable();
  socketError$ = this.socketErrorSource.asObservable();
  smartScreenId: string;
  smartScreenAddress: string;
  smartScreenPort: string;

  constructor(private sockJsService: SockJsService) {
  }

  /**
   *
   * @param route
   */
  setupAndConnect(route: ActivatedRoute){
    const paramMap = route.snapshot.paramMap;
    this.smartScreenId = paramMap.get('id');
    this.smartScreenAddress = paramMap.get('address');
    this.smartScreenPort = paramMap.get('port');
    this.connectToService();
  }

  /**
   * Connects to the service via websocket
   */
  connectToService() {
    const url = `ws://${this.smartScreenAddress}:${this.smartScreenPort}/api/smart-screen/subscribe/screen`;
    const subject = this.sockJsService.connect(url);

    subject.subscribe(value => {
      this.notifyNewMessage(value);
    }, error => {
      console.log(error);
      this.notifyError();
      this.reconnectAfterDelay();
    }, () => {
      console.log('disconnected from ' + url);
      this.notifyError();
      this.reconnectAfterDelay();
    });
  }

  private reconnectAfterDelay(timeout: number = 5000) {
    this.sockJsService.close();
    // Try to reconnect in X milliseconds
    setTimeout(() => this.connectToService(), timeout);
  }

  /**
   * Notifies new messages incoming from the websocket
   * @param value the raw message received
   */
  private notifyNewMessage(value: any) {
    this.sockJsService.parseData(value).then(parsedValue => this.socketMessageReceivedSource.next(parsedValue));
  }

  private notifyError() {
    this.socketErrorSource.next();
  }

  ngOnDestroy() {
    this.sockJsService.close();
  }
}
