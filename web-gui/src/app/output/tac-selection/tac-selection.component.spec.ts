import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TacSelectionComponent } from './tac-selection.component';

describe('TacSelectionComponent', () => {
  let component: TacSelectionComponent;
  let fixture: ComponentFixture<TacSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TacSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TacSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
