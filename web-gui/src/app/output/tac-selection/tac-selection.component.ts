import { Component } from '@angular/core';
import { TacInfo } from '../../models/tac-info';
import { DataTimeComponent } from '../../grid-layout/data-time.component';
import { Tile } from '../../models/tile';
import { InitInfo } from '../../models/init-info';

@Component({
  selector: 'app-tac-selection',
  templateUrl: './tac-selection.component.html',
  styleUrls: ['./tac-selection.component.scss']
})
export class TacSelectionComponent extends DataTimeComponent {

  displayedColumns: string[] = ['internalNumber', 'name', 'date'];
  availableTacList: TacInfo[] = [];

  constructor() {
    super();
  }

  init(info: InitInfo) {
    console.log('init tac');
    this.availableTacList = info.data.data;
  }

  update(data) {
    console.log('update tac');
    this.availableTacList = data.data;
  }

  onTileClick(tile: Tile) {
    // do nothing
  }
}


