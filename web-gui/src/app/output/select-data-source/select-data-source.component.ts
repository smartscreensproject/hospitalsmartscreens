import { Component } from '@angular/core';
import { NoClickActionDataComponent } from '../../grid-layout/no-click-action-data.component';
import { InitInfo } from '../../models/init-info';

@Component({
  selector: 'app-select-source',
  templateUrl: './select-data-source.component.html',
  styleUrls: ['./select-data-source.component.scss']
})
export class SelectDataSourceComponent extends NoClickActionDataComponent {

  selector: string = 'mat-grid-tile';

  constructor() {
    super();
  }

  init(info: InitInfo) {
  }

  update(data) {
  }
}
