import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Paths } from '../routes-paths';

@Component({
  selector: 'app-mode-selection',
  templateUrl: './mode-selection.component.html',
  styleUrls: ['./mode-selection.component.scss']
})
export class ModeSelectionComponent {

  constructor(private router: Router) {
  }

  goToSmartScreenSelection() {
    this.changeRoute(Paths.SmartScreenSelection.valueOf());
  }

  goToInputMangerSelection() {
    this.changeRoute(Paths.InputManagerSelection.valueOf());
  }

  changeRoute(route: string) {
    this.router.navigate([`/${route}`]).catch(console.log);
  }
}
