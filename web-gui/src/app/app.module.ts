import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponentsModule } from './material-components/material-components.module';
import { GridLayoutComponent } from './grid-layout/grid-layout.component';
import { InstantVitalSignsComponent } from './output/instant-vital-signs/instant-vital-signs.component';
import { TacDetailComponent } from './output/tac-detail/tac-detail.component';
import { DataViewerDirective } from './grid-layout/data-viewer.directive';
import { SelectDataSourceComponent } from './output/select-data-source/select-data-source.component';
import { NotificationSnackbarComponent } from './utils/notification-snackbar/notification-snackbar.component';
import { OutputComponent } from './output/output.component';
import { InputComponent } from './input/input.component';
import { ChartVitalSignsComponent } from './output/chart-vital-signs/chart-vital-signs.component';
import { TacSelectionComponent } from './output/tac-selection/tac-selection.component';
import { ChartComponent } from './utils/chart/chart.component';
import { VideoStreamSelectionComponent } from './output/video-stream-selection/video-stream-selection.component';
import { VideoStreamPlayerComponent } from './output/video-stream-player/video-stream-player.component';
import { SelectSourceComponent } from './input/select-source/select-source.component';
import { VideoStreamsDetailComponent } from './output/video-streams-detail/video-streams-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { InputManagerSelectionComponent } from './input-manager-selection/input-manager-selection.component';
import { SelectPatientDialogComponent } from './input/select-patient-dialog/select-patient-dialog.component';
import { FormsModule } from '@angular/forms';
import { SelectPatientSourceDialogComponent } from './input/select-patient-source-dialog/select-patient-source-dialog.component';
import { SourceWithActionsComponent } from './input/source-with-actions/source-with-actions.component';
import { ShowActionsDialogComponent } from './input/show-actions-dialog/show-actions-dialog.component';
import { SelectSourcePositionsDialogComponent } from './input/select-source-positions-dialog/select-source-positions-dialog.component';
import { SmartScreenSelectionComponent } from './smart-screen-selection/smart-screen-selection.component';
import { ModeSelectionComponent } from './mode-selection/mode-selection.component';

@NgModule({
  declarations: [
    AppComponent,
    GridLayoutComponent,
    InstantVitalSignsComponent,
    TacDetailComponent,
    DataViewerDirective,
    SelectDataSourceComponent,
    NotificationSnackbarComponent,
    OutputComponent,
    InputComponent,
    ChartVitalSignsComponent,
    TacSelectionComponent,
    ChartComponent,
    VideoStreamSelectionComponent,
    VideoStreamPlayerComponent,
    SelectSourceComponent,
    VideoStreamsDetailComponent,
    InputManagerSelectionComponent,
    SelectPatientDialogComponent,
    SelectPatientSourceDialogComponent,
    SourceWithActionsComponent,
    ShowActionsDialogComponent,
    SelectSourcePositionsDialogComponent,
    SmartScreenSelectionComponent,
    ModeSelectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    HttpClientModule,
    FormsModule
  ],
  entryComponents: [
    NotificationSnackbarComponent,
    // Output
    SelectDataSourceComponent,
    InstantVitalSignsComponent,
    ChartVitalSignsComponent,
    TacSelectionComponent,
    TacDetailComponent,
    VideoStreamSelectionComponent,
    VideoStreamsDetailComponent,
    VideoStreamPlayerComponent,
    // Input
    SelectSourceComponent,
    SelectSourcePositionsDialogComponent,
    SelectPatientDialogComponent,
    SelectPatientSourceDialogComponent,
    SourceWithActionsComponent,
    ShowActionsDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
