export interface Image {
  id: string;
  filename: string;
  name: string;
  extension: string;
  url: string;
  base64: string;
  weight: number;
}
