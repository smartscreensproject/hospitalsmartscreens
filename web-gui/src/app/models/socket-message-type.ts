export enum SocketMessageType {
  ScreenState,
  AddComponent,
  RemoveComponent,
  Clear,
  UpdateData,
  ModifyViewer,
  Resize,
  Move,
  ChangeGrid
}
