export interface Video {
  id: number;
  title: string;
  descr: string;
  duration: string;
  streamType: string;
  manifest: string;
  live: boolean;
  currentTime: number;
  creationDate: string;
  playing?: boolean;
}
