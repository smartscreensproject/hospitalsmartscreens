import { WindowResizeListenerService } from '../utils/window-resize-listener.service';

export interface Grid {
  rows: number;
  columns: number;
}

export interface GridList extends Grid {

  rowHeight: number;
  windowResizeListenerService: WindowResizeListenerService;
  selector: string;

  /**
   * Sets the grid's size
   * @param rows the number of rows the grid should have
   * @param columns the number of columns the grid should have
   */
  setGridSize(rows: number, columns: number);

  /**
   * Sets the grid's columns number
   * @param columns the number of columns the grid should have
   */
  setColumns(columns: number);

  /**
   * Sets the grid's rows number
   * @param rows the number of rows the grid should have
   */
  setRows(rows: number);

  /**
   * Resizes the row height based on the height of it's container,
   * the elements above and elements under it
   * @param adjustingFactor pixels added to adjust the computed height
   */
  setContainerHeight(adjustingFactor: number);

}

export class GridListImpl implements GridList {
  rows = 1;
  columns = 1;
  private containerHeight: number = null;
  rowHeight = 200;
  windowResizeListenerService: WindowResizeListenerService;
  selector: string;

  /**
   *
   * @param windowResizeListenerService the service that listen for window resize events
   * @param selector the container html selector
   */
  constructor(windowResizeListenerService: WindowResizeListenerService, selector: string) {
    this.windowResizeListenerService = windowResizeListenerService;
    this.selector = selector;
  }

  setGridSize(rows: number, columns: number) {
    this.setColumns(columns);
    this.setRows(rows);
  }

  setColumns(columns: number) {
    this.columns = columns;
  }

  setRows(rows: number) {
    this.rows = rows;
    this.computeNewRowHeight();
  }

  setContainerHeight(adjustingFactor: number = 0) {
    // computes the container height
    let containerHeight = this.windowResizeListenerService.getHtmlSelectorHeight(this.selector) + adjustingFactor;

    /* TODO: for now the elements above and under the container
        must set the 'subtractToGridHeight' class name, probably
        exists a smarter way to look at the above/under elements
        of the current one */
    // computes the height of all elements above and under
    // the grid and subtracts it to the container height
    const list = document.getElementsByClassName('subtractToGridHeight');
    for (let i = 0; i < list.length; i++) {
      containerHeight -= this.windowResizeListenerService.getElementHeigth(list[i]);
    }
    // if the height has changed sets the new one and computes the new row height
    if (this.containerHeight !== containerHeight) {
      this.containerHeight = containerHeight;
      this.computeNewRowHeight();
    }
  }

  private computeNewRowHeight() {
    this.rowHeight = this.containerHeight / this.rows;
  }
}
