export interface Future<T> {
  resolve: () => T;
  reject: () => T;
}
