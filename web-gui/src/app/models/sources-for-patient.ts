export interface SourcesForPatient {
  patientId: string;
  sources: string[];
}
