import { VitalSign } from './vital-sign';

export interface PatientData {
  patientId: string;
  patientName: string;
  timestamp: string;
  deviceId: string;
  data: VitalSign[];
}

export class PatientDataImpl implements PatientData {
  patientId = '';
  patientName = 'unknown';
  timestamp = '';
  deviceId = '';
  data = [];
}
