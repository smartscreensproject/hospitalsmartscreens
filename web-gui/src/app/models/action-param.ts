export interface ActionParam {
  description: string;
  items: any[];
  name: string;
  type: string;
  value: any;
  visible: boolean;
}
