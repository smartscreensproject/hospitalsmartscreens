import { Component } from './component';

export interface ComponentsWithActions {
  actions: any[];
  component: Component;
}
