import { Action } from './action';
import { ActionParam } from './action-param';

export interface SelectedAction {
  selectedAction: Action,
  selectedParam: ActionParam,
  selectedItem: any
}
