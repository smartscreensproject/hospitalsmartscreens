export interface TacInfo {
  id: string;
  internalNumber: number;
  date: string;
  name: string;
}
