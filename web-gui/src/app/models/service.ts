import { ServiceAddress } from './service-address';

export interface Service {
  id: string
  deviceIdInRoom: string
  roomId: string
  serviceAddress: ServiceAddress
}
