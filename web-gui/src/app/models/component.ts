export interface Component {
  componentId: string;
  patientId: string;
  positions: number[];
  sourceType: string;
}

export class ComponentImpl implements Component {
  componentId: string;
  patientId: string;
  positions: number[];
  sourceType: string;

  constructor(patientId: string, positions: number[], sourceType: string, componentId: string = null) {
    this.patientId = patientId;
    this.positions = positions;
    this.sourceType = sourceType;
    this.componentId = componentId;
  }
}
