import { ActionParam } from './action-param';

export interface Action {
  description: string;
  href: string;
  id: string;
  method: string;
  name: string;
  params: ActionParam[];
}
