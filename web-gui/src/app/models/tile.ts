import { Cell } from './cell';
import { ComponentRef } from '@angular/core';
import '../utils/my-array';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  cell: Cell;
  componentRef: ComponentRef<{}>;

  /**
   * Sets the tile occupied positions and computes it's new rows and columns
   * @param positions the tile occupied positions
   * @param gridColumns the current grid columns
   */
  setOccupiedPositions(positions: number[], gridColumns: number);

  /**
   * Gets the grid cells occupied by this tile
   */
  getOccupiedPositions(): number[];

  /**
   * Gets the first grid cell occupied by this tile
   */
  getFirstOccupiedPosition(): number;

  /**
   * Gets the component id
   */
  getComponentId(): string;

  /**
   * Gets the positions occupied by each column
   */
  getPositionsByColumns(): number[][];

  /**
   * Gets the positions occupied by each row
   */
  getPositionsByRows(): number[][];

  /**
   * Gets the source type
   */
  getSourceType(): string;
}

export class TileImpl implements Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  cell: Cell;
  componentRef: ComponentRef<{}>;

  /**
   * Creates a new tile element
   * @param cell the cell assigned to this tile
   * @param gridColumns the current number of grid columns
   */
  constructor(cell: Cell, gridColumns: number) {
    this.cell = cell;
    this.setOccupiedPositions(this.getOccupiedPositions(), gridColumns);
    this.color = 'lightgray';
    this.text = '';
  }

  setOccupiedPositions(positions: number[], gridColumns: number) {
    positions = positions.sortNumerically();
    const positionsEntries = Object.entries(positions);
    // count the tile columns number
    let cols = 0;
    let currPosition = 0;
    for (const [, p] of positionsEntries) {
      // counts the first consecutive numbers to determine the columns number
      if (!currPosition || currPosition + 1 === p) {
        currPosition = p;
        cols += 1;
      } else {
        break;
      }
    }
    // count the tile rows number
    let rows = 1;
    for (const [i, p] of positionsEntries) {
      if (i !== '0') {
        // if the positions are not consecutive numbers it means that the tile is on more rows
        if (currPosition + 1 !== p) {
          rows += 1;
        }
      }
      currPosition = p;
    }
    // fix count if tile fills the entire screen
    if (cols > gridColumns) {
      cols = gridColumns;
      rows = positions.length / cols;
    }
    // console.log("cols " + cols + " rows " + rows);
    this.cell.component.positions = positions;
    this.cols = cols;
    this.rows = rows;
  }

  getOccupiedPositions(): number[] {
    return this.cell.component.positions.sortNumerically();
  }

  getFirstOccupiedPosition(): number {
    return this.getOccupiedPositions()[0];
  }

  getComponentId(): string {
    return this.cell.component.componentId;
  }

  getPositionsByColumns(): number[][] {
    const positionsByColumn = new Array<number[]>(this.cols).fill([]);

    this.getOccupiedPositions().forEach((v, idx) => {
      let colIdx = idx;
      while (colIdx - this.cols > 0) {
        colIdx = colIdx - this.cols;
      }
      positionsByColumn[colIdx].push(v);
    });
    return positionsByColumn;
  }

  getPositionsByRows(): number[][] {
    const positionsByRows = new Array<number[]>(this.rows).fill([]);

    this.getOccupiedPositions().forEach((v, idx) => {
      let rowIdx = 0;
      let curr = idx;
      while (curr > this.rows) {
        curr -= this.rows;
        rowIdx++;
      }
      positionsByRows[rowIdx].push(v);
    });
    return positionsByRows;
  }

  getSourceType(): string {
    return this.cell.component.sourceType;
  }
}
