export enum DataEmittedType {
  AddComponentWithActions,
  GetGridState,
  ResizeComponent
}
