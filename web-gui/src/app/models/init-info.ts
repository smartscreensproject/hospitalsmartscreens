import { Tile } from './tile';

export interface InitInfo {
  tile: Tile;
  data: any;
}
