export interface ServiceAddress {
  address: string,
  port: number
}
