import { DataComponent } from '../grid-layout/data.component';
import { ComponentRef, Type } from '@angular/core';

export interface DynamicDataArea {
  idx: number;
  viewer: Type<DataComponent>;
  componentRef: ComponentRef<{}>;
}
