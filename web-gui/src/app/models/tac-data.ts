import { Patient } from './patient';
import { Image } from './image';

export interface TacData {
  id: string;
  internalNumber: number;
  date: number;
  patient: Patient;
  images: Image;
}

export class TacDataImpl implements TacData {
  id = '';
  internalNumber = 0;
  date = 0;
  patient = {
    id: '',
    ps: '',
    name: '',
    surname: ''
  };
  images = {
    id: '',
    filename: 'undefined',
    name: 'undefined',
    extension: 'undefined',
    url: '',
    base64: '',
    weight: 0
  };
}
