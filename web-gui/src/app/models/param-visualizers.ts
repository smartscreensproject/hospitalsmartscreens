export const enum ParamVisualizers {
  ArrayType = 'array-type',
  SingleType = 'single-type'
}
