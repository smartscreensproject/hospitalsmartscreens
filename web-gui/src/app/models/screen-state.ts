import { Grid } from './grid';
import { Cell } from './cell';

export interface ScreenState {
  room: string;
  deviceIdInRoom: string;
  grid: Grid;
  areas: Cell[];
}

export class ScreenStateImpl implements ScreenState {
  areas: Cell[];
  deviceIdInRoom: string;
  grid: Grid;
  room: string;

  constructor(grid: Grid, areas: Cell[]) {
    this.grid = grid;
    this.areas = areas;
  }
}
