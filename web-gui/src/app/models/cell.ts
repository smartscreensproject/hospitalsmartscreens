import { Component } from './component';

export interface Cell {
  component: Component;
  data: object;
  dataReferredTo: string;
  viewer: string;
  actions?: any[]
}

export class CellImpl implements Cell {
  component;
  data = {};
  dataReferredTo = '';
  viewer;
  actions;

  constructor(component: Component, actions: any[] = [], viewer: string = '') {
    this.component = component;
    this.actions = actions;
    this.viewer = viewer;
  }

}
