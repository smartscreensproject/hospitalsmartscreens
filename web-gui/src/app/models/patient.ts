export interface Patient {
  id: string;
  ps: string;
  name: string;
  surname: string;
}
