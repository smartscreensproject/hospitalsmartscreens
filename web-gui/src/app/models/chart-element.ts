export interface ChartElement {
  label: string;
  data: number[];
  fill: boolean;
  lineTension: number;
  borderColor: string;
  backgroundColor: string;
  borderWidth: number;
}

export class ChartElementImpl implements ChartElement {
  label: string;
  data: number[] = [];
  fill: boolean = false;
  lineTension: number = 0.2;
  backgroundColor: string;
  borderColor: string;
  borderWidth: number = 2;

  constructor(label: string, data: number[], color: string = ChartElementImpl.getRandomColor()) {
    this.label = label;
    this.data = data;
    this.borderColor = color;
    this.backgroundColor = color;
  }

  private static getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}
