import { ComponentsWithActions } from './components-with-actions';
import { Grid } from './grid';

export interface InputGridState {
  grid: Grid;
  componentsWithActions: ComponentsWithActions[];
}
