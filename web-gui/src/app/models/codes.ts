export interface Codes {
  ps: string;
  tmp: string;
  timestamp: string;
  name: string;
  pid: string;
}

export class CodesImpl implements Codes {
  ps = '';
  tmp = '';
  timestamp = '';
  name = '';
  pid = '';
}
