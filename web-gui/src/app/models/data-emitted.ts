import { DataEmittedType } from './data-emitted-type';

export interface DataEmitted {
  type: DataEmittedType;
  data: any;
}

export class DataEmittedImpl {
  type: DataEmittedType;
  data: any;

  constructor(type: DataEmittedType, data: any) {
    this.type = type;
    this.data = data;
  }
}
