export interface VitalSign {
  type: string;
  value: string;
  uom: string;
}
