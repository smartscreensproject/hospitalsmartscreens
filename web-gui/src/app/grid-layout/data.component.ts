import { v4 as uuid } from 'uuid';
import { AfterViewInit } from '@angular/core';
import { ResizeListener } from '../utils/resize-listener';
import { Future } from '../models/future';
import { Tile } from '../models/tile';
import { InitInfo } from '../models/init-info';

/**
 * Class to extend to dynamically display a component inside a cell in the GridLayoutComponent
 */
export abstract class DataComponent extends ResizeListener implements AfterViewInit {

  // useful to have unique value inside the component (ex. html id={{uuid}})
  protected uuid: string = uuid();
  protected matGridTileSelector = 'mat-grid-tile';
  protected deferredPromise: Future<void>;
  viewInitializedPromise: Promise<void> = new Promise<void>((resolve, reject) => {
    this.deferredPromise = {resolve: resolve, reject: reject};
  });
  tile: Tile;

  ngAfterViewInit() {
    this.runBeforePromiseResolve();
    // console.log('view initialized');
    this.deferredPromise.resolve();
  }

  /**
   * Logging utility
   * @param message the message to display
   * @param data the data to log
   */
  protected log(message: string, data: any) {
    console.log(message);
    console.log(data);
  }

  /**
   * Override this function to run code before resolving the promise
   * telling that the view has been rendered
   */
  protected runBeforePromiseResolve() {
  }

  /**
   * Called when at the component initialization
   * @param info the data to initialize
   */
  abstract init(info: InitInfo): void;

  /**
   * Called when the component data should be updated
   * @param data the data to update
   */
  abstract update(data): void;

  /**
   * Called when the tile is clicked
   * @param tile the tile clicked
   */
  abstract onTileClick(tile: Tile): void;

  /**
   * Called when the component size or the grid dimensions change.
   * Some components may need to override this to recompute
   * the dimensions of their elements
   */
  layoutChanged() {
  }
}

