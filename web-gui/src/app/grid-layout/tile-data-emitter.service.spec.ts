import { TestBed } from '@angular/core/testing';

import { TileDataEmitterService } from './tile-data-emitter.service';

describe('TileDataEmitterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TileDataEmitterService = TestBed.get(TileDataEmitterService);
    expect(service).toBeTruthy();
  });
});
