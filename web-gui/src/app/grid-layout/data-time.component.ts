import { DataComponent } from './data.component';
import * as moment from 'moment';

export abstract class DataTimeComponent extends DataComponent {

  protected preferredStringFormat: string = 'YYYY-MM-DD HH:mm:ss';

  /**
   * Given moment compatible timestamp, converts it to javascript Date and then
   * converts it to utc string using the format specified in preferredStringFormat variable
   * @param timestamp the timestamp to parse
   * @param momentFormat the moment format of the timestamp, default is ISO_8601
   */
  parseTimestamp(timestamp: string, momentFormat = moment.ISO_8601): string {
    let date = moment(timestamp, momentFormat);
    // console.log(date.isValid());
    return date.utc().format(this.preferredStringFormat);
  }

  protected parseMilliSecondFromEpochAsString(millisecond: string): string {
    return this.parseMilliSecondFromEpoch(+millisecond);
  }

  protected parseMilliSecondFromEpoch(millisecond: number): string {
    let date = new Date(millisecond).toUTCString();
    return date.substring(0, (date.length - 3));
  }
}
