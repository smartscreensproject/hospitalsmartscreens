import { Directive, ViewContainerRef } from '@angular/core';
import { v4 as uuid } from 'uuid';

@Directive({
  selector: '[appDataViewer]'
})
export class DataViewerDirective {

  uuid = uuid();

  constructor(public viewContainerRef: ViewContainerRef) {
  }

}
