import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnDestroy,
  QueryList,
  Type,
  ViewChildren
} from '@angular/core';
import { Tile, TileImpl } from '../models/tile';
import { ScreenState } from '../models/screen-state';
import { DataViewerDirective } from './data-viewer.directive';
import { DataComponent } from './data.component';
import { GridList, GridListImpl } from '../models/grid';
import { SmartScreenEventsListenerService } from '../output/smart-screen-events-listener.service';
import { WindowResizeListenerService } from '../utils/window-resize-listener.service';
import { Cell } from '../models/cell';
import { ResizeListener } from '../utils/resize-listener';
import { TileDataEmitterService } from './tile-data-emitter.service';
import { DataEmitted } from '../models/data-emitted';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-grid-layout',
  templateUrl: './grid-layout.component.html',
  styleUrls: ['./grid-layout.component.scss'],
  providers: [SmartScreenEventsListenerService]
})
export class GridLayoutComponent extends ResizeListener implements AfterViewInit, OnDestroy {

  /**
   * Function that given a tile selects the correct component to dynamically load
   * @param tile the tile data used to determine the component to dynamically load
   */
  @Input() componentResolver: (tile: Tile) => Type<DataComponent>;
  /**
   * Function called by dynamically generated components
   * when emitting data via tile-data-emitter.service
   * @param gridLayoutComponent the reference to the gridLayoutComponent emitted
   * @param dataEmitted the value emitted
   */
  @Input() onEventEmitted: (gridLayoutComponent: GridLayoutComponent, dataEmitted: DataEmitted) => void;
  @ViewChildren(DataViewerDirective) dataViewerList: QueryList<DataViewerDirective>;
  selector = '#grid-container';
  gridList: GridList = new GridListImpl(this.windowResizeListenerService, this.selector);
  tiles: Tile[] = [];
  tileDataSourceSubscription: Subscription;

  constructor(private windowResizeListenerService: WindowResizeListenerService,
              private changeDetectorRef: ChangeDetectorRef,
              private componentFactoryResolver: ComponentFactoryResolver,
              public tileDataEmitterService: TileDataEmitterService) {
    super();
    const self = this;
    this.tileDataSourceSubscription = this.tileDataEmitterService.observableTileDataSource$.subscribe(v =>
      this.onEventEmitted(self, v));
  }

  ngAfterViewInit() {
    const adjustingFactor = -2;
    this.onResize(this.windowResizeListenerService, this.changeDetectorRef, this.setContainerHeight,
      this.gridList, adjustingFactor);
  }

  /**
   * Strategy used to resize the height of the tiles
   * @param args
   */
  private setContainerHeight({}: WindowResizeListenerService, ...args: any[]) {
    // TODO: improve the strategy that considers also the height
    //  of the elements above and under the the app-grid tag
    const gridList: GridList = args[0];
    const adjustingFactor: number = args[1];
    gridList.setContainerHeight(adjustingFactor);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    if (this.tileDataSourceSubscription) {
      this.tileDataSourceSubscription.unsubscribe();
    }
  }

  /**
   * Add a new component
   * @param cell the json data containing the data of the new component
   */
  addComponent(cell: Cell) {
    this.addTile(new TileImpl(cell, this.gridList.columns));
  }

  /**
   * Removes a component. All the occupied positions will be replaced with the default viewer
   * @param componentIdToRemove the component id to remove
   * @param newAreas the new empty areas that replace the removed component
   */
  removeComponent(componentIdToRemove: string, newAreas: Cell[]) {
    // removes the tile filtering by componentId
    this.tiles = this.tiles.filter(t => t.getComponentId() !== componentIdToRemove);
    // adds the new tiles that will replace the removed one
    newAreas.forEach(cell => this.addComponent(cell));
  }

  /**
   * Clears all the screen areas, replacing them with the default viewer
   * @param newAreas the cells used to reinitialize the grid
   */
  clear(newAreas: Cell[]) {
    this.tiles = [];
    // reinitializes the grid with empty areas
    newAreas.forEach(cell => this.addComponent(cell));
  }

  /**
   * Gets the component ref given the specified component id
   * @param componentId the component id used to find the component ref
   */
  private getComponentRef(componentId: string): ComponentRef<any> {
    return this.tiles.find(t => t.getComponentId() === componentId).componentRef;
  }

  /**
   * Calls a DataComponent function. The function is called only after
   * the component's view has been correctly initialized
   * @param componentId used to find the DataComponent which function should be called
   * @param data the data to pass to the DataComponent function to call
   * @param childFunctionToCall the DataComponent function to call
   */
  private callChildFunction(componentId: string, data: any,
                            childFunctionToCall: (componentInstance: DataComponent, data) => void) {
    const componentRef = this.getComponentRef(componentId);
    const componentInstance = (componentRef.instance as DataComponent);
    // calls the component function after the viewInitializedPromise is resolved
    componentInstance.viewInitializedPromise.then(() => {
      childFunctionToCall(componentInstance, data);
    });
  }

  /**
   * Inits the specified component data. The strategy adopted
   * for the initialization should be implemented inside the component
   * @param componentId the component id to update
   * @param data with which it will be initialized
   */
  initData(componentId: string, data: any) {
    const tile = this.tiles.find(tile => tile.getComponentId() === componentId);
    const obj = {
      'tile': tile,
      'data': data
    };
    // calls the DataComponent function init(data)
    const childFunctionToCall = (componentInstance: DataComponent, data) =>
      componentInstance.init(data);
    this.callChildFunction(componentId, obj, childFunctionToCall);
  }

  /**
   * Updates the specified component data. The strategy adopted
   * for the update should be implemented inside the component
   * @param componentId the component id to update
   * @param data the new incoming data
   */
  updateData(componentId: string, data: any) {
    // calls the DataComponent function update(data)
    const childFunctionToCall = (componentInstance: DataComponent, data) =>
      componentInstance.update(data);
    this.callChildFunction(componentId, data, childFunctionToCall);
  }

  /**
   * Calls onTileClick(data) of the clicked tile
   * @param idx the index of the tile clicked
   */
  onTileClick(idx: number) {
    const clickedTile = this.tiles[idx];
    // calls the DataComponent function onTileClick(data)
    const childFunctionToCall = (componentInstance: DataComponent, data) =>
      componentInstance.onTileClick(data);
    this.callChildFunction(clickedTile.getComponentId(), clickedTile, childFunctionToCall);
  }

  /**
   * Inits the specified component data. The strategy adopted
   * for the initialization should be implemented inside the component
   * @param componentId the component id to update
   */
  layoutChanged(componentId: string) {
    // TODO: horrible thing done to allow the parent component
    //  to be resized before checking for his dimensions
    setTimeout(
      () => {
        // calls the DataComponent function layoutChanged(data)
        const childFunctionToCall = (componentInstance: DataComponent, {}) =>
          componentInstance.layoutChanged();
        this.callChildFunction(componentId, null, childFunctionToCall);
      }, 1000
    );
  }

  /**
   * Updates the tile viewer, initializing the new dynamically
   * generated component calling the init function
   * @param componentId the component di
   * @param viewer the viewer to use
   * @param data the data with will be used to initialize the component
   */
  modifyViewer(componentId: string, viewer: string, data: any) {
    const idx = this.tiles.findIndex(v => v.getComponentId() === componentId);

    let tile = this.tiles[idx];
    tile.cell.viewer = viewer;
    // sets the correct tile viewer
    this.setViewer(tile, viewer, idx);
    // initializes the data of new loaded component
    this.initData(componentId, data);
  }

  /**
   * Adds a new tile to the grid. Every tile occupying it's position will be deleted
   * @param tile the tile to add
   */
  private addTile(tile: Tile) {
    // removes all the tiles occupying the positions of the tile to insert
    this.tiles = this.tiles.filter(t => !tile.getOccupiedPositions().includes(t.getFirstOccupiedPosition()));

    const firstOccupiedCell = tile.getFirstOccupiedPosition();
    // finds the index where the tile should be inserted,
    // the tile ordering is based on their first occupied position
    let idx = this.tiles.findIndex(value => value.getFirstOccupiedPosition() >= firstOccupiedCell);
    // inserts the tile at the correct index
    if (idx < 0) {
      this.tiles.push(tile);
      idx = this.tiles.length - 1;
    } else {
      this.tiles.splice(idx, 0, tile);
    }
    // sets the tile correct viewer
    this.setViewer(tile, tile.cell.viewer, idx);
    // initializes the tile data
    this.initData(tile.getComponentId(), tile.cell.data);
  }

  /**
   * Sets the correct viewer for the tile
   * @param tile the tile that should change viewer
   * @param viewer the viewer to set
   * @param idx the index of the tile inside the tiles list
   */
  private setViewer(tile: Tile, viewer: string, idx: number) {
    // TODO: the viewer field is probably useless
    // detects changes to prevent errors
    this.changeDetectorRef.detectChanges();
    // finds the correct component to dynamically load
    const component = this.componentResolver(tile);
    // gets the tile directive reference
    const viewContainerRef = this.getDataViewerByIdx(idx).viewContainerRef;
    // clears the tile from the previous initialized component
    viewContainerRef.clear();
    // resolves the component inside the project structure and gets it's generator factory
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    // dynamically generates the component and
    // assigns the returned component ref to the tile
    tile.componentRef = viewContainerRef.createComponent(componentFactory);
  }

  private getDataViewerByIdx(idx: number): DataViewerDirective {
    return this.dataViewerList.toArray()[idx];
  }

  /**
   * Resizes the grid
   * @param screenState
   */
  resizeGrid(screenState: ScreenState) {
    const areas = screenState.areas;
    const grid = screenState.grid;
    const newColumns = grid.columns;

    // setting the grid rows and columns
    this.gridList.setGridSize(grid.rows, newColumns);

    function getComponentId(c: Cell): string {
      return c.component.componentId;
    }

    // removing the tiles not present in the new received screen state
    this.tiles = this.tiles.filter(t => areas.map(c => getComponentId(c)).includes(t.getComponentId()));
    // updating the existing tiles
    areas.forEach(c => this.tiles.filter(t => t.getComponentId() === getComponentId(c)).forEach(t => {
      t.setOccupiedPositions(c.component.positions, newColumns);
    }));
    // inserting the missing tiles
    areas.filter(c => !this.tiles.map(t => t.getComponentId()).includes(getComponentId(c))).forEach(c => {
      this.addTile(new TileImpl(c, newColumns));
    });
    // sorts the tiles, to respect the first position order
    this.tiles.sort((a, b) => this.tileComparator(a, b));
    // calls for each tile the layoutChanged method to recompute their elements dimensions
    this.tiles.forEach(tile => this.layoutChanged(tile.getComponentId()));
  }

  /**
   * Resizes the area occupied by a tile
   * @param updatedCell the tile to update
   * @param newScreenAreas the new screens area to be added when the tile is shrank down
   */
  resizeComponent(updatedCell: Cell, newScreenAreas: Cell[]) {
    // finding the tile which position will be updated
    const tile = this.tiles.find(tile => tile.getComponentId() === updatedCell.component.componentId);
    const currentlyOccupiedPositions = tile.getOccupiedPositions();

    updatedCell.component.positions.filter(position =>
      // takes only the positions not included in the cell to update
      !currentlyOccupiedPositions.includes(position))
      .forEach(position =>
        // removes the tiles that occupy the positions
        this.tiles = this.tiles.filter(tile => !tile.getOccupiedPositions().includes(position)));

    const gridColumns = this.gridList.columns;
    // set the new positions occupied by the tile
    tile.setOccupiedPositions(updatedCell.component.positions, gridColumns);
    // adds a new tile for each cell in the newScreensArea
    newScreenAreas.forEach(cell => this.addTile(new TileImpl(cell, gridColumns)));
    // sorts the tiles, to respect the first position order
    // there should not be the case where the occupied positions are the same
    this.tiles.sort((a, b) => this.tileComparator(a, b));
    // calls the layoutChanged method to recompute the tile's elements dimensions
    // TODO: not working
    this.layoutChanged(updatedCell.component.componentId);
  }


  /**
   * Compares two tiles based on their first occupied position
   * @param a
   * @param b
   */
  tileComparator(a: Tile, b: Tile): number {
    return this.simpleComparator(a.getFirstOccupiedPosition(), b.getFirstOccupiedPosition());
  }

  /**
   * Compares two numbers, if a < b returns -1, if a > b returns 1, if a == b returns 0
   * @param a the first number
   * @param b the second number
   */
  simpleComparator(a: number, b: number): number {
    return a < b ? -1 : a > b ? 1 : 0;
  }
}
