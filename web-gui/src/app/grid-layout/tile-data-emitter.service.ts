import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { DataEmitted } from '../models/data-emitted';

@Injectable({
  providedIn: 'root'
})
export class TileDataEmitterService {

  // Observable sources
  private tileDataSource = new Subject<DataEmitted>();
  private gridDataSource = new Subject<DataEmitted>();

  // Observable streams
  observableTileDataSource$ = this.tileDataSource.asObservable();
  observableGridDataSource$ = this.gridDataSource.asObservable();

  // Function to update the value of the source
  updateAndNotifyTileDataSource(data: DataEmitted) {
    this.tileDataSource.next(data);
  }

  updateAndNotifyGridDataSource(data: DataEmitted) {
    this.gridDataSource.next(data);
  }
}
