import { DataComponent } from './data.component';
import { Tile } from '../models/tile';

export abstract class NoClickActionDataComponent extends DataComponent {

  onTileClick(tile: Tile) {
    // do nothing
  }

}
