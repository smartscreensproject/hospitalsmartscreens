import { AfterViewInit, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { DialogComponent } from '../dialog-component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component as MyComponent } from '../../models/component';
import { WindowResizeListenerService } from '../../utils/window-resize-listener.service';

const enum Color {
  Free = 'gray',
  OccupiedByOtherSource = 'red',
  OccupiedByCurrentSource = 'blue'
}

export interface Interface {
  color: Color;
  idx: number;
}

export class InterfaceImpl implements Interface {
  color: Color;
  idx: number;

  constructor(idx: number, color: Color = Color.Free) {
    this.idx = idx;
    this.color = color;
  }

}

@Component({
  selector: 'app-select-source-positions-dialog',
  templateUrl: './select-source-positions-dialog.component.html',
  styleUrls: ['./select-source-positions-dialog.component.scss']
})
export class SelectSourcePositionsDialogComponent extends DialogComponent<SelectSourcePositionsDialogComponent> implements AfterViewInit {

  emptyIdStartingValue: string = 'E_';
  gridColumns;
  gridRows;
  positionsTiles: Interface[] = [];
  currentComponentId: string;
  positionsToOccupyList: number[] = [];
  height = '100px';
  rowHeight = 100;

  constructor(public dialogRef: MatDialogRef<SelectSourcePositionsDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private changeDetectorRef: ChangeDetectorRef,
              private wrlService: WindowResizeListenerService) {
    super(dialogRef);

    this.gridColumns = data.gridColumns;
    this.currentComponentId = data.currentComponentId;
    // initializing the positions tiles
    console.log((data.components as MyComponent[]));
    (data.components as MyComponent[]).forEach(c => {
      c.positions.forEach(p => {
        let color: Color;
        if (c.componentId === this.currentComponentId) {
          this.positionsToOccupyList.push(p);
          color = Color.OccupiedByCurrentSource;
        } else if (this.isFree(c.componentId)) {
          color = Color.Free;
        } else {
          color = Color.OccupiedByOtherSource;
        }
        this.positionsTiles.push(new InterfaceImpl(p, color));
      });
    });
    this.positionsTiles.sort((a, b) => a.idx < b.idx ? -1 : a.idx > b.idx ? 1 : 0);
    this.gridRows = this.positionsTiles.length / this.gridColumns;
  }

  ngAfterViewInit() {
    this.onResize();
    this.wrlService.onResize$.subscribe(() => {
      this.onResize();
    });
    this.changeDetectorRef.detectChanges();
  }

  onResize() {
    // TODO improve height computing with general values
    const adjustingFactor = 40;
    const matDialogContainerHeight = this.wrlService.getHtmlSelectorHeight('mat-dialog-container');
    const h1Height = this.wrlService.getHtmlSelectorHeight('h1');
    const actionsHeight = this.wrlService.getHtmlSelectorHeight('#actions');
    const computed = matDialogContainerHeight - h1Height - actionsHeight - adjustingFactor;
    this.height = `${computed}px`;
    this.rowHeight = (computed - 200) / this.gridRows;
    /* console.log(matDialogContainerHeight);
     console.log(h1Height);
     console.log(actionsHeight);
     console.log(computed);
     console.log(this.height);
     console.log(this.rowHeight);*/
  }

  isFree(componentId: string): boolean {
    return componentId != null && componentId.startsWith(this.emptyIdStartingValue);
  }

  onTileClick(color: Color, idx) {
    // TODO: implement strategy that automatically marks/unmarks the positions
    //  to mantain the rectangular shape of the occupied area
    const listIdx = idx - 1;
    switch (color) {
      case Color.Free:
        this.positionsTiles[listIdx].color = Color.OccupiedByCurrentSource;
        this.positionsToOccupyList.push(idx);
        break;
      case Color.OccupiedByCurrentSource:
        // this is done only the clicked tile is not the last one referred to the component to insert
        if (this.positionsToOccupyList.length >= 2) {
          this.positionsTiles[listIdx].color = Color.Free;
          // removes the index from the positionToOccupyList
          this.positionsToOccupyList = this.positionsToOccupyList.filter(value => value != idx);
        }
        break;
      case Color.OccupiedByOtherSource:
        // do nothing, another source is already present
        break;
    }
  }
}
