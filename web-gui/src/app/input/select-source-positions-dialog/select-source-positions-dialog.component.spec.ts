import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSourcePositionsDialogComponent } from './select-source-positions-dialog.component';

describe('SelectSourcePositionsDialogComponent', () => {
  let component: SelectSourcePositionsDialogComponent;
  let fixture: ComponentFixture<SelectSourcePositionsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSourcePositionsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSourcePositionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
