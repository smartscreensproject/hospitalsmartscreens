import { Component, OnInit, Type } from '@angular/core';
import { GridContainerComponent } from '../grid-container.component';
import { DataComponent } from '../grid-layout/data.component';
import { SelectSourceComponent } from './select-source/select-source.component';
import { ApiGatewayCommunicationService } from './api-gateway-communication.service';
import { Cell, CellImpl } from '../models/cell';
import { ActivatedRoute } from '@angular/router';
import { InputVisualizers } from './input-visualizers';
import { SourceWithActionsComponent } from './source-with-actions/source-with-actions.component';
import { Tile } from '../models/tile';
import { Grid } from '../models/grid';
import { ScreenStateImpl } from '../models/screen-state';
import { InputGridState } from '../models/input-grid-state';
import { DataEmitted } from '../models/data-emitted';
import { DataEmittedType } from '../models/data-emitted-type';
import { ComponentsWithActions } from '../models/components-with-actions';
import { Component as MyComponent } from '../models/component';
import { GridLayoutComponent } from '../grid-layout/grid-layout.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent extends GridContainerComponent implements OnInit {

  grid: Grid = {
    rows: 0,
    columns: 0
  };

  constructor(public agcService: ApiGatewayCommunicationService,
              public route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.agcService.setInputManagerUri(this.route);
    this.agcService.getState().then(value => {
      console.log(value);
      this.resizeGrid(value);
    }).catch(err => {
      console.log(err);
    });
  }

  componentResolver(tile: Tile): Type<DataComponent> {
    let type = tile.getSourceType();
    console.log('type ' + type);
    let component;
    switch (type) {
      case InputVisualizers.SourceToBeSelected.valueOf():
        component = SelectSourceComponent;
        break;
      default:
        component = SourceWithActionsComponent;
        break;
    }
    return component;
  }

  onEventEmitted(gridLayoutComponent: GridLayoutComponent, dataEmitted: DataEmitted) {
    const data = dataEmitted.data;
    switch (dataEmitted.type) {
      case DataEmittedType.AddComponentWithActions:
        const componentInfo: ComponentsWithActions = data.componentInfo;
        const cell = new CellImpl(componentInfo.component, componentInfo.actions, data.viewer);
        gridLayoutComponent.addComponent(cell);
        break;
      case DataEmittedType.GetGridState:
        // TODO: this is a broadcast, probably it is better to take the component ref and call updateTile
        gridLayoutComponent.tileDataEmitterService.updateAndNotifyGridDataSource({
          type: DataEmittedType.GetGridState,
          data: {
            gridColumns: gridLayoutComponent.gridList.columns,
            components: gridLayoutComponent.tiles.map(tile => tile.cell.component),
            tileToNotifyId: data
          }
        });
        break;
      case DataEmittedType.ResizeComponent:
        // creating a new Cell with the updated component
        const componentUpdated: MyComponent = data.componentUpdated;
        const updatedCell = new CellImpl(componentUpdated);
        // for each new component creates a new Cell
        const newComponents: MyComponent[] = data.newComponents;
        const updatedScreenAreas: Cell[] = newComponents.map(component => new CellImpl(component));
        // resizes the component
        gridLayoutComponent.resizeComponent(updatedCell, updatedScreenAreas);
        break;
    }
  }

  changeGridSize() {
    this.agcService.changeGridSize(this.grid).then(inputState => this.resizeGrid(inputState));
  }

  resizeGrid(inputState: InputGridState) {
    this.grid = inputState.grid;
    const cells = inputState.componentsWithActions.map(cwa => new CellImpl(cwa.component, cwa.actions));
    const screenState = new ScreenStateImpl(this.grid, cells);
    this.gridLayoutComponent.resizeGrid(screenState);
  }

  saveProfile() {
    // TODO
    alert('TODO: non implemented yet');
  }
}
