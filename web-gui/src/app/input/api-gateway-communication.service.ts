import { Injectable } from '@angular/core';
import { HttpRequestsService } from '../utils/server-communication/http-requests.service';
import { ActivatedRoute } from '@angular/router';
import { AddressPortUriImpl, Uri } from '../utils/server-communication/uri';
import { InputGridState } from '../models/input-grid-state';
import { ServicesRestApi } from '../utils/server-communication/services-rest-api';
import { Codes } from '../models/codes';
import { ServiceAddress } from '../models/service-address';
import { Service } from '../models/service';
import { SourcesForPatient } from '../models/sources-for-patient';
import { Component, ComponentImpl } from '../models/component';
import { ComponentsWithActions } from '../models/components-with-actions';
import { Action } from '../models/action';
import { Grid } from '../models/grid';

@Injectable({
  providedIn: 'root'
})
export class ApiGatewayCommunicationService {

  // TODO create class to extend with this?
  servicesRestApi = new ServicesRestApi();
  registryUrl: Uri = this.servicesRestApi.registryUrl;
  inputManagerUrl: Uri;
  inputManagerId: string;

  constructor(public httpRequestService: HttpRequestsService) {
  }

  getAvailableSmartScreens(): Promise<Service[]> {
    return this.httpRequestService.get<Service[]>(this.servicesRestApi.smartScreenUri.getPath());
  }

  getAvailableInputManagers(): Promise<Service[]> {
    return this.httpRequestService.get<Service[]>(this.servicesRestApi.inputManagersUri.getPath());
  }

  setInputManagerUri(route: ActivatedRoute) {
    const paramMap = route.snapshot.paramMap;
    this.inputManagerId = paramMap.get('id');
    const address = paramMap.get('address');
    const port = paramMap.get('port');
    this.inputManagerUrl = new AddressPortUriImpl(address, port);
  }

  getState(): Promise<InputGridState> {
    const path = `api/input-manager/${this.inputManagerId}/input-state`;
    const url = this.inputManagerUrl.addPath(path).getPath();
    return this.httpRequestService.get<InputGridState>(url);
  }

  getAvailableServices(): Promise<any> {
    const getServices = this.registryUrl.addPath(this.servicesRestApi.availableServicesPath).getPath();
    return this.httpRequestService.get<any>(getServices);
  }

  getPatientSources(codes: Codes): Promise<SourcesForPatient> {
    console.log('getting patient sources');
    let pirUri;
    let prsUri;
    console.log(codes);
    return this.getAvailableServices().then(value => {
      console.log(value);
      const patientIdResolver: ServiceAddress = value.find(value => value.name === 'Patient id resolver').serviceAddress;
      pirUri = new AddressPortUriImpl(patientIdResolver.address, '' + patientIdResolver.port);
      const patientRelatedSources: ServiceAddress = value.find(value => value.name === 'Patient related source').serviceAddress;
      prsUri = new AddressPortUriImpl(patientRelatedSources.address, '' + patientRelatedSources.port);
      return this.httpRequestService.post<any>(pirUri.addPath('api/patient-id-resolver/patient/search').getPath(), codes);
    }).then(value => {
      // TODO: this will always take the first patient found,
      //  if more patients are found, the user should be prompted to select the wanted one
      const usersId = value.find(v => v.patientId != null).patientId;
      return this.httpRequestService.get<SourcesForPatient>(prsUri.addPath(`api/patient-related-sources/sources/${usersId}`).getPath());
    });
  }

  addComponent(patientId: string, positions: number[], sourceType: string): Promise<ComponentsWithActions> {
    console.log('adding new component');
    const component = new ComponentImpl(patientId, positions, sourceType);
    const addComponentRelativeRoute = `api/input-manager/${this.inputManagerId}/components`;
    const addComponentUri = this.inputManagerUrl.addPath(addComponentRelativeRoute);
    return this.httpRequestService.post<ComponentsWithActions>(addComponentUri.getPath(), component);
  }

  getComponentActions(componentId): Promise<Action[]> {
    const currentInputManger = `api/input-manager/${this.inputManagerId}/`;
    const componentsActions = `components/${componentId}/actions`;
    const componentActionsUri = this.inputManagerUrl.addPath(currentInputManger)
      .addPath(componentsActions);
    return this.httpRequestService.get(componentActionsUri.getPath());
  }

  removeComponent(componentId: string): Promise<Component[]> {
    const currentInputManger = `api/input-manager/${this.inputManagerId}/`;
    const componentToDelete = `components/${componentId}`;
    const componentActionsUri = this.inputManagerUrl.addPath(currentInputManger)
      .addPath(componentToDelete);
    return this.httpRequestService.delete(componentActionsUri.getPath());
  }

  makeAction(componentId: string, selectedAction: Action): Promise<any> {
    const actionId = selectedAction.id;
    const currentInputManger = `api/input-manager/${this.inputManagerId}`;
    const component = `components/${componentId}`;
    const componentAction = `actions/${actionId}`;
    const path = this.inputManagerUrl.addPath(currentInputManger)
      .addPath(component).addPath(componentAction).getPath();
    console.log('path:');
    console.log(path);
    // transforming all the values into strings
    if (selectedAction.params != null) {
      selectedAction.params.forEach(param => {
        if (typeof param.value !== 'string') {
          param.value = JSON.stringify(param.value);
        }
      });
    }
    console.log(selectedAction);
    return this.httpRequestService.post(path, selectedAction);
  }

  changeGridSize(grid: Grid): Promise<InputGridState> {
    const currentInputManger = `api/input-manager/${this.inputManagerId}`;
    const path = this.inputManagerUrl.addPath(currentInputManger).addPath('change-grid');
    return this.httpRequestService.post(path.getPath(), grid);
  }

  resizeComponent(componentId: string, updatedComponent: Component): Promise<any> {
    const currentInputManger = `api/input-manager/${this.inputManagerId}`;
    const component = `components/${componentId}`;
    const resizeComponentUri = this.inputManagerUrl.addPath(currentInputManger).addPath(component);
    return this.httpRequestService.put(resizeComponentUri.getPath(), updatedComponent);
  }
}
