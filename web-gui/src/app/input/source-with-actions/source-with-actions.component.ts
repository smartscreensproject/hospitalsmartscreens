import { Component } from '@angular/core';
import { DataComponent } from '../../grid-layout/data.component';
import { Tile } from '../../models/tile';
import { ApiGatewayCommunicationService } from '../api-gateway-communication.service';
import { DialogService } from '../../utils/dialog.service';
import { Action } from '../../models/action';
import { DefaultDialogConfigImpl } from '../../models/dialog-config';
import { ShowActionsDialogComponent } from '../show-actions-dialog/show-actions-dialog.component';
import { TileDataEmitterService } from '../../grid-layout/tile-data-emitter.service';
import { DataEmitted, DataEmittedImpl } from '../../models/data-emitted';
import { DataEmittedType } from '../../models/data-emitted-type';
import { ComponentsWithActions } from '../../models/components-with-actions';
import { InitInfo } from '../../models/init-info';
import { SelectSourcePositionsDialogComponent } from '../select-source-positions-dialog/select-source-positions-dialog.component';
import { Component as MyComponent, ComponentImpl } from '../../models/component';

@Component({
  selector: 'app-source-with-actions',
  templateUrl: './source-with-actions.component.html',
  styleUrls: ['./source-with-actions.component.scss']
})
export class SourceWithActionsComponent extends DataComponent {

  constructor(public dialogService: DialogService,
              public agcService: ApiGatewayCommunicationService,
              private tileDataEmitterService: TileDataEmitterService) {
    super();
    tileDataEmitterService.observableGridDataSource$.subscribe(v => {
      const data = v.data;
      const condition = this.tile != null &&
        this.tile.getComponentId() === data.tileToNotifyId &&
        v.type === DataEmittedType.GetGridState;
      if (condition) {
        this.openPositionSelectionDialogComponent(data.gridColumns, data.components);
      }
    });
  }

  init(info: InitInfo) {
    this.tile = info.tile;
  }

  update(data) {
    console.log(data);
  }

  onTileClick(tile: Tile) {
    this.openAvailableActionsDialog(tile.getComponentId());
  }

  /**
   * Shows the available actions for the current source
   * @param componentId
   */
  openAvailableActionsDialog(componentId: string) {
    const config = new DefaultDialogConfigImpl(this.tile.cell.actions);
    // opens the dialog loading ShowActionsDialogComponent
    const sourceDialogRef = this.dialogService.open(ShowActionsDialogComponent, config);
    // get the dialog closure event
    this.dialogService.getClosureEvent(sourceDialogRef).then(result => {
      console.log(result);
      if (this.dialogService.checkResult(result, '')) {
        // sending selected action
        return this.agcService.makeAction(componentId, result);
      } else {
        return Promise.reject();
      }
    }).then(newActions => {
      // this.actions = newActions;
      this.tile.cell.actions = newActions;
      // TODO: decide if it is better to immediately open the dialog with the new actions
      //this.openAvailableActionsDialog(componentId);
    }).catch(console.log); // TODO: distinguish between cancel and a server error
  }

  /**
   * Removes the component
   */
  removeComponent() {
    console.log(this.tile);
    this.agcService.removeComponent(this.tile.getComponentId()).then(response => {
      console.log(response);
      // updates the grid with the new received empty cells
      response.forEach(component => {
        const data: ComponentsWithActions = {
          actions: [],
          component: component
        };
        const cell = {
          componentInfo: data,
          viewer: '',
          dataReferredTo: 'TODO',
        };
        const dataEmitted: DataEmitted = {
          type: DataEmittedType.AddComponentWithActions,
          data: cell
        };
        this.tileDataEmitterService.updateAndNotifyTileDataSource(dataEmitted);
      });
      console.log('removed successfully');
    });
  }

  /**
   * Resizes the component
   */
  resizeComponent() {
    // requesting grid state
    this.tileDataEmitterService.updateAndNotifyTileDataSource({
      type: DataEmittedType.GetGridState,
      data: this.tile.getComponentId()
    });
  }

  openPositionSelectionDialogComponent(gridColumns: number, components: MyComponent[]) {
    // opens the dialog loading SelectSourcePositionsDialogComponent
    const sourceDialogRef = this.dialogService.open(SelectSourcePositionsDialogComponent,
      new DefaultDialogConfigImpl({
        currentComponentId: this.tile.getComponentId(),
        components: components,
        gridColumns: gridColumns
      }));
    // get the dialog closure event
    this.dialogService.getClosureEvent(sourceDialogRef).then(result => {
      if (this.dialogService.checkResult(result, [])) {
        const component = this.tile.cell.component;
        const updatedComponent = new ComponentImpl(component.patientId, result,
          component.sourceType, component.componentId);
        // sends the updated component to the input manager
        return this.agcService.resizeComponent(this.tile.getComponentId(), updatedComponent);
      } else {
        return Promise.reject();
      }
    }).then(result => {
      const dataEmitted = new DataEmittedImpl(DataEmittedType.ResizeComponent, result);
      this.tileDataEmitterService.updateAndNotifyTileDataSource(dataEmitted);
    }).catch(console.log); // TODO: distinguish between cancel and a server error
  }
}
