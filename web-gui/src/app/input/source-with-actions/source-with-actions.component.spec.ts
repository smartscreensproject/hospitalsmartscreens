import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceWithActionsComponent } from './source-with-actions.component';

describe('SourceWithActionsComponent', () => {
  let component: SourceWithActionsComponent;
  let fixture: ComponentFixture<SourceWithActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceWithActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceWithActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
