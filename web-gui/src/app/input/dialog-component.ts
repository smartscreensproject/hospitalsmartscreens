import { MatDialogRef } from '@angular/material';

export class DialogComponent<D> {

  constructor(public dialogRef: MatDialogRef<D>) {
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
