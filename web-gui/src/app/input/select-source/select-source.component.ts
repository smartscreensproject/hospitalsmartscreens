import { Component } from '@angular/core';
import { DataComponent } from '../../grid-layout/data.component';
import { ApiGatewayCommunicationService } from '../api-gateway-communication.service';
import { SelectPatientSourceDialogComponent } from '../select-patient-source-dialog/select-patient-source-dialog.component';
import { SelectPatientDialogComponent } from '../select-patient-dialog/select-patient-dialog.component';
import { SourcesForPatient } from '../../models/sources-for-patient';
import { Tile } from '../../models/tile';
import { InputVisualizers } from '../input-visualizers';
import { TileDataEmitterService } from '../../grid-layout/tile-data-emitter.service';
import { DialogService } from '../../utils/dialog.service';
import { DefaultDialogConfigImpl } from '../../models/dialog-config';
import { DataEmitted } from '../../models/data-emitted';
import { DataEmittedType } from '../../models/data-emitted-type';
import { SelectSourcePositionsDialogComponent } from '../select-source-positions-dialog/select-source-positions-dialog.component';
import { Component as MyComponent } from '../../models/component';
import { InitInfo } from '../../models/init-info';

@Component({
  selector: 'app-select-source',
  templateUrl: './select-source.component.html',
  styleUrls: ['./select-source.component.scss']
})
export class SelectSourceComponent extends DataComponent {

  constructor(public dialogService: DialogService,
              public agcService: ApiGatewayCommunicationService,
              private tileDataEmitterService: TileDataEmitterService) {
    super();
    // listens for the events emitted by the GridComponent
    tileDataEmitterService.observableGridDataSource$.subscribe(v => {
      const data = v.data;
      // opens the position selection dialog only if it has been requested by the current tile
      const condition = this.tile != null &&
        this.tile.getComponentId() === data.tileToNotifyId &&
        v.type === DataEmittedType.GetGridState;
      if (condition) {
        this.openPositionSelectionDialogComponent(data.gridColumns, data.components);
      }
    });
  }

  init(info: InitInfo) {
  }

  update(data) {
  }

  onTileClick(tile: Tile) {
    this.tile = tile;
    this.getGridState();
  }

  getGridState() {
    this.tileDataEmitterService.updateAndNotifyTileDataSource({
      type: DataEmittedType.GetGridState,
      data: this.tile.getComponentId()
    });
  }

  /**
   * Opens the dialog for selecting the positions that should be occupied by the tile
   * @param gridColumns the number of columns of the grid
   * @param components the components that are in the list
   */
  openPositionSelectionDialogComponent(gridColumns: number, components: MyComponent[]) {
    // opens the dialog loading SelectSourcePositionsDialogComponent
    const sourceDialogRef = this.dialogService.open(SelectSourcePositionsDialogComponent,
      new DefaultDialogConfigImpl({
        currentComponentId: this.tile.getComponentId(),
        components: components,
        gridColumns: gridColumns
      }));
    // get the dialog closure event
    this.dialogService.getClosureEvent(sourceDialogRef).then(result => {
      console.log(result);
      if (this.dialogService.checkResult(result, [])) {
        this.openSelectPatientDialogComponent(result);
      }
    }).catch(console.log);
  }

  /**
   * Opens the patient selection dialog
   * @param occupiedPositions the positions that should be occupied by the tile
   */
  openSelectPatientDialogComponent(occupiedPositions: number[]) {
    // opens the dialog loading SelectPatientDialogComponent
    const sourceDialogRef = this.dialogService.open(SelectPatientDialogComponent);
    // get the dialog closure event
    this.dialogService.getClosureEvent(sourceDialogRef).then(result => {
      if (this.dialogService.checkResult(result, '')) {
        // gets the patient's sources
        return this.agcService.getPatientSources(result);
      } else {
        return Promise.reject();
      }
    }).then(value => {
      this.openPatientSourceSelectionDialog(occupiedPositions, value);
    }).catch(console.log);
  }

  /**
   * Opens the source selection dialog
   * @param occupiedPositions the positions that should be occupied by the tile
   * @param patientSources the patient available sources
   */
  openPatientSourceSelectionDialog(occupiedPositions: number[], patientSources: SourcesForPatient) {
    const config = new DefaultDialogConfigImpl(patientSources);
    // opens the dialog loading SelectPatientSourceDialogComponent
    const sourceDialogRef = this.dialogService.open(SelectPatientSourceDialogComponent, config);
    // get the dialog closure event
    this.dialogService.getClosureEvent(sourceDialogRef).then(result => {
      console.log(result);
      if (this.dialogService.checkResult(result, '')) {
        const patientId = patientSources.patientId;
        return this.agcService.addComponent(patientId, occupiedPositions, result);
      } else {
        return Promise.reject();
      }
    }).then(componentInfo => {
      console.log('received');
      console.log(componentInfo);
      const data = {
        dataReferredTo: 'TODO',
        componentInfo: componentInfo,
        viewer: InputVisualizers.SourceSelected.valueOf()
      };
      const dataToEmit: DataEmitted = {
        type: DataEmittedType.AddComponentWithActions,
        data: data
      };
      this.tileDataEmitterService.updateAndNotifyTileDataSource(dataToEmit);
    }).catch(console.log);
  }
}
