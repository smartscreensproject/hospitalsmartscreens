import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DialogComponent } from '../dialog-component';
import { SourcesForPatient } from '../../models/sources-for-patient';
import '../../utils/my-array';

@Component({
  selector: 'app-select-patient-source-dialog',
  templateUrl: './select-patient-source-dialog.component.html',
  styleUrls: ['./select-patient-source-dialog.component.scss']
})
export class SelectPatientSourceDialogComponent extends DialogComponent<SelectPatientSourceDialogComponent> {

  selected;

  constructor(public dialogRef: MatDialogRef<SelectPatientSourceDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: SourcesForPatient) {
    super(dialogRef);
    data.sources = data.sources.sortNumerically();
    this.selected = data.sources[0];
  }

}
