import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPatientSourceDialogComponent } from './select-patient-source-dialog.component';

describe('SelectPatientSourceDialogComponent', () => {
  let component: SelectPatientSourceDialogComponent;
  let fixture: ComponentFixture<SelectPatientSourceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPatientSourceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPatientSourceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
