import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPatientDialogComponent } from './select-patient-dialog.component';

describe('SelectPatientDialogComponent', () => {
  let component: SelectPatientDialogComponent;
  let fixture: ComponentFixture<SelectPatientDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPatientDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPatientDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
