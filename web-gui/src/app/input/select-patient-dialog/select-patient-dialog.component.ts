import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Codes, CodesImpl } from '../../models/codes';
import { DialogComponent } from '../dialog-component';

@Component({
  selector: 'app-select-data-source-dialog',
  templateUrl: './select-patient-dialog.component.html',
  styleUrls: ['./select-patient-dialog.component.scss']
})
export class SelectPatientDialogComponent extends DialogComponent<SelectPatientDialogComponent> {

  codes: Codes = new CodesImpl();

  constructor(public dialogRef: MatDialogRef<SelectPatientDialogComponent>) {
    super(dialogRef);
  }

}
