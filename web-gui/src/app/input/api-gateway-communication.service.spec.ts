import { TestBed } from '@angular/core/testing';

import { ApiGatewayCommunicationService } from './api-gateway-communication.service';

describe('ApiGatewayCommunicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiGatewayCommunicationService = TestBed.get(ApiGatewayCommunicationService);
    expect(service).toBeTruthy();
  });
});
