import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowActionsDialogComponent } from './show-actions-dialog.component';

describe('ShowActionsDialogComponent', () => {
  let component: ShowActionsDialogComponent;
  let fixture: ComponentFixture<ShowActionsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowActionsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowActionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
