import { Component, Inject } from '@angular/core';
import { DialogComponent } from '../dialog-component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Action } from '../../models/action';
import { ActionParam } from '../../models/action-param';
import { ParamVisualizers } from '../../models/param-visualizers';


@Component({
  selector: 'app-show-actions-dialog',
  templateUrl: './show-actions-dialog.component.html',
  styleUrls: ['./show-actions-dialog.component.scss']
})
export class ShowActionsDialogComponent extends DialogComponent<ShowActionsDialogComponent> {

  /*selected: SelectedAction = {
    selectedAction: null,
    selectedParam: null,
    selectedItem: null
  };*/
  selectedAction: Action;

  //selectedItem: any;

  constructor(public dialogRef: MatDialogRef<ShowActionsDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Action[]) {
    super(dialogRef);
    console.log(data);
    this.selectedAction = data[0];
    this.onSelectedActionChange();
    /*this.selected.selectedParam = this.selected.selectedAction.params[0];
    this.selected.selectedItem = this.selected.selectedParam.value;
    this.selected.selectedParam.visible*/
  }

  switchActionVisualizer(param: ActionParam): string {
    // the param.type is the type returned in the value field
    const paramType = param.type;
    let visualizer;
    if (paramType.endsWith('[]')) {
      visualizer = ParamVisualizers.ArrayType.valueOf();
    } else {
      visualizer = ParamVisualizers.SingleType.valueOf();
    }
    // TODO: add disabled property to ok button if not all the actions have been made
    return visualizer;
  }

  /**
   * Called when the selected action changes
   */
  onSelectedActionChange() {
    console.log(this.selectedAction);
    if (this.selectedAction.params != null) {
      this.selectedAction.params.forEach(p => {
        if (ParamVisualizers.ArrayType.valueOf() === this.switchActionVisualizer(p)) {
          p.value = [];
        }
      });
    }
  }

  /**
   * Adds/removes the item from the ActionParam value set when the checkbox is set to true/false
   * @param checked determines if the item should be added or removed from the ActionParam value set
   * @param param the action param that will be sent
   * @param item the current item
   */
  onCheckboxChange(checked: boolean, param: ActionParam, item) {
    if (checked) {
      // adds the item to the value set
      param.value.push(item);
    } else {
      // removes the item from the value set
      param.value = param.value.filter(e => e != item);
    }
    //console.log(this.selectedAction);
  }

  /**
   * Checks all the checkboxes
   * @param param the action param to check
   * @param items
   */
  checkAll(param: ActionParam, items: any[]) {
    items.forEach(item => this.onCheckboxChange(true, param, item));
  }

  /**
   * Unchecks all the checkboxes
   * @param param
   * @param items
   */
  unCheckAll(param: ActionParam, items: any[]) {
    items.forEach(item => this.onCheckboxChange(false, param, item));
  }

  isToCheck(param: ActionParam, item): boolean {
    return param.value.includes(item);
  }

  isArray(items): boolean {
    return Array.isArray(items);
  }
}
