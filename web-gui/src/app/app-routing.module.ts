import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OutputComponent } from './output/output.component';
import { InputComponent } from './input/input.component';
import { InputManagerSelectionComponent } from './input-manager-selection/input-manager-selection.component';
import { SmartScreenSelectionComponent } from './smart-screen-selection/smart-screen-selection.component';
import { ModeSelectionComponent } from './mode-selection/mode-selection.component';
import { Paths } from './routes-paths';

const routes: Routes = [
  {path: Paths.DataDisplay.valueOf(), component: OutputComponent},
  {path: Paths.DataSelection.valueOf(), component: InputComponent},
  {path: Paths.InputManagerSelection.valueOf(), component: InputManagerSelectionComponent},
  {path: Paths.SmartScreenSelection.valueOf(), component: SmartScreenSelectionComponent},
  {path: Paths.ModeSelection.valueOf(), component: ModeSelectionComponent},
  {
    path: '',
    redirectTo: Paths.ModeSelection.valueOf(),
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
