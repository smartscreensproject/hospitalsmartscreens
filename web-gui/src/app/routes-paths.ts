export enum Paths {
  DataDisplay = 'data-display',
  DataSelection = 'data-selection',
  InputManagerSelection = 'input-manager-selection',
  SmartScreenSelection = 'smart-screen-selection',
  ModeSelection = 'mode-selection'
}
