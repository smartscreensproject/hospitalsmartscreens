package it.unibo.smartscreens.common.apiclient.interfaces;

import io.vertx.core.Future;
import it.unibo.smartscreens.common.model.Action;
import it.unibo.smartscreens.common.model.ComponentInitParams;

import java.util.List;

/**
 * Generic interface representing the available operations for a data manager microservice
 */
public interface DataManagerApiClient {

    /**
     * Initialises the manager microservice
     * @param params the parameters that will be used for the initialization
     * @return a future representing the result of the request
     */
    Future<List<Action>> init(final ComponentInitParams params);

    /**
     * Destroys the manager microservice
     * @return a future representing the result of the request
     */
    Future destroy();

    /**
     * Retrieves the manager microservice supported actions
     * @return a future representing the result of the request
     */
    Future<List<Action>> getActions();
}
