package it.unibo.smartscreens.common.model.registry;

import it.unibo.smartscreens.common.model.ServiceAddress;

import java.util.UUID;

public abstract class AbstractEntity {

    private String id;
    private ServiceAddress serviceAddress;

    public AbstractEntity() {
    }

    public AbstractEntity(ServiceAddress serviceAddress) {
        this.id = generateId();
        this.serviceAddress = serviceAddress;
    }

    protected String generateId() {
        return UUID.randomUUID().toString().replaceAll("-", "_");
    }

    public AbstractEntity setId(String id) {
        if (getId() == null) {
            this.id = id;
        }
        return this;
    }

    public String getId() {
        return id;
    }

    public ServiceAddress getServiceAddress() {
        return serviceAddress;
    }

    public AbstractEntity setServiceAddress(ServiceAddress serviceAddress) {
        this.serviceAddress = serviceAddress;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractEntity that = (AbstractEntity) o;
        return getServiceAddress().equals(that.getServiceAddress());
    }
}


