package it.unibo.smartscreens.common.msroute;

public class VideoStreamsRoute {
    // Api root
    public static final String API_ROOT = "/api";

    // Init
    public static final String API_INIT = API_ROOT + "/init";
    // Destroy
    public static final String API_DESTROY = API_ROOT + "/destroy";
    // Actions
    public static final String API_ACTIONS = API_ROOT + "/actions";
}
