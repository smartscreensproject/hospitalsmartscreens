package it.unibo.smartscreens.common.msroute;

public class SmartScreenRoute {

    public static final String API_ROOT = "/api/smart-screen";
    public static final String API_STATE = API_ROOT + "/state";
    public static final String API_PROFILE = API_ROOT + "/profile";
    public static final String API_COMPONENTS = API_ROOT + "/components";
    public static final String API_CLEAR = API_ROOT + "/clear";
    public static final String API_UPDATE_DATA = API_ROOT + "/update-data";
    public static final String API_CHANGE_GRID = API_ROOT + "/change-grid";
    public static final String API_MONITOR_SOCKET = API_ROOT + "/subscribe/screen/*";
    public static final String API_SOURCE_SOCKET = API_ROOT + "/subscribe/source/*";
}
