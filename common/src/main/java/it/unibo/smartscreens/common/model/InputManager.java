package it.unibo.smartscreens.common.model;

/**
 * InputManager POJO
 */
public class InputManager {

    private String id;
    private String room;
    private String deviceId;
    private String address;
    private int port;

    public InputManager(String id, String room, String deviceId, String address, int port) {
        this.id = id;
        this.room = room;
        this.deviceId = deviceId;
        this.address = address;
        this.port = port;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
