package it.unibo.smartscreens.common.model.tac;

/**
 * TacImage POJO
 */
public class TacImage {

    private String id;
    private String filename;
    private String name;
    private String extension;
    private String url;
    private String base64;
    private int weight;

    public TacImage() {}

    public TacImage(final String id, final String filename, final String name, final String extension, final String url, final String base64, final int weight) {
        this.id = id;
        this.filename = filename;
        this.name = name;
        this.extension = extension;
        this.url = url;
        this.base64 = base64;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
