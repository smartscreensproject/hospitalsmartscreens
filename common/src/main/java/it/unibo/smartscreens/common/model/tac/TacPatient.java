package it.unibo.smartscreens.common.model.tac;

/**
 * TacPatient POJO
 */
public class TacPatient {

    private String name;

    public TacPatient() {}

    public TacPatient(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
