package it.unibo.smartscreens.common.msroute;

public class InputManagerRoute {

    // Api root
    public static final String API_ROOT = "/api/input-manager";
    // Output id related routes
    public static final String API_INPUT_ID = API_ROOT + "/%s";
    public static final String API_INPUT_CHANGE_GRID = API_ROOT + "/%s/change-grid";
    public static final String API_INPUT_SAVE_PROFILE = API_ROOT + "/%s/save-profile";
    public static final String API_INPUT_LOAD_PROFILE = API_ROOT + "/%s/load-profile";
    public static final String API_INPUT_CLEAR = API_ROOT + "/%s/clear";
    public static final String API_INPUT_COMPONENTS = API_ROOT + "/%s/components";
    public static final String API_INPUT_ACTIONS = API_ROOT + "/%s/actions";
    public static final String API_INPUT_INPUT_STATE = API_ROOT + "/%s/input-state";
    // Component id related routes
    public static final String API_COMPONENT_ID = API_ROOT + "/%s/components/%s";
    public static final String API_COMPONENT_ACTIONS = API_ROOT + "/%s/components/%s/actions";
    public static final String API_COMPONENT_DATA = API_ROOT + "/%s/components/%s/data";
    // Component action related routes
    public static final String API_ACTIONS_ID = API_ROOT + "/%s/components/%s/actions/%s";
}
