package it.unibo.smartscreens.common.model;

import io.vertx.core.json.JsonObject;
import it.unibo.smartscreens.common.enums.SourceSocketMessageType;

public class SourceSocketMsg {

    private SourceSocketMessageType type;
    private JsonObject msg;

    public SourceSocketMsg() {
        msg = new JsonObject();
    }

    public SourceSocketMsg(SourceSocketMessageType type, JsonObject msg) {
        this.type = type;
        this.msg = msg;
    }

    public SourceSocketMessageType getType() {
        return type;
    }

    public SourceSocketMsg setType(SourceSocketMessageType type) {
        this.type = type;
        return this;
    }

    public JsonObject getMsg() {
        return msg;
    }

    public SourceSocketMsg setMsg(JsonObject msg) {
        this.msg = msg;
        return this;
    }
}
