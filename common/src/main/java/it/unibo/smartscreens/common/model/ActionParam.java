package it.unibo.smartscreens.common.model;

import java.util.List;

/**
 * ActionParam POJO
 */
public class ActionParam {

    private String name;
    private String description;
    private String type;
    private String value;
    private List<String> items;
    private boolean visible;

    public ActionParam() {}

    public ActionParam(String name, String description, String type, String value, boolean visible) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.value = value;
        this.visible = visible;
    }

    public ActionParam(String name, String description, String type, String value, List<String> items, boolean visible) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.value = value;
        this.items = items;
        this.visible = visible;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
