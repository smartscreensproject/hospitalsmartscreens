package it.unibo.smartscreens.common.apiclient.interfaces;

import io.vertx.core.Future;
import it.unibo.smartscreens.common.model.PatientSources;


public interface PatientRelatedSourcesApiClient {

    Future<PatientSources> getSourceByPatientId(String patientId);

    Future<PatientSources> addSource(String patientId, String source);

    Future<PatientSources> removeSource(String patientId, String source);
}
