package it.unibo.smartscreens.common.msroute;

public class HeartBeatRoute {

    public static final String API_HEARTBEAT = "/status";
}
