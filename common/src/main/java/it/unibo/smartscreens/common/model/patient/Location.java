package it.unibo.smartscreens.common.model.patient;

public class Location {

    private String roomId;
    private String roomName;

    public Location() {
    }

    public Location(String roomId, String roomName) {
        this.roomId = roomId;
        this.roomName = roomName;
    }

    public String getRoomId() {
        return roomId;
    }

    public Location setRoomId(String roomId) {
        this.roomId = roomId;
        return this;
    }

    public String getRoomName() {
        return roomName;
    }

    public Location setRoomName(String roomName) {
        this.roomName = roomName;
        return this;
    }
}
