package it.unibo.smartscreens.common.apiclient.interfaces;

import io.vertx.core.Future;
import it.unibo.smartscreens.common.model.ServiceAddress;

public interface HeartBeatApiClient {

    Future<Void> heartBeat(ServiceAddress serviceAddress);
}
