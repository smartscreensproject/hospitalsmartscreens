package it.unibo.smartscreens.common.apiclient.base;

/**
 * Simple POJO class to manage required roots for Component Data Manager clients
 */
public class BasicDataManagerRoutes {

    private final String initRoute;
    private final String destroyRoute;
    private final String actionsRoute;

    public BasicDataManagerRoutes(String initRoute, String destroyRoute, String actionsRoute) {
        this.initRoute = initRoute;
        this.destroyRoute = destroyRoute;
        this.actionsRoute = actionsRoute;
    }

    public String getInitRoute() {
        return initRoute;
    }

    public String getDestroyRoute() {
        return destroyRoute;
    }

    public String getActionsRoute() {
        return actionsRoute;
    }

    public static BasicDataManagerRoutes create(String initRoute, String destroyRoute, String actionsRoute) {
        return new BasicDataManagerRoutes(initRoute, destroyRoute, actionsRoute);
    }
}
