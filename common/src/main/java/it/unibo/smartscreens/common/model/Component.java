package it.unibo.smartscreens.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Component POJO
 */
public class Component {

    private String componentId;
    private List<Integer> positions;
    private String sourceType;
    private String patientId;

    public Component() {
    }

    public Component(String componentId, List<Integer> positions, String sourceType, String patientId) {
        this.componentId = componentId;
        this.positions = positions;
        this.sourceType = sourceType;
        this.patientId = patientId;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    @JsonIgnore
    public Stream<Integer> getPositionsAsStream() {
        return getPositions().stream();
    }

    public List<Integer> getPositions() {
        return this.positions;
    }

    public void setPositions(List<Integer> positions) {
        this.positions = positions;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public void initId() {
        this.componentId = UUID.randomUUID().toString().replace("-", "_");
    }

    @JsonIgnore
    public boolean isAllInitialized() {
        return getComponentId() != null && !getComponentId().isEmpty() &&
            getPositions() != null && !getPositions().isEmpty() &&
            getSourceType() != null &&
            getPatientId() != null;
    }

}
