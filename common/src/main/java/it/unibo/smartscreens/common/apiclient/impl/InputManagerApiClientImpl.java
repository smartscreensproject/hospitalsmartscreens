package it.unibo.smartscreens.common.apiclient.impl;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.smartscreens.common.apiclient.base.BaseInputManagerApiClient;
import it.unibo.smartscreens.common.apiclient.interfaces.InputManagerApiClient;
import it.unibo.smartscreens.common.model.*;
import it.unibo.smartscreens.common.msroute.InputManagerRoute;

import java.util.List;

/**
 * Useful api client who manages the client requests related to the InputManager microservice
 */
public class InputManagerApiClientImpl extends BaseInputManagerApiClient implements InputManagerApiClient {

    public InputManagerApiClientImpl(String address, int port, Vertx vertx) {
        super(address, port, vertx);
    }

    public InputManagerApiClientImpl(Vertx vertx) {
        super(vertx);
    }

    @Override
    public Future<InputConfiguration> changeGrid(String inputId, Grid newGrid) {
        final Future<InputConfiguration> future = Future.future();
        post(String.format(InputManagerRoute.API_INPUT_CHANGE_GRID, inputId))
            .sendJsonObject(JsonObject.mapFrom(newGrid), (response) -> {
                if (response.succeeded()) {
                    InputConfiguration inputConfiguration = response.result().bodyAsJson(InputConfiguration.class);
                    future.complete(inputConfiguration);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<JsonObject> saveProfile(String inputId, String newProfileName) {
        final Future<JsonObject> future = Future.future();
        final JsonObject jsonObject = new JsonObject();
        jsonObject.put("name", newProfileName);
        post(String.format(InputManagerRoute.API_INPUT_SAVE_PROFILE, inputId))
            .sendJsonObject(jsonObject, (response) -> {
                if (response.succeeded()) {
                    JsonObject responseJson = response.result().bodyAsJsonObject();
                    future.complete(responseJson);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<Profile> loadProfile(String inputId, String profileId) {
        final Future<Profile> future = Future.future();
        final JsonObject jsonObject = new JsonObject();
        jsonObject.put("id", profileId);
        post(String.format(InputManagerRoute.API_INPUT_LOAD_PROFILE, inputId))
            .sendJsonObject(jsonObject, (response) -> {
                if (response.succeeded()) {
                    Profile profile = response.result().bodyAsJson(Profile.class);
                    future.complete(profile);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<List<Component>> clear(String inputId) {
        final Future<List<Component>> future = Future.future();
        delete(String.format(InputManagerRoute.API_INPUT_CLEAR, inputId))
            .send((response) -> {
                if (response.succeeded()) {
                    JsonArray responseArray = response.result().bodyAsJsonArray();
                    List<Component> components = convertJsonArray(responseArray, Component.class);
                    future.complete(components);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<InputConfiguration> retrieveInputState(String inputId) {
        final Future<InputConfiguration> future = Future.future();
        get(String.format(InputManagerRoute.API_INPUT_INPUT_STATE, inputId))
            .send((response) -> {
                if (response.succeeded()) {
                    InputConfiguration inputConfiguration = response.result().bodyAsJson(InputConfiguration.class);
                    future.complete(inputConfiguration);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }

    /*
    @Override
    public Future<List<ComponentWithActions>> retrieveComponents(String inputId) {
        final Future<List<ComponentWithActions>> future = Future.future();
        get(String.format(InputManagerRoute.API_INPUT_COMPONENTS, inputId))
            .send((response) -> {
                if (response.succeeded()) {
                    JsonArray responseArray = response.result().bodyAsJsonArray();
                    List<ComponentWithActions> components = convertJsonArray(responseArray, ComponentWithActions.class);
                    future.complete(components);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }
   */

    @Override
    public Future<ComponentWithActions> addComponent(String inputId, Component newComponent) {
        final Future<ComponentWithActions> future = Future.future();
        newComponent.setComponentId(null);
        post(String.format(InputManagerRoute.API_INPUT_COMPONENTS, inputId))
            .sendJsonObject(JsonObject.mapFrom(newComponent), (response) -> {
                if (response.succeeded()) {
                    ComponentWithActions component = response.result().bodyAsJson(ComponentWithActions.class);
                    future.complete(component);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<UpdateComponentResponse> updateComponent(String inputId, Component newComponent) {
        final Future<UpdateComponentResponse> future = Future.future();
        String route = String.format(InputManagerRoute.API_COMPONENT_ID, inputId, newComponent.getComponentId());
        put(route)
            .sendJsonObject(JsonObject.mapFrom(newComponent), (response) -> {
                if (response.succeeded()) {
                    UpdateComponentResponse updateComponentResponse = response.result().bodyAsJson(UpdateComponentResponse.class);
                    future.complete(updateComponentResponse);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<List<Component>> removeComponent(String inputId, String componentId) {
        final Future<List<Component>> future = Future.future();
        String route = String.format(InputManagerRoute.API_COMPONENT_ID, inputId, componentId);
        delete(route)
            .send((response) -> {
                if (response.succeeded()) {
                    JsonArray responseArray = response.result().bodyAsJsonArray();
                    List<Component> emptyComponents = convertJsonArray(responseArray, Component.class);
                    future.complete(emptyComponents);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<List<Action>> retrieveComponentActions(String inputId, String componentId) {
        final Future<List<Action>> future = Future.future();
        String route = String.format(InputManagerRoute.API_COMPONENT_ACTIONS, inputId, componentId);
        get(route)
            .send((response) ->{
                if (response.succeeded()) {
                    JsonArray responseArray = response.result().bodyAsJsonArray();
                    List<Action> componentActions = convertJsonArray(responseArray, Action.class);
                    future.complete(componentActions);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return null;
    }

    @Override
    public Future<List<Action>> resolveAction(final String inputId, final String componentId, final Action action) {
        final Future<List<Action>> future = Future.future();
        String route = String.format(InputManagerRoute.API_ACTIONS_ID, inputId, componentId, action.getId());
        post(route)
            .sendJsonObject(JsonObject.mapFrom(action), response -> {
                if (response.succeeded()) {
                    JsonArray responseArray = response.result().bodyAsJsonArray();
                    List<Action> actions = convertJsonArray(responseArray, Action.class);
                    future.complete(actions);
                } else {
                    future.tryFail(response.cause());
                }
            });
        return future;
    }
}
