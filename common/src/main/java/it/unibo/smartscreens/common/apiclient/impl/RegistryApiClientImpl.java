package it.unibo.smartscreens.common.apiclient.impl;

import it.unibo.smartscreens.common.apiclient.interfaces.RegistryApiClient;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.model.registry.MicroServiceEntity;
import it.unibo.smartscreens.common.model.registry.InputManagerEntity;
import it.unibo.smartscreens.common.model.registry.SmartScreenEntity;
import it.unibo.smartscreens.common.msroute.RegistryRoute;

import java.util.LinkedList;
import java.util.List;

public class RegistryApiClientImpl extends AbstractApiClient implements RegistryApiClient {

    public RegistryApiClientImpl(ServiceAddress serviceAddress, Vertx vertx) {
        super(serviceAddress, vertx);
    }

    @Override
    public Future<List<MicroServiceEntity>> getServices() {
        return getServices(new LinkedList<>());
    }

    @Override
    public Future<List<MicroServiceEntity>> getServices(List<String> microServiceNames) {
        Future<List<MicroServiceEntity>> future = Future.future();
        HttpRequest<Buffer> request = super.get(RegistryRoute.API_SERVICES);
        microServiceNames.forEach(msn -> request.addQueryParam("nameService", msn));
        request.send(res -> {
                if (res.succeeded()) {
                    future.complete(convertJsonArray(res.result().bodyAsJsonArray(), MicroServiceEntity.class));
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<MicroServiceEntity> getServicesById(String id) {
        Future<MicroServiceEntity> future = Future.future();
        super.get(RegistryRoute.API_SERVICES + "/" + id)
            .send(res -> {
                if (res.succeeded()) {
                    MicroServiceEntity serviceEntity = res.result().bodyAsJson(MicroServiceEntity.class);
                    future.complete(serviceEntity);
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<MicroServiceEntity> postServices(int port, String name, String desc) {
        return postServices(null, port, name, desc);
    }

    @Override
    public Future<MicroServiceEntity> postServices(String address, int port, String name, String desc) {
        return postServices(new ServiceAddress(address, port), name, desc);
    }

    @Override
    public Future<MicroServiceEntity> postServices(ServiceAddress serviceAddress, String name, String desc) {
        Future<MicroServiceEntity> future = Future.future();

        JsonObject body = new JsonObject()
            .put("serviceAddress", JsonObject.mapFrom(serviceAddress))
            .put("nameService", name)
            .put("description", desc);

        super.post(RegistryRoute.API_SERVICES)
            .sendJsonObject(body, res -> {
                if (res.succeeded()) {
                    MicroServiceEntity serviceEntity = res.result().bodyAsJson(MicroServiceEntity.class);
                    future.complete(serviceEntity);
                } else {
                    future.fail(res.cause());
                }
            });

        return future;
    }

    @Override
    public Future<List<SmartScreenEntity>> getSmartScreens() {
        return getSmartScreens(new LinkedList<>());
    }

    @Override
    public Future<List<SmartScreenEntity>> getSmartScreens(List<String> rooms) {
        return getSmartScreens(rooms, new LinkedList<>());
    }

    @Override
    public Future<List<SmartScreenEntity>> getSmartScreens(List<String> rooms, List<String> devicesIdInRoom) {
        Future<List<SmartScreenEntity>> future = Future.future();
        HttpRequest<Buffer> request = super.get(RegistryRoute.API_SMART_SCREEN);
        rooms.forEach(r -> request.addQueryParam("roomId", r));
        devicesIdInRoom.forEach(r -> request.addQueryParam("deviceIdInRoom", r));
        request.send(res -> {
            if (res.succeeded()) {
                future.complete(convertJsonArray(res.result().bodyAsJsonArray(), SmartScreenEntity.class));
            } else {
                future.fail(res.cause());
            }
        });

        return future;
    }

    @Override
    public Future<SmartScreenEntity> getSmartScreensById(String id) {
        Future<SmartScreenEntity> future = Future.future();
        super.get(RegistryRoute.API_SMART_SCREEN + "/" + id)
            .send(res -> {
                if (res.succeeded()) {
                    SmartScreenEntity smartScreenEntity = res.result().bodyAsJson(SmartScreenEntity.class);
                    future.complete(smartScreenEntity);
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<SmartScreenEntity> postSmartScreens(int port, String room, String deviceIdInRoom) {
        return postSmartScreens(null, port, room, deviceIdInRoom);
    }

    @Override
    public Future<SmartScreenEntity> postSmartScreens(String address, int port, String room, String deviceIdInRoom) {
        return postSmartScreens(new ServiceAddress(address, port), room, deviceIdInRoom);
    }

    @Override
    public Future<SmartScreenEntity> postSmartScreens(ServiceAddress serviceAddress, String room, String deviceIdInRoom) {
        Future<SmartScreenEntity> future = Future.future();

        JsonObject body = new JsonObject()
            .put("serviceAddress", JsonObject.mapFrom(serviceAddress))
            .put("roomId", room)
            .put("deviceIdInRoom", deviceIdInRoom);

        super.post(RegistryRoute.API_SMART_SCREEN)
            .sendJsonObject(body, res -> {
                if (res.succeeded()) {
                    SmartScreenEntity smartScreenEntity = res.result().bodyAsJson(SmartScreenEntity.class);
                    future.complete(smartScreenEntity);
                } else {
                    future.fail(res.cause());
                }
            });

        return future;
    }

    @Override
    public Future<List<InputManagerEntity>> getInputManagers() {
        return getInputManagers(new LinkedList<>());
    }

    @Override
    public Future<List<InputManagerEntity>> getInputManagers(List<String> rooms) {
        return getInputManagers(rooms, new LinkedList<>());
    }

    @Override
    public Future<List<InputManagerEntity>> getInputManagers(List<String> rooms, List<String> devicesIdInRoom) {
        Future<List<InputManagerEntity>> future = Future.future();

        HttpRequest<Buffer> request = super.get(RegistryRoute.API_INPUT_MANAGER);
        rooms.forEach(r -> request.addQueryParam("roomId", r));
        devicesIdInRoom.forEach(r -> request.addQueryParam("deviceIdInRoom", r));
        request.send(res -> {
            if (res.succeeded()) {
                future.complete(convertJsonArray(res.result().bodyAsJsonArray(), InputManagerEntity.class));
            } else {
                future.fail(res.cause());
            }
        });

        return future;
    }

    @Override
    public Future<InputManagerEntity> getInputManagersById(String id) {
        Future<InputManagerEntity> future = Future.future();
        super.get(RegistryRoute.API_INPUT_MANAGER + "/" + id)
            .send(res -> {
                if (res.succeeded()) {
                    InputManagerEntity inputManagerEntity = res.result().bodyAsJson(InputManagerEntity.class);
                    future.complete(inputManagerEntity);
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<InputManagerEntity> postInputManagers(int port, String room, String deviceIdInRoom) {
        return postInputManagers(null, port, room, deviceIdInRoom);
    }

    @Override
    public Future<InputManagerEntity> postInputManagers(String address, int port, String room, String deviceIdInRoom) {
        return postInputManagers(new ServiceAddress(address, port), room, deviceIdInRoom);
    }

    @Override
    public Future<InputManagerEntity> postInputManagers(ServiceAddress serviceAddress, String room, String deviceIdInRoom) {
        Future<InputManagerEntity> future = Future.future();

        JsonObject body = new JsonObject()
            .put("serviceAddress", JsonObject.mapFrom(serviceAddress))
            .put("roomId", room)
            .put("deviceIdInRoom", deviceIdInRoom);

        super.post(RegistryRoute.API_INPUT_MANAGER)
            .sendJsonObject(body, res -> {
                if (res.succeeded()) {
                    InputManagerEntity inputManagerEntity = res.result().bodyAsJson(InputManagerEntity.class);
                    future.complete(inputManagerEntity);
                } else {
                    future.fail(res.cause());
                }
            });

        return future;
    }
}
