package it.unibo.smartscreens.common.enums;

public enum SourceSocketMessageType {

    UPDATE_DATA (0);

    private final int type;

    SourceSocketMessageType(int type) {
        this.type = type;
    }

    public int type() {
        return type;
    }
}
