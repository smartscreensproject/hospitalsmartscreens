package it.unibo.smartscreens.common.apiclient.interfaces;

/**
 * Basic interface representing the available operations one can require from the Vital Signs microservice
 */
public interface VitalSignsApiClient extends DataManagerApiClient {
}
