package it.unibo.smartscreens.common.apiclient.impl;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import it.unibo.smartscreens.common.apiclient.interfaces.HeartBeatApiClient;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.msroute.HeartBeatRoute;

public class HeartBeatApiClientImpl extends AbstractApiClient implements HeartBeatApiClient {

    public HeartBeatApiClientImpl(ServiceAddress serviceAddress, Vertx vertx) {
        super(serviceAddress, vertx);
    }

    @Override
    public Future<Void> heartBeat(ServiceAddress serviceAddress) {
        Future<Void> future = Future.future();

        super.get(serviceAddress, HeartBeatRoute.API_HEARTBEAT)
            .send(res -> {
                if (res.succeeded()) {
                    future.complete();
                } else {
                    future.fail(res.cause());
                }
            });

        return future;
    }
}
