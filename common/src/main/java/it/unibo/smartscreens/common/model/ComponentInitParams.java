package it.unibo.smartscreens.common.model;

public class ComponentInitParams {

    private String inputManagerId;
    private String componentId;
    private String patientId;
    private ServiceRoute updateDataEndpoint;
    private ServiceRoute updateDataEndpointAsync;

    public ComponentInitParams() {}

    public ComponentInitParams(String inputManagerId, String componentId, String patientId, ServiceRoute updateDataEndpoint, ServiceRoute updateDataEndpointAsync) {
        this.inputManagerId = inputManagerId;
        this.componentId = componentId;
        this.patientId = patientId;
        this.updateDataEndpoint = updateDataEndpoint;
        this.updateDataEndpointAsync = updateDataEndpointAsync;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public String getInputManagerId() {
        return inputManagerId;
    }

    public void setInputManagerId(String inputManagerId) {
        this.inputManagerId = inputManagerId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public ServiceRoute getUpdateDataEndpoint() {
        return updateDataEndpoint;
    }

    public void setUpdateDataEndpoint(ServiceRoute updateDataEndpoint) {
        this.updateDataEndpoint = updateDataEndpoint;
    }

    public ServiceRoute getUpdateDataEndpointAsync() {
        return updateDataEndpointAsync;
    }

    public void setUpdateDataEndpointAsync(ServiceRoute updateDataEndpointAsync) {
        this.updateDataEndpointAsync = updateDataEndpointAsync;
    }
}
