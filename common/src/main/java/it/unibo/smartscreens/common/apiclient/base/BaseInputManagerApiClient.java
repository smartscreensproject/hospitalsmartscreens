package it.unibo.smartscreens.common.apiclient.base;

import it.unibo.smartscreens.common.apiclient.impl.AbstractApiClient;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClientOptions;

/**
 * Base class managing the necessary info to connect with the InputManager microservice
 */
public class BaseInputManagerApiClient extends AbstractApiClient {

    public BaseInputManagerApiClient(final String address, final int port, final Vertx vertx) {
        super(address, port, vertx);
    }

    public BaseInputManagerApiClient(final Vertx vertx) {
        this(HttpClientOptions.DEFAULT_DEFAULT_HOST,  HttpClientOptions.DEFAULT_DEFAULT_PORT, vertx);
    }
}
