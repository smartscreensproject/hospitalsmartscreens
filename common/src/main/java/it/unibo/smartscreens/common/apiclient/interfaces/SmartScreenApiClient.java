package it.unibo.smartscreens.common.apiclient.interfaces;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import it.unibo.smartscreens.common.model.*;

import java.util.List;

public interface SmartScreenApiClient {

    Future<Component> addComponent(Component component);

    Future<List<Component>> removeComponent(String componentId);

    Future<List<Component>> clearScreen();

    Future<Void> updateData(String componentId, String viewer, JsonObject data);

    Future<SmartScreen> getState();

    Future<Profile> getProfile();

    Future<UpdateComponentResponse> updateComponent(Component component);

    Future<Profile> changeGrid(Grid newGrid);
}
