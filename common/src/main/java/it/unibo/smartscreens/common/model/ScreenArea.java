package it.unibo.smartscreens.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

/**
 * ScreenArea POJO
 */
public class ScreenArea {

    private final static ObjectMapper objectMapper = new ObjectMapper();
    private final static String PREFIX_FREE_COMPONENT_ID = "E_";
    private Component component;
    private String viewer;
    private JsonNode data;
    private String dataReferredTo;

    public ScreenArea() {
    }

    public ScreenArea(Component component) {
        this(component, "", objectMapper.createObjectNode(), component.getPatientId());
    }

    public ScreenArea(Component component, String viewer, JsonNode data, String dataReferredTo) {
        this.component = component;
        this.viewer = viewer;
        this.data = data;
        this.dataReferredTo = dataReferredTo;
    }

    public static ScreenArea createEmptyArea(int position) {
        String id = PREFIX_FREE_COMPONENT_ID + UUID.randomUUID().toString().replaceAll("-", "_");
        return new ScreenArea(new Component(id, new ArrayList<>(Collections.singletonList(position)), "", ""));
    }

    public Component getComponent() {
        return component;
    }

    public ScreenArea setComponent(Component component) {
        this.component = component;
        return this;
    }

    public String getViewer() {
        return viewer;
    }

    public ScreenArea setViewer(String viewer) {
        this.viewer = viewer;
        return this;
    }

    public JsonNode getData() {
        return data;
    }

    public ScreenArea setData(JsonNode data) {
        this.data = data;
        return this;
    }

    public String getDataReferredTo() {
        return dataReferredTo;
    }

    public ScreenArea setDataReferredTo(String dataReferredTo) {
        this.dataReferredTo = dataReferredTo;
        return this;
    }

    @JsonIgnore
    public boolean isEmpty() {
        return component.getComponentId().startsWith(PREFIX_FREE_COMPONENT_ID);
    }
}
