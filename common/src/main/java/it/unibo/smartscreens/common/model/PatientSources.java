package it.unibo.smartscreens.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashSet;
import java.util.Set;

@JsonIgnoreProperties("_id")
public class PatientSources {

    private String patientId;
    private Set<String> sources;

    public PatientSources() {
        sources = new HashSet<>();
    }

    public PatientSources(String patientId) {
        this(patientId, new HashSet<>());
    }

    public PatientSources(String patientId, Set<String> sources) {
        this.patientId = patientId;
        this.sources = sources;
    }

    public String getPatientId() {
        return patientId;
    }

    public PatientSources setPatientId(String patientId) {
        this.patientId = patientId;
        return this;
    }

    public Set<String> getSources() {
        return sources;
    }

    public PatientSources setSources(Set<String> sources) {
        this.sources = sources;
        return this;
    }
}
