package it.unibo.smartscreens.common.model;

import java.util.List;

public class UpdateComponentResponse {

    private Component componentUpdated;
    private List<Component> newComponents;

    public UpdateComponentResponse() {
    }

    public UpdateComponentResponse(Component componentUpdated, List<Component> newComponents) {
        this.componentUpdated = componentUpdated;
        this.newComponents = newComponents;
    }

    public Component getComponentUpdated() {
        return componentUpdated;
    }

    public UpdateComponentResponse setComponentUpdated(Component componentUpdated) {
        this.componentUpdated = componentUpdated;
        return this;
    }

    public List<Component> getNewComponents() {
        return newComponents;
    }

    public UpdateComponentResponse setNewComponents(List<Component> newComponents) {
        this.newComponents = newComponents;
        return this;
    }
}
