package it.unibo.smartscreens.common.apiclient.impl;

import io.vertx.core.Vertx;
import it.unibo.smartscreens.common.apiclient.base.BasicDataManagerRoutes;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.msroute.TacRoute;

/**
 * Useful api client who manages the client requests related to the Tac microservice
 */
public class TacApiClientImpl extends DataManagerApiClientImpl {

    public TacApiClientImpl(String address, int port, Vertx vertx) {
        super(address, port, vertx, BasicDataManagerRoutes.create(TacRoute.API_TAC_INIT, TacRoute.API_TAC_DESTROY, TacRoute.API_TAC_ACTIONS));
    }

    public TacApiClientImpl(ServiceAddress serviceAddress, Vertx vertx) {
        super(serviceAddress, vertx, BasicDataManagerRoutes.create(TacRoute.API_TAC_INIT, TacRoute.API_TAC_DESTROY, TacRoute.API_TAC_ACTIONS));
    }
}
