package it.unibo.smartscreens.common.apiclient.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import it.unibo.smartscreens.common.apiclient.interfaces.PatientRelatedSourcesApiClient;
import it.unibo.smartscreens.common.model.PatientSources;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.msroute.PatientRelatedSourcesRoute;


public class PatientRelatedSourcesApiClientImpl extends AbstractApiClient implements PatientRelatedSourcesApiClient {

    public PatientRelatedSourcesApiClientImpl(ServiceAddress serviceAddress, Vertx vertx) {
        super(serviceAddress, vertx);
    }

    @Override
    public Future<PatientSources> getSourceByPatientId(String patientId) {
        Future<PatientSources> future = Future.future();

        super.get(PatientRelatedSourcesRoute.API_SOURCES + "/" + patientId)
            .send(res -> handleResponse(future, res));

        return future;
    }

    @Override
    public Future<PatientSources> addSource(String patientId, String source) {
        Future<PatientSources> future = Future.future();

        JsonObject body = new JsonObject()
            .put("source", source);
        super.post(PatientRelatedSourcesRoute.API_SOURCES + "/" + patientId)
            .sendJsonObject(body, res -> handleResponse(future, res));

        return future;
    }

    @Override
    public Future<PatientSources> removeSource(String patientId, String source) {
        Future<PatientSources> future = Future.future();

        JsonObject body = new JsonObject()
            .put("source", source);
        super.delete(PatientRelatedSourcesRoute.API_SOURCES + "/" + patientId)
            .sendJsonObject(body, res -> handleResponse(future, res));


        return future;
    }

    private void handleResponse(Future<PatientSources> future, AsyncResult<HttpResponse<Buffer>> response) {
        if (response.succeeded()) {
            PatientSources patientSources = response.result().bodyAsJsonObject().mapTo(PatientSources.class);
            future.complete(patientSources);
        } else {
            future.fail(response.cause());
        }
    }
}
