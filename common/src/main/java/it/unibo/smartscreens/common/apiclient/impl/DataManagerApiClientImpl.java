package it.unibo.smartscreens.common.apiclient.impl;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.smartscreens.common.apiclient.base.BasicDataManagerRoutes;
import it.unibo.smartscreens.common.apiclient.interfaces.DataManagerApiClient;
import it.unibo.smartscreens.common.model.Action;
import it.unibo.smartscreens.common.model.ComponentInitParams;
import it.unibo.smartscreens.common.model.ServiceAddress;

import java.util.List;

/**
 * Useful api client who manages the client requests related a generic data manager microservice
 */
public class DataManagerApiClientImpl extends AbstractApiClient implements DataManagerApiClient {

    private final BasicDataManagerRoutes baseRoutes;

    public DataManagerApiClientImpl(String address, int port, Vertx vertx, BasicDataManagerRoutes baseRoutes) {
        super(address, port, vertx);
        this.baseRoutes = baseRoutes;
    }

    public DataManagerApiClientImpl(ServiceAddress serviceAddress, Vertx vertx, BasicDataManagerRoutes baseRoutes) {
        super(serviceAddress, vertx);
        this.baseRoutes = baseRoutes;
    }

    @Override
    public Future<List<Action>> init(ComponentInitParams params) {
        final Future<List<Action>> future = Future.future();
        final JsonObject body = JsonObject.mapFrom(params);
        post(baseRoutes.getInitRoute())
            .sendJsonObject(body, res -> {
                if (res.succeeded()) {
                    JsonArray responseArray = res.result().bodyAsJsonArray();
                    List<Action> actions = convertJsonArray(responseArray, Action.class);
                    future.complete(actions);
                } else {
                    future.tryFail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future destroy() {
        final Future future = Future.future();
        post(baseRoutes.getDestroyRoute())
            .send(res -> {
                if (res.succeeded()) {
                    future.complete();
                } else {
                    future.tryFail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<List<Action>> getActions() {
        final Future<List<Action>> future = Future.future();
        get(baseRoutes.getActionsRoute())
            .send(res -> {
                if (res.succeeded()) {
                    JsonArray responseArray = res.result().bodyAsJsonArray();
                    List<Action> actions = convertJsonArray(responseArray, Action.class);
                    future.complete(actions);
                } else {
                    future.tryFail(res.cause());
                }
            });
        return future;
    }
}

