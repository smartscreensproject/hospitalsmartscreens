package it.unibo.smartscreens.common.apiclient.impl;

import it.unibo.smartscreens.common.apiclient.interfaces.SmartScreenApiClient;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import it.unibo.smartscreens.common.model.*;
import it.unibo.smartscreens.common.msroute.SmartScreenRoute;

import java.util.List;

public class SmartScreenApiClientImpl extends AbstractApiClient implements SmartScreenApiClient {

    public SmartScreenApiClientImpl(final ServiceAddress serviceAddress, final Vertx vertx) {
        super(serviceAddress, vertx);
    }

    @Override
    public Future<Component> addComponent(Component component) {
        final Future<Component> future = Future.future();
        super.post(SmartScreenRoute.API_COMPONENTS)
            .sendJsonObject(JsonObject.mapFrom(component), response -> {
                if (response.succeeded()) {
                    future.complete(response.result().bodyAsJsonObject().mapTo(Component.class));
                } else {
                    future.fail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<List<Component>> removeComponent(String componentId) {
        final Future<List<Component>> future = Future.future();
        super.delete(SmartScreenRoute.API_COMPONENTS + "/" + componentId)
            .send(response -> {
                if (response.succeeded()) {
                    List<Component> components = super.convertJsonArray(response.result().bodyAsJsonArray(), Component.class);
                    future.complete(components);
                } else {
                    future.fail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<List<Component>> clearScreen() {
        final Future<List<Component>> future = Future.future();
        super.delete(SmartScreenRoute.API_CLEAR)
            .send(response -> {
                if (response.succeeded()) {
                    List<Component> components = super.convertJsonArray(response.result().bodyAsJsonArray(), Component.class);
                    future.complete(components);
                } else {
                    future.fail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<Void> updateData(String componentId, String viewer, JsonObject data) {
        final Future<Void> future = Future.future();
        JsonObject body = new JsonObject()
            .put("componentId", componentId)
            .put("viewer", viewer)
            .put("data", data);
        super.post(SmartScreenRoute.API_UPDATE_DATA)
                .sendJsonObject(body, res-> this.handlerResponse(res, future));
        return future;
    }

    @Override
    public Future<SmartScreen> getState() {
        final Future<SmartScreen> future = Future.future();
        super.get(SmartScreenRoute.API_STATE)
            .send(response -> {
                if (response.succeeded()) {
                    SmartScreen state = response.result().bodyAsJsonObject().mapTo(SmartScreen.class);
                    future.complete(state);
                } else {
                    future.fail(response.cause());
                }
            });
        return future;
    }

    @Override
    public Future<Profile> getProfile() {
        Future<Profile> future = Future.future();
        super.get(SmartScreenRoute.API_PROFILE)
            .send(res -> {
                if (res.succeeded()) {
                    future.complete(res.result().bodyAsJsonObject().mapTo(Profile.class));
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<UpdateComponentResponse> updateComponent(Component component) {
        final Future<UpdateComponentResponse> future = Future.future();
        JsonObject body = new JsonObject()
            .put("positions", component.getPositions())
            .put("patientId", component.getPatientId())
            .put("sourceType", component.getSourceType());

        super.put(SmartScreenRoute.API_COMPONENTS + "/" + component.getComponentId())
            .sendJsonObject(body, response -> {
                if (response.succeeded()) {
                    future.complete(response.result().bodyAsJsonObject().mapTo(UpdateComponentResponse.class));
                } else {
                    future.fail(response.cause());
                }
            });

        return future;
    }

    @Override
    public Future<Profile> changeGrid(Grid newGrid) {
        Future<Profile> future = Future.future();
        super.put(SmartScreenRoute.API_CHANGE_GRID)
            .sendJsonObject(JsonObject.mapFrom(newGrid), response -> {
                if (response.succeeded()) {
                    Profile state = response.result().bodyAsJsonObject().mapTo(Profile.class);
                    future.complete(state);
                } else {
                    future.fail(response.cause());
                }
            });
        return future;
    }

    private void handlerResponse(AsyncResult<HttpResponse<Buffer>> response, Future<Void> future) {
        if (response.succeeded()) {
            future.complete();
        } else {
            future.fail(response.cause());
        }
    }
}
