package it.unibo.smartscreens.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Profile POJO
 */
public class Profile {

    private Grid grid;
    private List<Component> components;

    public Profile() {}

    public Profile(Grid grid, List<Component> components) {
        this.grid = grid;
        this.components = components;
    }

    public Grid getGrid() {
        return grid;
    }

    public Profile setGrid(Grid grid) {
        this.grid = grid;
        return this;
    }

    public List<Component> getComponents() {
        return components;
    }

    public Profile setComponents(List<Component> components) {
        this.components = components;
        return this;
    }

    @JsonIgnore
    public InputConfiguration toInputConfiguration() {
        return new InputConfiguration(this.getGrid(), this.getComponents().stream().map(c -> new ComponentWithActions(c, Collections.emptyList())).collect(Collectors.toList()));
    }
}
