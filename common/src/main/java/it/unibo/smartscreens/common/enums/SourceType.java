package it.unibo.smartscreens.common.enums;

import java.util.Arrays;
import java.util.Optional;

public enum SourceType {

    TAC("tac"),
    VITAL_SIGNS("vitalsigns"),
    VIDEO_STREAMS("videostreams");

    private final String name;

    SourceType(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Optional<SourceType> getSourceTypeFromName(final String name) {
        return Arrays.stream(values()).filter(st -> st.name.equalsIgnoreCase(name)).findFirst();
    }
}
