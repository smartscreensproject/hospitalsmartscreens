package it.unibo.smartscreens.common.apiclient.interfaces;

/**
 * Basic interface representing the available operations one can require from the video streams microservice
 */
public interface VideoStreamsApiClient extends DataManagerApiClient {
}
