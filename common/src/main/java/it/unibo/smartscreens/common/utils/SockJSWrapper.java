package it.unibo.smartscreens.common.utils;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import io.vertx.ext.web.handler.sockjs.SockJSSocket;
import java.util.function.BiConsumer;

public class SockJSWrapper {

    private Handler<SockJSSocket> onOpen;
    private BiConsumer<SockJSSocket, Buffer> onMessage;
    private Handler<SockJSSocket> onClose;

    public SockJSWrapper() {
    }

    public SockJSWrapper setOnOpen(Handler<SockJSSocket> onOpen) {
        this.onOpen = onOpen;
        return this;
    }

    public SockJSWrapper setOnMessage(BiConsumer<SockJSSocket, Buffer> onMessage) {
        this.onMessage = onMessage;
        return this;
    }

    public SockJSWrapper setOnClose(Handler<SockJSSocket> onClose) {
        this.onClose = onClose;
        return this;
    }

    public SockJSHandler build(Vertx vertx) {
        return this.build(vertx, new SockJSHandlerOptions());
    }

    public SockJSHandler build(Vertx vertx, SockJSHandlerOptions options) {
        SockJSHandler sockJSHandler = SockJSHandler.create(vertx, options);
        sockJSHandler.socketHandler(sockJSSocket -> {
            if (onOpen != null) {
                onOpen.handle(sockJSSocket);
            }
            if (onMessage != null) {
                sockJSSocket.handler(msg -> onMessage.accept(sockJSSocket, msg));
            }
            if (onClose != null) {
                sockJSSocket.endHandler(close -> onClose.handle(sockJSSocket));
            }
        });
        return sockJSHandler;
    }
}
