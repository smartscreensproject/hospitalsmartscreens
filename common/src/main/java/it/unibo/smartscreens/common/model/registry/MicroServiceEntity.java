package it.unibo.smartscreens.common.model.registry;

import it.unibo.smartscreens.common.model.ServiceAddress;

public class MicroServiceEntity extends AbstractEntity {

    private String name;
    private String description;

    public MicroServiceEntity() {
    }

    public MicroServiceEntity(ServiceAddress serviceAddress, String name, String description) {
        super(serviceAddress);
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public MicroServiceEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public MicroServiceEntity setDescription(String description) {
        this.description = description;
        return this;
    }
}
