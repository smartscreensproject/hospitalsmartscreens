package it.unibo.smartscreens.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import java.util.stream.Collectors;

/**
 * InputConfiguration POJO
 */
public class InputConfiguration {

    private Grid grid;
    private List<ComponentWithActions> componentsWithActions;

    public InputConfiguration() {}

    public InputConfiguration(Grid grid, List<ComponentWithActions> componentsWithActions) {
        this.grid = grid;
        this.componentsWithActions = componentsWithActions;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public List<ComponentWithActions> getComponentsWithActions() {
        return componentsWithActions;
    }

    public void setComponentsWithActions(List<ComponentWithActions> componentsWithActions) {
        this.componentsWithActions = componentsWithActions;
    }

    @JsonIgnore
    public Profile toProfile() {
        return new Profile(this.getGrid(), this.getComponentsWithActions().stream().map(ComponentWithActions::getComponent).collect(Collectors.toList()));
    }
}
