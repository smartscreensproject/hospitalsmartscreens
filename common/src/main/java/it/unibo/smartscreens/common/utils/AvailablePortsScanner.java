package it.unibo.smartscreens.common.utils;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.function.Function;
import java.util.function.Supplier;

public class AvailablePortsScanner {

    private static final int MIN_PORT_NUMBER = 2;
    private static final int MAX_PORT_NUMBER = 65535;

    /**
     * Checks if the specified port is between the range 2 - 65535 (extremes included)
     *
     * @param port the port to check
     */
    private void isPortValid(int port) {
        if (port < MIN_PORT_NUMBER || port > MAX_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid port number: " + port);
        }
    }

    /**
     * Creates a new server socket
     *
     * @param port      the port where should be created, if it is 0 it will be randomly chosen between the available ones
     * @param onSuccess the callback to call after the deploy is succeeded
     * @param onFailure the callback to call after the deploy is failed
     * @param <T>       the type returned by the callbacks
     * @return the value returned by the callbacks
     */
    private <T> T createServerSocket(int port, Function<ServerSocket, T> onSuccess, Supplier<T> onFailure) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            serverSocket.setReuseAddress(true);
            return onSuccess.apply(serverSocket);
        } catch (IOException e) {
            /* failed opening the socket */
            return onFailure.get();
        }
    }

    /**
     * Closes the server socket connection
     *
     * @param serverSocket the server socket to close
     */
    private void close(ServerSocket serverSocket) {
        try {
            serverSocket.close();
        } catch (IOException e) {
            System.err.println("IOException on close()");
        }
    }

    /**
     * Finds a random free port
     *
     * @return the free port number
     */
    public int findFreePort() {
        return createServerSocket(0, (serverSocket) -> {
            int freePort = serverSocket.getLocalPort();
            close(serverSocket);
            return freePort;
        }, () -> {
            throw new IllegalStateException("Could not find a free TCP/IP port");
        });
    }

    /**
     * Finds a free port starting from the specified one
     *
     * @param startingPort the first port used when starting the availability check
     * @return the first free port number found
     */
    public int findFreePort(int startingPort) {
        int port = startingPort;
        while (!isPortAvailable(port) && port <= MAX_PORT_NUMBER) {
            port++;
        }
        isPortValid(port);
        return port;
    }

    /**
     * Checks if the specified port is available
     *
     * @param port the port to check
     * @return true if the port is available, false otherwise
     */
    public boolean isPortAvailable(int port) {
        isPortValid(port);

        return createServerSocket(port, (serverSocket) -> {
            close(serverSocket);
            return true;
        }, () -> false);
    }

    public static void main(String[] args) {
        AvailablePortsScanner a = new AvailablePortsScanner();
        System.out.println(a.findFreePort());
        System.out.println(a.findFreePort(4200));
    }
}
