package it.unibo.smartscreens.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * SmartScreen POJO
 */
public class SmartScreen {

    private final static int DEFAULT_GRID_SIZE = 1;
    private String id;
    private String room;
    private String deviceIdInRoom;
    private Grid grid;
    private List<ScreenArea> areas;

    public SmartScreen() {
        grid = new Grid(DEFAULT_GRID_SIZE, DEFAULT_GRID_SIZE);
    }

    public SmartScreen(String room, String deviceIdInRoom, Grid grid) {
        this(room, deviceIdInRoom, grid, new LinkedList<>());
    }

    public SmartScreen(String room, String deviceIdInRoom, Grid grid, List<ScreenArea> areas) {
        this.room = room;
        this.deviceIdInRoom = deviceIdInRoom;
        this.grid = grid;
        this.areas = areas;
        fillEmptyArea();
    }

    private void fillEmptyArea() {
        List<Integer> cellBusy = getAreasAsStream()
            .flatMap(sa -> sa.getComponent().getPositionsAsStream())
            .collect(Collectors.toList());
        IntStream.rangeClosed(1, grid.getRows()*grid.getColumns())
            .boxed()
            .filter(pos -> !cellBusy.contains(pos))
            .forEach(pos -> areas.add(ScreenArea.createEmptyArea(pos)));
    }

    public String getId() {
        return id;
    }

    public SmartScreen setId(String id) {
        this.id = id;
        return this;
    }

    public String getRoom() {
        return room;
    }

    public SmartScreen setRoom(String room) {
        this.room = room;
        return this;
    }

    public String getDeviceIdInRoom() {
        return deviceIdInRoom;
    }

    public SmartScreen setDeviceIdInRoom(String deviceIdInRoom) {
        this.deviceIdInRoom = deviceIdInRoom;
        return this;
    }

    public Grid getGrid() {
        return grid;
    }

    public SmartScreen setGrid(Grid grid) {
        this.grid = grid;
        return this;
    }

    @JsonIgnore
    public List<Component> getComponents() {
        return getAreasAsStream()
            .map(ScreenArea::getComponent)
            .collect(Collectors.toList());
    }

    @JsonIgnore
    public Profile getProfile() {
        return new Profile()
            .setGrid(getGrid())
            .setComponents(getComponents());
    }

    public void changeGrid(Grid grid) {
        while (this.grid.getColumns() < grid.getColumns()) {
            addColumn();
        }

        while (this.grid.getRows() < grid.getRows()) {
            addRaw();
        }
        //fill new columns and/or raws with empty ScreenArea
        fillEmptyArea();

        while (this.grid.getRows() > grid.getRows()) {
            removeRaw();
        }

        while (this.grid.getColumns() > grid.getColumns()) {
            removeColumn();
        }

        removeComponentsWithoutPosition();
        assert this.grid == grid;
    }

    /**
     *
     * @param cell position of the cell
     * @return the raw in interval [0,...,nRaws[
     */
    private int getRawByCell(int cell) {
        return (cell - 1) / grid.getColumns();
    }

    private void addColumn() {
        //compute new position value of the cells with a new column
        getAreas().forEach(a -> a.getComponent().getPositions().replaceAll(pos -> pos + getRawByCell(pos)));
        //add the new column
        this.grid.setColumns(this.grid.getColumns() + 1);
    }

    private void removeColumn() {
        List<Integer> columnToRemove = IntStream.rangeClosed(1, grid.getRows())
            .boxed()
            .map(p -> p * grid.getColumns())
            .collect(Collectors.toList());
        this.areas.forEach(a -> a.getComponent().getPositions().removeIf(columnToRemove::contains));
        getAreas().forEach(a -> a.getComponent().getPositions().replaceAll(pos -> pos - getRawByCell(pos)));
        this.grid.setColumns(this.grid.getColumns() - 1);
    }

    private void addRaw() {
        this.grid.setRows(this.grid.getRows() + 1);
    }

    private void removeRaw() {
        List<Integer> rawToRemove = IntStream
            .rangeClosed((grid.getRows() - 1) * grid.getColumns() + 1, grid.getRows()*grid.getColumns())
            .boxed()
            .collect(Collectors.toList());
        this.areas.forEach(a -> a.getComponent().getPositions().removeAll(rawToRemove));
        this.grid.setRows(this.grid.getRows() - 1);
    }

    private void removeComponentsWithoutPosition() {
        this.areas.removeIf(a -> a.getComponent().getPositions().isEmpty());
    }

    public List<ScreenArea> getAreas() {
        return areas;
    }

    @JsonIgnore
    public Stream<ScreenArea> getAreasAsStream() {
        return areas.stream();
    }

    @JsonIgnore
    public List<ScreenArea> getFreeAreas() {
        return getFreeAreasAsStream().collect(Collectors.toList());
    }

    /***
     * Substitute the area with component specified with empty ScreenArea
     * @param componentId Id of component contained in the ScreenArea to cleaning
     * @return Optional with the removed screenArea, empty if screenArea required not found
     */
    public Optional<ScreenArea> cleanArea(String componentId) {
        Optional<ScreenArea> area = getAreasAsStream().filter(sa -> sa.getComponent().getComponentId().equals(componentId)).findFirst();
        if (area.isPresent()) {
            areas.remove(area.get());
            area.get().getComponent().getPositionsAsStream().forEach(pos -> areas.add(ScreenArea.createEmptyArea(pos)));
        }
        return area;
    }

    public Optional<ScreenArea> addArea(Component component) {
        // check all cell free
        List<Integer> freeCell = getFreeCell();
        if (!freeCell.containsAll(component.getPositions())) {
            return Optional.empty();
        }
        if (!isCellGroupValid(component.getPositions())) {
            return Optional.empty();
        }
        //remove empty Area
        areas.removeIf(sa -> component.getPositions().containsAll(sa.getComponent().getPositions()));
        //add new Area
        Optional<ScreenArea> sa = Optional.of(new ScreenArea(component));
        areas.add(sa.get());
        return sa;
    }

    private boolean isCellGroupValid(List<Integer> positions) {
        if (positions.isEmpty()) {
            return false;
        }
        int topLeftCorner = positions.stream().min(Integer::compareTo).get();
        int lastPosOfFirstRawToCheck = grid.getColumns()*(getRawByCell(topLeftCorner)+1);
        //get first raw
        List<Integer> rawToCheck = positions.stream()
            .filter(p -> p <= lastPosOfFirstRawToCheck)
            .collect(Collectors.toList());
        List<Integer> copy = new LinkedList<>(positions);
        //until copy list not empty try to remove a whole raw
        while (!copy.isEmpty() && copy.containsAll(rawToCheck)) {
            copy.removeAll(rawToCheck);
            rawToCheck.replaceAll(elem -> elem + grid.getColumns());
        }
        /*
           if copy list is empty the group is a rectangle, so the group is valid!!!
           if copy list is not empty means that in copy missing at least one cell of
           rawToCheck (in this case is possible that una raw before or after have more
           cell than the first raw) anyway the group of cell isn't a rectangle and
           the group isn't valid
        */
        return copy.isEmpty();
    }

    @JsonIgnore
    public List<Integer> getFreeCell() {
        return getFreeAreasAsStream()
            .flatMap(sa -> sa.getComponent().getPositionsAsStream())
            .collect(Collectors.toList());
    }

    @JsonIgnore
    public Stream<ScreenArea> getFreeAreasAsStream() {
        return areas.stream().filter(ScreenArea::isEmpty);
    }

    public SmartScreen setAreas(List<ScreenArea> areas) {
        this.areas = areas;
        return this;
    }

    public void clear() {
        this.areas.clear();
        fillEmptyArea();
    }

    public boolean updateComponent(Component component) {
        Optional<ScreenArea> screenArea = getAreasAsStream()
            .filter(sa -> sa.getComponent().getComponentId().equals(component.getComponentId()))
            .findAny();
        //check component already present and new positions are valid
        if (!screenArea.isPresent() || !isCellGroupValid(component.getPositions())) {
            return false;
        }

        List<Integer> positionAvailable = getFreeCell();
        positionAvailable.addAll(screenArea.get().getComponent().getPositions());
        if (positionAvailable.containsAll(component.getPositions())) {
            //remove empty areas to add to th component
            areas.removeIf(sa ->  sa.isEmpty() &&
                component.getPositions().containsAll(sa.getComponent().getPositions()));
            screenArea.get().setComponent(component);
            //fill the areas precedent busy from the component and now empty
            fillEmptyArea();
            return true;
        }
        return false;
    }
}
