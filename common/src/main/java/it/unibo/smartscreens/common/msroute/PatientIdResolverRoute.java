package it.unibo.smartscreens.common.msroute;

public class PatientIdResolverRoute {

    public static final String API_ROOT = "/api/patient-id-resolver";
    public static final String API_PATIENT = API_ROOT + "/patient";
    public static final String API_SEARCH_PATIENT = API_PATIENT + "/search";
}
