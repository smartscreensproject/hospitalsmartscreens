package it.unibo.smartscreens.common.apiclient.interfaces;

import io.vertx.core.Future;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.model.registry.MicroServiceEntity;
import it.unibo.smartscreens.common.model.registry.InputManagerEntity;
import it.unibo.smartscreens.common.model.registry.SmartScreenEntity;

import java.util.List;

public interface RegistryApiClient {

    Future<List<MicroServiceEntity>> getServices();

    Future<List<MicroServiceEntity>> getServices(List<String> microServiceNames);

    Future<MicroServiceEntity> getServicesById(String id);

    Future<MicroServiceEntity> postServices(int port, String name, String desc);

    Future<MicroServiceEntity> postServices(String address, int port, String name, String desc);

    Future<MicroServiceEntity> postServices(ServiceAddress serviceAddress, String name, String desc);

    Future<List<SmartScreenEntity>> getSmartScreens();

    Future<List<SmartScreenEntity>> getSmartScreens(List<String> rooms);

    Future<List<SmartScreenEntity>> getSmartScreens(List<String> rooms, List<String> devicesIdInRoom);

    Future<SmartScreenEntity> getSmartScreensById(String id);

    Future<SmartScreenEntity> postSmartScreens(int port, String room, String deviceIdInRoom);

    Future<SmartScreenEntity> postSmartScreens(String address, int port, String room, String deviceIdInRoom);

    Future<SmartScreenEntity> postSmartScreens(ServiceAddress serviceAddress, String room, String deviceIdInRoom);

    Future<List<InputManagerEntity>> getInputManagers();

    Future<List<InputManagerEntity>> getInputManagers(List<String> rooms);

    Future<List<InputManagerEntity>> getInputManagers(List<String> rooms, List<String> devicesIdInRoom);

    Future<InputManagerEntity> getInputManagersById(String id);

    Future<InputManagerEntity> postInputManagers(int port, String room, String deviceIdInRoom);

    Future<InputManagerEntity> postInputManagers(String address, int port, String room, String deviceIdInRoom);

    Future<InputManagerEntity> postInputManagers(ServiceAddress serviceAddress, String room, String deviceIdInRoom);
}
