package it.unibo.smartscreens.common.apiclient.impl;

import io.vertx.core.Vertx;
import it.unibo.smartscreens.common.apiclient.base.BasicDataManagerRoutes;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.msroute.VitalSignsRoute;

/**
 * Useful api client who manages the client requests related to the Vital Signs microservice
 */
public class VitalSignsApiClientImpl extends DataManagerApiClientImpl {

    public VitalSignsApiClientImpl(String address, int port, Vertx vertx) {
        super(address, port, vertx, BasicDataManagerRoutes.create(VitalSignsRoute.API_INIT, VitalSignsRoute.API_DESTROY, VitalSignsRoute.API_ACTIONS));
    }

    public VitalSignsApiClientImpl(ServiceAddress serviceAddress, Vertx vertx) {
        super(serviceAddress, vertx, BasicDataManagerRoutes.create(VitalSignsRoute.API_INIT, VitalSignsRoute.API_DESTROY, VitalSignsRoute.API_ACTIONS));
    }

}
