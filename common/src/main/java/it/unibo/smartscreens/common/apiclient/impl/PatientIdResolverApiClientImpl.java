package it.unibo.smartscreens.common.apiclient.impl;

import it.unibo.smartscreens.common.apiclient.interfaces.PatientIdResolverApiClient;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import it.unibo.smartscreens.common.model.patient.Location;
import it.unibo.smartscreens.common.model.patient.Patient;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.msroute.PatientIdResolverRoute;

import java.util.List;
import java.util.Map;

public class PatientIdResolverApiClientImpl extends AbstractApiClient implements PatientIdResolverApiClient {


    public PatientIdResolverApiClientImpl(ServiceAddress serviceAddress, Vertx vertx) {
        super(serviceAddress, vertx);
    }

    @Override
    public Future<Patient> getPatientById(String patientId) {
        Future<Patient> future = Future.future();

        super.get(PatientIdResolverRoute.API_PATIENT + "/" + patientId)
            .send(res -> {
                if(res.succeeded()) {
                    future.complete(res.result().bodyAsJsonObject().mapTo(Patient.class));
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<List<Patient>> getPatients() {
        Future<List<Patient>> future = Future.future();

        super.get(PatientIdResolverRoute.API_PATIENT)
            .send(res -> {
                if(res.succeeded()) {
                    future.complete(convertJsonArray(res.result().bodyAsJsonArray(), Patient.class));
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<List<Patient>> getPatientsByCodes(Map<String, String> queryCodes) {
        Future<List<Patient>> future = Future.future();
        JsonObject body = JsonObject.mapFrom(queryCodes);
        super.post(PatientIdResolverRoute.API_SEARCH_PATIENT)
            .sendJsonObject(body, res -> {
                if(res.succeeded()) {
                    future.complete(convertJsonArray(res.result().bodyAsJsonArray(), Patient.class));
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<Patient> postPatient(String name, String lastName, Map<String, String> codes, Location location) {
        Future<Patient> future = Future.future();
        JsonObject body = new JsonObject()
            .put("firstName", name)
            .put("lastName", lastName)
            .put("codes", JsonObject.mapFrom(codes))
            .put("location", JsonObject.mapFrom(location));
        super.post(PatientIdResolverRoute.API_PATIENT)
            .sendJsonObject(body, res -> {
                if(res.succeeded()) {
                    future.complete(res.result().bodyAsJsonObject().mapTo(Patient.class));
                } else {
                    future.fail(res.cause());
                }
            });
        return future;
    }

    @Override
    public Future<Patient> updatePatient(Patient patient) {
        Future<Patient> future = Future.future();

        super.put(PatientIdResolverRoute.API_PATIENT + "/" + patient.getPatientId())
            .sendJsonObject(JsonObject.mapFrom(patient), res -> {
                if(res.succeeded()) {
                    future.complete(res.result().bodyAsJsonObject().mapTo(Patient.class));
                } else {
                    future.fail(res.cause());
                }
            });

        return future;
    }


}
