package it.unibo.smartscreens.common.model;

public class ServiceAddress {

    private String address;
    private int port;

    public ServiceAddress() {
    }

    public ServiceAddress(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public String getAddress() {
        return address;
    }

    public ServiceAddress setAddress(String address) {
        this.address = address;
        return this;
    }

    public int getPort() {
        return port;
    }

    public ServiceAddress setPort(int port) {
        this.port = port;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceAddress that = (ServiceAddress) o;
        return getPort() == that.getPort() &&
            getAddress().equals(that.getAddress());
    }

    @Override
    public String toString() {
        return address + ':' + port;
    }
}
