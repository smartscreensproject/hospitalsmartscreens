package it.unibo.smartscreens.common.model.tac;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.List;

/**
 * Tac POJO
 */
public class Tac {

    private String id;
    private String date;
    private String name;
    private TacPatient patient;
    private List<TacImage> images;

    public Tac() {}

    public Tac(String id, String date, String name, TacPatient patient, List<TacImage> images) {
        this.id = id;
        this.date = date;
        this.name = name;
        this.patient = patient;
        this.images = images;
    }

    @JsonAlias("_id")
    public String getId() {
        return id;
    }

    @JsonAlias("_id")
    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TacPatient getPatient() {
        return patient;
    }

    public void setPatient(TacPatient patient) {
        this.patient = patient;
    }

    public List<TacImage> getImages() {
        return images;
    }

    public void setImages(List<TacImage> images) {
        this.images = images;
    }
}
