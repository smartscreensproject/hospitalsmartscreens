package it.unibo.smartscreens.common.model;

public class ServiceRoute extends ServiceAddress {

    private String route;

    public ServiceRoute(String address, int port, String route) {
        super(address, port);
        this.route = route;
    }

    public ServiceRoute() {}

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }
}
