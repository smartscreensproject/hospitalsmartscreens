package it.unibo.smartscreens.common.model;

import java.util.List;

/**
 * ComponentWithActions POJO, consisting by a composition between a Component instance and its related Action instances
 */
public class ComponentWithActions {

    private Component component;
    private List<Action> actions;

    public ComponentWithActions() {}

    public ComponentWithActions(Component component, List<Action> actions) {
        this.component = component;
        this.actions = actions;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }
}
