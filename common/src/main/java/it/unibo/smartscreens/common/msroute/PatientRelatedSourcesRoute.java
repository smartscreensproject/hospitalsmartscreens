package it.unibo.smartscreens.common.msroute;

public class PatientRelatedSourcesRoute {

    public static final String API_ROOT = "/api/patient-related-sources";
    public static final String API_SOURCES = API_ROOT + "/sources";
}
