package it.unibo.smartscreens.common.model;

import java.util.List;

/**
 * Action POJO
 */
public class Action {

    private String id;
    private String name;
    private String description;
    private String href;
    private String method;
    private List<ActionParam> params;

    public Action() {}

    public Action(String id, String name, String description, String href, String method, List<ActionParam> params) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.href = href;
        this.method = method;
        this.params = params;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<ActionParam> getParams() {
        return params;
    }

    public void setParams(List<ActionParam> params) {
        this.params = params;
    }
}
