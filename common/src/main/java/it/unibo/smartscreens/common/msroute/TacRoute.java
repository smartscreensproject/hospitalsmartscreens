package it.unibo.smartscreens.common.msroute;

public class TacRoute {

    // Api root
    public static final String API_ROOT = "/api/tac";
    // Init
    public static final String API_TAC_INIT = API_ROOT + "/init";
    // Actions
    public static final String API_TAC_ACTIONS = API_ROOT + "/actions";
    // Destroy
    public static final String API_TAC_DESTROY = API_ROOT + "/destroy";
    // Search
    public static final String API_TAC_PATIENT_SEARCH = API_ROOT + "/patient-search";
    // Tac
    public static final String API_TAC_PATIENT_TAC = API_ROOT + "/patient/%s/tac/%s";
    // TacImage
    public static final String API_TAC_PATIENT_TAC_IMAGE = API_TAC_PATIENT_TAC + "/images/%s";
}
