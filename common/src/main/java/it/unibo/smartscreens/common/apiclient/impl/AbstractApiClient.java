package it.unibo.smartscreens.common.apiclient.impl;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.client.predicate.ErrorConverter;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import it.unibo.smartscreens.common.model.ServiceAddress;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractApiClient {

    private final String NAME = getClass().getCanonicalName();
    private final ServiceAddress serviceAddress;
    private static WebClient webClient;

    public AbstractApiClient(final String address, final int port, final Vertx vertx) {
        this(new ServiceAddress(address, port), vertx);
    }

    public AbstractApiClient(final ServiceAddress serviceAddress, final Vertx vertx) {
        this.serviceAddress = serviceAddress;
        if (webClient == null) {
            webClient = WebClient.create(vertx, new WebClientOptions().setLogActivity(true));
        }
    }

    protected ResponsePredicate getSuccessPredicate() {
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        ErrorConverter converter = ErrorConverter.createFullBody(result ->
            new Throwable(String.format("[%s.%s]: %s", NAME, methodName, result.response().statusMessage()))
        );
        return ResponsePredicate.create(ResponsePredicate.SC_SUCCESS, converter);
    }

    protected HttpRequest<Buffer> post(String requestURI) {
        return post(this.serviceAddress, requestURI);
    }

    protected HttpRequest<Buffer> post(ServiceAddress serviceAddress, String requestURI) {
        return webClient
            .post(serviceAddress.getPort(), serviceAddress.getAddress(), requestURI)
            .expect(getSuccessPredicate());
    }

    protected HttpRequest<Buffer> get(String requestURI) {
        return get(this.serviceAddress, requestURI);
    }

    protected HttpRequest<Buffer> get(ServiceAddress serviceAddress, String requestURI) {
        return webClient
            .get(serviceAddress.getPort(), serviceAddress.getAddress(), requestURI)
            .expect(getSuccessPredicate());
    }

    protected HttpRequest<Buffer> delete(String requestURI) {
        return webClient
            .delete(serviceAddress.getPort(), serviceAddress.getAddress(), requestURI)
            .expect(getSuccessPredicate());
    }

    protected HttpRequest<Buffer> put(String requestURI) {
        return webClient
            .put(serviceAddress.getPort(), serviceAddress.getAddress(), requestURI)
            .expect(getSuccessPredicate());
    }

    protected <T> List<T> convertJsonArray(JsonArray jsonArray, Class<T> type) {
        return jsonArray.stream().map(obj -> JsonObject.mapFrom(obj).mapTo(type)).collect(Collectors.toList());
    }
}
