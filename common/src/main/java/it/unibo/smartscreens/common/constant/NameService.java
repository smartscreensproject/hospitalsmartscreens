package it.unibo.smartscreens.common.constant;

public class NameService {
    public static final String PATIENT_ID_RESOLVER = "Patient id resolver";
    public static final String PATIENT_RELATED_SOURCE = "Patient related source";
    public static final String REGISTRY = "Registry";
    public static final String TAC = "Tac";
    public static final String VITAL_SIGNS_SOURCE = "vital-signs-source";
    public static final String VIDEO_STREAMS_SOURCE = "video-streams-source";

    private NameService() {
    }
}
