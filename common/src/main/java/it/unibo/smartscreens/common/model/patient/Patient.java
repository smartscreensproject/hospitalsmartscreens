package it.unibo.smartscreens.common.model.patient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties("_id")
public class Patient {

    private String patientId;
    private String firstName;
    private String lastName;
    private Location location;
    private Map<String, String> codes;

    public Patient() {
    }

    public Patient(String patientId, String firstName, String lastName) {
        this(patientId, firstName, lastName, new Location("", ""), new HashMap<>());
    }

    public Patient(String patientId, String firstName, String lastName, Location location, Map<String, String> codes) {
        this.patientId = patientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.location = location;
        this.codes = codes;
    }

    public String getPatientId() {
        return patientId;
    }

    public Patient setPatientId(String patientId) {
        this.patientId = patientId;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Patient setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Patient setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Location getLocation() {
        return location;
    }

    public Patient setLocation(Location location) {
        this.location = location;
        return this;
    }

    public Map<String, String> getCodes() {
        return codes;
    }

    public Patient setCodes(Map<String, String> codes) {
        this.codes = codes;
        return this;
    }
}
