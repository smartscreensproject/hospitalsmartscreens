package it.unibo.smartscreens.common.apiclient.interfaces;

import io.vertx.core.Future;
import it.unibo.smartscreens.common.model.patient.Location;
import it.unibo.smartscreens.common.model.patient.Patient;

import java.util.List;
import java.util.Map;

public interface PatientIdResolverApiClient {

    Future<Patient> getPatientById(String patientId);

    Future<List<Patient>> getPatients();

    Future<List<Patient>> getPatientsByCodes(Map<String, String> queryCodes);

    Future<Patient> postPatient(String name, String surname, Map<String, String> codes, Location location);

    Future<Patient> updatePatient(Patient patient);
}
