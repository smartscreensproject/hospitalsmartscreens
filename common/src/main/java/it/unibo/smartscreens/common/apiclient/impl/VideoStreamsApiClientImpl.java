package it.unibo.smartscreens.common.apiclient.impl;

import io.vertx.core.Vertx;
import it.unibo.smartscreens.common.apiclient.base.BasicDataManagerRoutes;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.msroute.VideoStreamsRoute;

/**
 * Useful api client who manages the client requests related to the Video Stream microservice
 */
public class VideoStreamsApiClientImpl extends DataManagerApiClientImpl {

    public VideoStreamsApiClientImpl(String address, int port, Vertx vertx) {
        super(address, port, vertx, BasicDataManagerRoutes.create(VideoStreamsRoute.API_INIT, VideoStreamsRoute.API_DESTROY, VideoStreamsRoute.API_ACTIONS));
    }

    public VideoStreamsApiClientImpl(ServiceAddress serviceAddress, Vertx vertx) {
        super(serviceAddress, vertx, BasicDataManagerRoutes.create(VideoStreamsRoute.API_INIT, VideoStreamsRoute.API_DESTROY, VideoStreamsRoute.API_ACTIONS));
    }

}
