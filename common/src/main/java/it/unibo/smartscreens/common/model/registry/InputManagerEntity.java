package it.unibo.smartscreens.common.model.registry;

import it.unibo.smartscreens.common.model.ServiceAddress;

public class InputManagerEntity extends AbstractEntity {

    private String roomId;
    private String deviceIdInRoom;

    public InputManagerEntity() {
    }

    public InputManagerEntity(ServiceAddress serviceAddress, String roomId, String deviceIdInRoom) {
        super(serviceAddress);
        this.roomId = roomId;
        this.deviceIdInRoom = deviceIdInRoom;
    }

    public String getRoomId() {
        return roomId;
    }

    public InputManagerEntity setRoomId(String roomId) {
        this.roomId = roomId;
        return this;
    }

    public String getDeviceIdInRoom() {
        return deviceIdInRoom;
    }

    public InputManagerEntity setDeviceIdInRoom(String deviceIdInRoom) {
        this.deviceIdInRoom = deviceIdInRoom;
        return this;
    }
}
