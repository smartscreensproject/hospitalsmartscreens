package it.unibo.smartscreens.common.msroute;

public class RegistryRoute {

    public static final String API_ROOT = "/api/registry";
    public static final String API_SERVICES= API_ROOT + "/services";
    public static final String API_INPUT_MANAGER = API_ROOT + "/input-managers";
    public static final String API_SMART_SCREEN = API_ROOT + "/smart-screens";
}
