package it.unibo.smartscreens.common.apiclient.interfaces;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import it.unibo.smartscreens.common.model.*;

import java.util.List;

/**
 * Basic interface representing the available operations provided by the InputManager microservice
 */
public interface InputManagerApiClient {

    /**
     * Changes the current Grid of the selected input device
     * @param inputId the input device id
     * @param newGrid the new grid
     * @return a future representing the result of the request
     */
    Future<InputConfiguration> changeGrid(final String inputId, final Grid newGrid);

    /**
     * Saves the current profile, storing it for future usage
     * @param inputId the input device id
     * @param newProfileName the new profile name
     * @return a future representing the result of the request
     */
    Future<JsonObject> saveProfile(final String inputId, final String newProfileName);

    /**
     * Loads the requested profile
     * @param inputId the input device id
     * @param newProfileName the new profile name
     * @return a future representing the result of the request
     */
    Future<Profile> loadProfile(final String inputId, final String newProfileName);

    /**
     * Clear the current configuration
     * @param inputId the input device id
     * @return a future representing the result of the request
     */
    Future<List<Component>> clear(final String inputId);

    /**
     * Retrieve the current input state
     * @param inputId the input device id
     * @return a future representing the result of the request
     */
    Future<InputConfiguration> retrieveInputState(final String inputId);

    /**
     * Retrieves the current components
     * @param inputId the input device id
     * @return a future representing the result of the request
     */
    //Future<List<ComponentWithActions>> retrieveComponents(final String inputId);

    /**
     * Adds a new component to the configuration
     * @param inputId the input device id
     * @param newComponent the new Component info, whose id could be empty or null
     * @return a future representing the result of the request
     */
    Future<ComponentWithActions> addComponent(final String inputId, final Component newComponent);

    /**
     * Updates the current component
     * @param inputId the input device id
     * @param newComponent the new Component info, whose id must be present in the current configuration
     * @return a future representing the result of the request
     */
    Future<UpdateComponentResponse> updateComponent(final String inputId, final Component newComponent);

    /**
     * Delete the requested component
     * @param inputId the input device id
     * @param componentId the component id that must be deleted
     * @return a future representing the result of the request
     */
    Future<List<Component>> removeComponent(final String inputId, final String componentId);

    /**
     * Retrieves the request component actions
     * @param inputId the input device id
     * @param componentId the request component id
     * @return a future representing the result of the request
     */
    Future<List<Action>> retrieveComponentActions(final String inputId, final String componentId);

    /**
     * Resolve the requested action
     * @param inputId the input device id
     * @param componentId the request component id
     * @param action the action
     * @return a future representing the result of the request
     */
    Future<List<Action>> resolveAction(final String inputId, final String componentId, final Action action);
}

