package it.unibo.smartscreens.common.apiclient.interfaces;

/**
 * Basic interface representing the available operations one can require from the Tac microservice
 */
public interface TacApiClient extends DataManagerApiClient {

}
