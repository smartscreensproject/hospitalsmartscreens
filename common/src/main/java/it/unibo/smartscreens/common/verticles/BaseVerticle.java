package it.unibo.smartscreens.common.verticles;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.net.SocketAddress;
import io.vertx.ext.web.handler.CorsHandler;
import it.unibo.smartscreens.common.apiclient.impl.RegistryApiClientImpl;
import it.unibo.smartscreens.common.apiclient.interfaces.RegistryApiClient;
import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.core.json.JsonObject;
import it.unibo.smartscreens.common.model.ServiceAddress;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import io.vertx.ext.web.RoutingContext;
import it.unibo.smartscreens.common.msroute.HeartBeatRoute;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Basic Verticle implementation
 */
public class BaseVerticle extends AbstractVerticle {

    private final String NAME = getClass().getCanonicalName();
    private final String CONTENT_TYPE_HEADER_KEY = "Content-Type";
    private final String APPLICATION_JSON_MIME = "application/json";
    private final String REGISTRY_FILE_CONFIG = "/registryConfig.json";
    private final String VERTICLE_CONFIG = "/config.json";

    public static final String ADDRESS_REF = "verticleAddress";
    public static final String PORT_REF = "verticlePort";
    public static final String VERBOSE_REF = "verticleVerbose";

    private ServiceAddress registryAddress;
    private RegistryApiClient registryApiClient;

    public BaseVerticle() {}

    protected String formatServerPath(final String serverPath, final String... args) {
        return String.format(serverPath, Arrays.stream(args).map((arg) -> ":" + arg).toArray());
    }

    protected void handleAnyRoute(RoutingContext rc) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String path = rc.request().path();
            stringBuilder.append("Requested path:\n" + path);
            if (!rc.getBodyAsString().isEmpty()) {
                JsonObject body = rc.getBodyAsJson();
                stringBuilder.append("\nWith body:\n" + body.encodePrettily());
            }
            println(stringBuilder.toString());
            rc.next();
        } catch (Exception e) {
            rc.fail(500, e);
        }
    }

    private void handlerFailure(RoutingContext rc) {
        if (rc.failed() && rc.failure() != null) {
            SocketAddress addr = rc.request().remoteAddress();

            String method = rc.request().rawMethod();
            System.err.println(String.format(Locale.getDefault(),
                "Error detected while trying to handle a request\r\n" +
                "Remote address: %s:%s %s\r\n" +
                "Method: %s\r\n" +
                "Path: %s (%s)\r\n" +
                "Body: %s\r\n" +
                "Error: %s\r\n",
                addr.host(), addr.port(), addr.path(),
                method,
                rc.request().path(),
                rc.request().absoluteURI(),
                rc.getBodyAsString(),
                rc.failure().getMessage()));
            rc.failure().printStackTrace(System.err);
        }
    }

    protected void println(final String message) {
        System.out.println(String.format("[%s]: %s", NAME, message));
    }

    protected Router initRouter(String apiRoot) {
        return initRouter(apiRoot, false);
    }

    protected Router initRouter(String apiRoot, boolean verbose) {
        return initRouter(apiRoot, true, verbose);
    }

    protected Router initRouter(String apiRoot, boolean corsEnable, boolean verbose) {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());

        if (corsEnable) {
            router.route().handler(getCorsHandler());
        }

        router.route(apiRoot + "/*").handler(rc -> {
            rc.response().putHeader(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_MIME);
            rc.next();
        });
        router.get(HeartBeatRoute.API_HEARTBEAT).handler(rc -> rc.response().end());
        router.route().failureHandler(this::handlerFailure);
        if (verbose) {
            router.route().handler(this::handleAnyRoute);
        }
        return router;
    }

    private CorsHandler getCorsHandler() {
        Set<String> allowedHeaders = new HashSet<>();

        allowedHeaders.add("x-requested-with");
        allowedHeaders.add("Access-Control-Allow-Origin");
        allowedHeaders.add("origin");
        allowedHeaders.add("Content-Type");
        allowedHeaders.add("accept");
        allowedHeaders.add("X-PINGARUNER");

        Set<HttpMethod> allowedMethods = new HashSet<>();
        allowedMethods.add(HttpMethod.GET);
        allowedMethods.add(HttpMethod.POST);
        allowedMethods.add(HttpMethod.OPTIONS);
        allowedMethods.add(HttpMethod.DELETE);
        allowedMethods.add(HttpMethod.PATCH);
        allowedMethods.add(HttpMethod.PUT);

        // TODO: for security reasons .* should be replaced with specific url
        CorsHandler corsHandler = CorsHandler.create(".*");
        corsHandler.allowedHeaders(allowedHeaders);
        corsHandler.allowedMethods(allowedMethods);
        return corsHandler;
    }

    protected ServiceAddress getRegistryAddress() {
        if (registryAddress == null) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream(REGISTRY_FILE_CONFIG), StandardCharsets.UTF_8))) {

                String config = reader.lines().reduce(String::concat).orElse("");
                registryAddress = new JsonObject(config).mapTo(ServiceAddress.class);

            } catch (Exception e) {
                println(e.getMessage());
                e.printStackTrace();
            }
        }
        return registryAddress;
    }

    protected RegistryApiClient getRegistryApiClient() {
        if (registryApiClient == null) {
            registryApiClient = new RegistryApiClientImpl(getRegistryAddress(), vertx);
        }
        return registryApiClient;
    }

    protected JsonObject getVerticleConfig() {
        JsonObject jsonObject = new JsonObject();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream(VERTICLE_CONFIG), StandardCharsets.UTF_8))) {

                String config = reader.lines().reduce(String::concat).orElse("");
                jsonObject = new JsonObject(config);

            } catch (Exception e) {
                println(e.getMessage());
                e.printStackTrace();
            }

        return jsonObject;
    }

    protected String generateRandomUUID() {
        return UUID.randomUUID().toString().replace("-", "_");
    }

    protected <T> List<T> convertJsonArray(JsonArray jsonArray, Class<T> type) {
        return jsonArray.stream().map(obj -> JsonObject.mapFrom(obj).mapTo(type)).collect(Collectors.toList());
    }
}
