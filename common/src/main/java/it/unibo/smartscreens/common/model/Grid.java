package it.unibo.smartscreens.common.model;

import java.util.Objects;

/**
 * Grid POJO
 */
public class Grid {

    private int rows;
    private int columns;

    public Grid() {}

    public Grid(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Grid grid = (Grid) o;
        return getRows() == grid.getRows() &&
            getColumns() == grid.getColumns();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRows(), getColumns());
    }
}
