import json
import requests
import time

url = 'http://127.0.0.1:8082/api'


def get_state():
    print("getting current state")
    r = requests.get(url + '/state')
    print(r.status_code)
    print(r.json())


get_state()

# add a new component
print("adding a new component with width 2")
component = {
    'componentId': 'my-component-id2',
    'positions': [5, 6],
    'sourceType': "heartbeat",
    'patientId': "some-id"
}
r = requests.post(url + '/components', data=json.dumps(component))
print(r.status_code)

componentId = r.json()['componentId']

# time.sleep(3)

component['positions'] = [4, 5, 6]
print("changing occupied positions to " + str(component['positions']))
r = requests.put(url + '/components/' + componentId, data=json.dumps(component))
print(r.status_code)

time.sleep(3)

component['positions'] = [4, 5]
print("changing occupied positions to " + str(component['positions']))
r = requests.put(url + '/components/' + componentId, data=json.dumps(component))
print(r.status_code)

time.sleep(3)

component['positions'] = [1, 2, 4, 5]
print("changing occupied positions to " + str(component['positions']))
r = requests.put(url + '/components/' + componentId, data=json.dumps(component))
print(r.status_code)

time.sleep(3)

component['positions'] = [2]
print("changing occupied positions to " + str(component['positions']))
r = requests.put(url + '/components/' + componentId, data=json.dumps(component))
print(r.status_code)

time.sleep(3)

"""
print('updating grid')
grid = {
    "rows": 2,
    "columns": 2
}
r = requests.put(url + '/change-grid', data=json.dumps(grid))
print(r.status_code)
"""

# delete component
print("deleting component")
r = requests.delete(url + '/components/' + componentId)
print(r.status_code)

get_state()
