package it.unibo.smartscreens.smartscreen.api;

public enum UpdateDataResponse {

    INCOMPATIBLE_BODY("incompatible body", 400),
    COMPONENT_NOT_FOUND("component not found", 404),
    OK("ok", 200);

    private final String statusMessage;
    private final int statusCode;

    UpdateDataResponse(String statusMessage, int statusCode) {
        this.statusMessage = statusMessage;
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
