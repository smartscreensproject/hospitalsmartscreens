package it.unibo.smartscreens.smartscreen.api;

public enum ScreenSocketMessageType {

    SCREEN_STATE (0),
    ADD_COMPONENT (1),
    REMOVE_COMPONENT (2),
    CLEAR (3),
    UPDATE_DATA (4),
    MODIFY_VIEWER (5),
    RESIZE(6),
    MOVE(7),
    CHANGE_GRID(8);

    private final int type;

    ScreenSocketMessageType(int type) {
        this.type = type;
    }

    public int type() {
        return type;
    }
}
