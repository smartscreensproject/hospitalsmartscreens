package it.unibo.smartscreens.smartscreen.launcher;

import io.vertx.core.Vertx;
import it.unibo.smartscreens.smartscreen.verticle.SmartScreenVerticle;

public class SmartScreenLauncher {

    public static void main(String[] args) {

        Vertx vertx = Vertx.vertx();

        // Example of a verticle from the same module
        SmartScreenVerticle smartScreenVerticle = new SmartScreenVerticle(8082, "room1", "2", 3, 3);
        vertx.deployVerticle(smartScreenVerticle, (handler) -> {
            if (handler.succeeded()) {
                System.out.println("The verticle has been successfully deployed, with id: " + handler.result());
/*
                SmartScreenApiClient smartScreenApiClient = new SmartScreenApiClientImpl(new ServiceAddress("localhost", 8082), smartScreenVerticle.getVertx());
                Component c = new Component("aa", Arrays.asList(5, 6, 8, 9), "hh", "ll");
                smartScreenApiClient.addComponent(c).setHandler( res -> {
                    if (res.succeeded()) {
                        System.out.println("ok");
                        smartScreenApiClient.changeGrid(new Grid(4,4)).setHandler(res1 -> {
                            if (res1.succeeded()) {
                                System.out.println(Json.encodePrettily(res1.result()));
                            } else {
                                System.out.println(res.cause().getMessage());
                            }
                        });
                    } else {
                        System.out.println(res.cause().getMessage());
                    }
                });
*/
            } else {
                System.out.println("Error during deployment: " + handler.cause().getMessage());
            }
        });
    }
}
