package it.unibo.smartscreens.smartscreen.verticle;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import io.vertx.ext.web.handler.sockjs.SockJSSocket;
import it.unibo.smartscreens.smartscreen.api.ScreenSocketMessageType;
import it.unibo.smartscreens.smartscreen.api.UpdateDataResponse;
import it.unibo.smartscreens.common.enums.SourceSocketMessageType;
import it.unibo.smartscreens.common.model.*;
import it.unibo.smartscreens.common.verticles.BaseVerticle;
import it.unibo.smartscreens.common.msroute.SmartScreenRoute;
import it.unibo.smartscreens.common.utils.SockJSWrapper;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

//TODO check on sender of HTTP request
public class SmartScreenVerticle extends BaseVerticle {

    private final static ObjectMapper objectMapper = new ObjectMapper();
    private final int port; //TODO remove
    private final List<SockJSSocket> smartScreenSocket = new LinkedList<>();
    private final SmartScreen smartScreen;

    public SmartScreenVerticle(int port, String room, String deviceIdInRoom, int screenRaws, int screenColumns) {
        this(port, room, deviceIdInRoom, new Grid(screenRaws, screenColumns));
    }

    public SmartScreenVerticle(int port, String room, String deviceIdInRoom, Grid grid) {
        this.port = port;
        this.smartScreen = new SmartScreen(room, deviceIdInRoom, grid);
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        Router router = initRouter(SmartScreenRoute.API_ROOT);

        router.get(SmartScreenRoute.API_STATE).handler(this::handleGetState);
        router.get(SmartScreenRoute.API_PROFILE).handler(this::handleGetProfile);
        router.put(SmartScreenRoute.API_CHANGE_GRID).handler(this::handleChangeGrid);

        router.post(SmartScreenRoute.API_COMPONENTS).handler(this::handlePostComponents);

        router.put(SmartScreenRoute.API_COMPONENTS +"/:componentsId").handler(this::handlePutComponent);
        router.delete(SmartScreenRoute.API_COMPONENTS +"/:componentsId").handler(this::handleDeleteComponents);

        router.delete(SmartScreenRoute.API_CLEAR).handler(this::handleClear);

        router.post(SmartScreenRoute.API_UPDATE_DATA).handler(this::handleUpdateData);

        defineScreenSockJS(router);

        defineSourceSockJS(router);

        vertx.createHttpServer()
            .requestHandler(router)
            .listen(port, (response) -> {
                if (response.succeeded()) {
                    println("Verticle deployed successfully, try typing localhost:" + response.result().actualPort());
                    subscribeToRegister(response.result().actualPort()).setHandler(subscription -> {
                        if (subscription.succeeded()) {
                            startFuture.complete();
                        } else {
                            startFuture.fail(subscription.cause());
                        }
                    });
                } else {
                    println("Error in http server: " + response.cause().getMessage());
                    startFuture.fail(response.cause());
                }
            });
    }

    private Future<Void> subscribeToRegister(int port) {
        Future<Void> future = Future.future();
        getRegistryApiClient().postSmartScreens(port, smartScreen.getRoom(), smartScreen.getDeviceIdInRoom())
            .setHandler(res -> {
                if (res.succeeded()) {
                    smartScreen.setId(res.result().getId());
                    println(JsonObject.mapFrom(res.result()).encodePrettily());
                    future.complete();
                } else {
                    println(res.cause().getMessage());
                    future.fail(res.cause());
                }
            });
        return future;
    }

    private void defineSourceSockJS(Router router) {
        SockJSHandlerOptions options = new SockJSHandlerOptions().setHeartbeatInterval(2000);

        SockJSWrapper wrap = new SockJSWrapper();
        wrap.setOnOpen(s -> s.write("connected"))
            .setOnMessage(this::onSourceMessage);
        router.route(SmartScreenRoute.API_SOURCE_SOCKET).handler(wrap.build(vertx, options));
    }

    private void onSourceMessage(SockJSSocket sockJSSocket, Buffer message) {
        JsonObject wsMsg = new JsonObject(message);
        println("vs message: " + wsMsg.encodePrettily());
        SourceSocketMsg sourceMessage = wsMsg.mapTo(SourceSocketMsg.class);
        if (sourceMessage == null) {
            return;
        }

        if (sourceMessage.getType() == SourceSocketMessageType.UPDATE_DATA) {
            UpdateDataResponse response = updateData(sourceMessage.getMsg());
            sockJSSocket.write("" + response.getStatusCode());
        }
    }

    /**
     *  socket with physical screen
     */
    private void defineScreenSockJS(Router router) {
        SockJSHandlerOptions options = new SockJSHandlerOptions().setHeartbeatInterval(2000);
        SockJSWrapper wrap = new SockJSWrapper();
        wrap.setOnOpen(this::onOpen)
            .setOnClose(this::onClose)
            .setOnMessage(this::onMessage);
        router.route(SmartScreenRoute.API_MONITOR_SOCKET).handler(wrap.build(vertx, options));
    }

    private void onOpen(SockJSSocket sockJSSocket) {
        println("connect: " + sockJSSocket.remoteAddress().host()+":"+sockJSSocket.remoteAddress().port());
        this.smartScreenSocket.add(sockJSSocket);
        JsonObject firstMessage = new JsonObject();
        firstMessage.put("type", ScreenSocketMessageType.SCREEN_STATE.type());
        firstMessage.put("screenState", JsonObject.mapFrom(smartScreen));
        sockJSSocket.write(firstMessage.encode());
    }

    private void onMessage(SockJSSocket sockJSSocket, Buffer message) {
        println(sockJSSocket.remoteAddress().host()+":"+sockJSSocket.remoteAddress().port() + " say: " + message.toString());
    }

    private void onClose(SockJSSocket sockJSSocket) {
        this.smartScreenSocket.remove(sockJSSocket);
        println("closed from client");
    }

    private void sendBroadcast(String msg) {
        this.smartScreenSocket.forEach(s -> s.write(msg));
    }

    private void sendBroadcast(ScreenSocketMessageType type, JsonObject msg) {
        JsonObject json = new JsonObject();
        json.put("type", type.type());
        json.put("message", msg);
        sendBroadcast(json.encode());
    }

    /*
     * handle request
     */
    private void handleGetState(RoutingContext rc) {
        rc.response().end(JsonObject.mapFrom(smartScreen).encode());
    }

    private void handleGetProfile(RoutingContext rc) {
        rc.response().end(Json.encode(this.smartScreen.getProfile()));
    }

    private void handleChangeGrid(RoutingContext rc) {
        //read and check validity of body
        JsonObject body = rc.getBodyAsJson();
        Grid newGrid = body.mapTo(Grid.class);
        if (newGrid == null) {
            rc.response().setStatusCode(400).end();
            return;
        }
        this.smartScreen.changeGrid(newGrid);
        sendBroadcast(ScreenSocketMessageType.CHANGE_GRID, JsonObject.mapFrom(smartScreen));
        rc.response().end(Json.encode(this.smartScreen.getProfile()));
    }

    private void handlePostComponents(RoutingContext rc) {
        println("POST components");

        Component component = rc.getBodyAsJson().mapTo(Component.class);
        if (component == null) {
            rc.response().setStatusCode(400).setStatusMessage("Incompatible component").end();
            return;
        }
        component.initId();
        if (!component.isAllInitialized()) {
            rc.response().setStatusCode(400).setStatusMessage("Component not initialized").end();
            return;
        }

        //check component not already present
        if (smartScreen.getAreasAsStream()
            .anyMatch(sa -> sa.getComponent().getComponentId().equals(component.getComponentId()))) {
            rc.response().setStatusCode(400).setStatusMessage("Component already present").end();
            return;
        }
        Optional<ScreenArea> newScreenArea = this.smartScreen.addArea(component);
        if (!newScreenArea.isPresent()) {
            rc.response()
                .setStatusCode(400)
                .setStatusMessage("Group of cell not valid, it isn't a rectangle or same cell is already busy")
                .end();
        } else {
            JsonObject responseJson = JsonObject.mapFrom(newScreenArea.get());
            sendBroadcast(ScreenSocketMessageType.ADD_COMPONENT, responseJson);
            //response
            println(responseJson.encodePrettily());
            rc.response().end(Json.encode(newScreenArea.get().getComponent()));
        }
    }

    private void handlePutComponent(RoutingContext rc) {
        String componentsId = rc.request().getParam("componentsId");
        Component component = rc.getBodyAsJson()
            .put("componentId", componentsId)
            .mapTo(Component.class);
        if (component == null || !component.isAllInitialized()) {
            rc.response().setStatusCode(400).setStatusMessage("Incompatible component").end();
            return;
        }
        Optional<ScreenArea> screenAreaOpt = smartScreen.getAreasAsStream()
            .filter(sa -> sa.getComponent().getComponentId().equals(component.getComponentId()))
            .filter(sa -> sa.getComponent().getPatientId().equals(component.getPatientId()))
            .filter(sa -> sa.getComponent().getSourceType().equals(component.getSourceType()))
            .findAny();
        //check component already present
        if (!screenAreaOpt.isPresent()) {
            rc.response()
                .setStatusCode(404)
                .setStatusMessage("Component not found with the specified id, patientId and source type")
                .end();
            return;
        }
        ScreenArea screenArea = screenAreaOpt.get();
        ScreenSocketMessageType type;
        if (screenArea.getComponent().getPositionsAsStream()
            .anyMatch(pos -> component.getPositions().contains(pos))) {
            type = ScreenSocketMessageType.RESIZE;
        } else {
            type = ScreenSocketMessageType.MOVE;
        }
        List<Integer> oldPosition = screenArea.getComponent().getPositions();
        if (smartScreen.updateComponent(component)) {
            List<ScreenArea> newAreas = this.smartScreen.getFreeAreasAsStream()
                .filter(sa -> oldPosition.containsAll(sa.getComponent().getPositions()))
                .collect(Collectors.toList());
            JsonObject msg = new JsonObject()
                .put("screenAreaUpdated", JsonObject.mapFrom(screenArea))
                .put("newScreenAreas", newAreas);
            sendBroadcast(type, msg);

            UpdateComponentResponse updateComponentResponse = new UpdateComponentResponse()
                .setComponentUpdated(screenArea.getComponent())
                .setNewComponents(newAreas.stream().map(ScreenArea::getComponent).collect(Collectors.toList()));
            rc.response().end(Json.encode(updateComponentResponse));
        } else {
            rc.response()
                .setStatusCode(400)
                .setStatusMessage("Group of cell not valid, it isn't a rectangle or same cell is already busy")
                .end();
        }


    }

    private void handleDeleteComponents(RoutingContext rc) {
        String componentsId = rc.request().getParam("componentsId");
        println("DELETE components " + componentsId);
        if (componentsId == null) {
            rc.response().setStatusCode(400).setStatusMessage("id component missing").end();
        }
        Optional<ScreenArea> removedArea = this.smartScreen.cleanArea(componentsId);
        if (removedArea.isPresent()) {
            List<ScreenArea> newAreas = this.smartScreen.getFreeAreasAsStream()
                .filter(sa -> removedArea.get().getComponent().getPositions().containsAll(sa.getComponent().getPositions()))
                .collect(Collectors.toList());
            JsonObject msg = new JsonObject()
                .put("componentId", componentsId)
                .put("newAreas", newAreas);
            sendBroadcast(ScreenSocketMessageType.REMOVE_COMPONENT, msg);
            List<Component> newComponent = newAreas.stream().map(ScreenArea::getComponent).collect(Collectors.toList());
            rc.response().end(Json.encode(newComponent));
        } else {
            rc.response().setStatusCode(404).setStatusMessage("component not found").end();
        }
    }

    private void handleUpdateData(RoutingContext rc) {
        //read and check validity of body
        JsonObject body = rc.getBodyAsJson();
        UpdateDataResponse response = updateData(body);
        //response
        rc.response()
            .setStatusCode(response.getStatusCode())
            .setStatusMessage(response.getStatusMessage())
            .end();
    }

    private void handleClear(RoutingContext rc) {
        this.smartScreen.clear();
        JsonObject msg = new JsonObject()
            .put("newAreas", this.smartScreen.getAreas());
        sendBroadcast(ScreenSocketMessageType.CLEAR, msg);
        rc.response().end(Json.encode(this.smartScreen.getComponents()));
    }

    private UpdateDataResponse updateData(JsonObject body) {
        String componentId = body.getString("componentId");
        String viewer = body.getString("viewer");
        JsonNode data;
        try {
            data = objectMapper.readTree(body.getJsonObject("data").encode());
        } catch (IOException e) {
            e.printStackTrace();
            return UpdateDataResponse.INCOMPATIBLE_BODY;
        }
        if (componentId == null || viewer == null || data == null) {
            return UpdateDataResponse.INCOMPATIBLE_BODY;
        }
        Optional<ScreenArea> screenAreaOpt = this.smartScreen.getAreasAsStream()
            .filter(sa -> sa.getComponent().getComponentId().equals(componentId))
            .findFirst();
        if (!screenAreaOpt.isPresent()) {
            return UpdateDataResponse.COMPONENT_NOT_FOUND;
        }
        ScreenArea screenArea = screenAreaOpt.get();
        ScreenSocketMessageType typeMsg;
        //update data
        screenArea.setData(data);
        //check if viewer changed
        if (screenArea.getViewer().equals(viewer)) {
            typeMsg = ScreenSocketMessageType.UPDATE_DATA;
        } else {
            screenArea.setViewer(viewer);
            typeMsg = ScreenSocketMessageType.MODIFY_VIEWER;
        }
        //send broadcast
        sendBroadcast(typeMsg, body);
        return UpdateDataResponse.OK;
    }



    @Override
    public void stop(Future<Void> stopFuture) throws Exception {
        println("Verticle stopped successfully");
        this.smartScreenSocket.forEach(SockJSSocket::close);
        stopFuture.complete();
    }
}
