package it.unibo.smartscreens.tac.model;

import io.vertx.core.json.JsonObject;
import it.unibo.smartscreens.common.model.Action;
import it.unibo.smartscreens.common.model.tac.Tac;

public class TacInfoWrapper {

    private String id;
    private int internalNumber;
    private String date;
    private String name;
    private Action selectAction;

    TacInfoWrapper() {}

    public TacInfoWrapper(String id, int internalNumber, String date, String name, Action selectAction) {
        this.id = id;
        this.internalNumber = internalNumber;
        this.date = date;
        this.name = name;
        this.selectAction = selectAction;
    }

    public static TacInfoWrapper create(Tac tac, Action action) {
        return new TacInfoWrapper(tac.getId(), 0, tac.getDate(), tac.getName(), action);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getInternalNumber() {
        return internalNumber;
    }

    public void setInternalNumber(int internalNumber) {
        this.internalNumber = internalNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Action getSelectAction() {
        return selectAction;
    }

    public void setSelectAction(Action selectAction) {
        this.selectAction = selectAction;
    }

    public JsonObject getTacInfoAsJson() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("id", id);
        jsonObject.put("internalNumber", internalNumber);
        jsonObject.put("date", date);
        jsonObject.put("name", name);
        return jsonObject;
    }

    public JsonObject getTacActionAsJson() {
        return JsonObject.mapFrom(selectAction);
    }
}
