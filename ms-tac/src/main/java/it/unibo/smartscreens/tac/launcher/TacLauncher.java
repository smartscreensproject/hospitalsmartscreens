package it.unibo.smartscreens.tac.launcher;

import it.unibo.smartscreens.tac.base.TacVerticle;
import it.unibo.smartscreens.common.verticles.BaseVerticle;
import io.vertx.core.Vertx;

public class TacLauncher {

    public static void main(String[] args) {
        //Launching the verticle

        final BaseVerticle verticle = new TacVerticle();
        Vertx.vertx().deployVerticle(verticle, (deployHandler) -> {
            if (deployHandler.succeeded()) {
                System.out.println("The verticle has been successfully deployed, with id: " + deployHandler.result());
            } else {
                System.out.println(deployHandler.cause().getMessage());
                System.exit(0);
            }
        });
    }
}
