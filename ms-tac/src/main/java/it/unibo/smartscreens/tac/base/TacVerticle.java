package it.unibo.smartscreens.tac.base;

import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import it.unibo.smartscreens.tac.model.TacInfoWrapper;
import it.unibo.smartscreens.common.apiclient.impl.PatientIdResolverApiClientImpl;
import it.unibo.smartscreens.common.apiclient.impl.SmartScreenApiClientImpl;
import it.unibo.smartscreens.common.apiclient.interfaces.PatientIdResolverApiClient;
import it.unibo.smartscreens.common.apiclient.interfaces.SmartScreenApiClient;
import it.unibo.smartscreens.common.constant.NameService;
import it.unibo.smartscreens.common.model.*;
import it.unibo.smartscreens.common.model.patient.Patient;
import it.unibo.smartscreens.common.model.registry.MicroServiceEntity;
import it.unibo.smartscreens.common.model.tac.Tac;
import it.unibo.smartscreens.common.model.tac.TacImage;
import it.unibo.smartscreens.common.msroute.TacRoute;
import it.unibo.smartscreens.common.verticles.BaseVerticle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TacVerticle extends BaseVerticle {

    private enum ViewerType {
        TAC_SELECTION("tac-selection"),
        TAC_DETAIL("tac-detail");

        private final String viewerType;

        ViewerType(final String viewerType) {
            this.viewerType = viewerType;
        }

        public String getViewerType() {
            return viewerType;
        }
    }

    private static final String MONGODB_CONFIG = "/mongodbTac.json";
    private static final String TACS_COLLECTION = "tacs";

    private ServiceAddress serviceAddress;

    private String inputManagerId;
    private String componentId;
    private String patientId;
    private Patient patient;

    private MongoClient mongoClient;

    private boolean isInitialised;

    private ServiceAddress smartScreenServiceAddress;
    private SmartScreenApiClient smartScreenClient;

    public TacVerticle() { }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        println("TacVerticle started");

        //Retrieve config to set the verticle up
        JsonObject config = config();
        if (config == null || !config.containsKey(ADDRESS_REF) || !config.containsKey(PORT_REF)) {
            startFuture.fail("Failed to configure TacVerticle");
            return;
        }

        String address = config.getString(ADDRESS_REF);
        int port = config.getInteger(PORT_REF);
        serviceAddress = new ServiceAddress(address, port);
        boolean isRouterVerbose = config.getBoolean(VERBOSE_REF, false);

        // Init MongoDB client
        try (BufferedReader mongoConfigInputStream = new BufferedReader(new InputStreamReader(
            this.getClass().getResourceAsStream(MONGODB_CONFIG), StandardCharsets.UTF_8))) {

            JsonObject mongoConfig = new JsonObject(mongoConfigInputStream.lines().reduce(String::concat).orElse(""));
            mongoClient = MongoClient.createShared(vertx, mongoConfig);
        } catch (Exception e) {
            startFuture.fail(e.getCause());
            return;
        }

        // Creating the router
        Router router = createRouter(isRouterVerbose);

        // Creating the server, using the router
        vertx.createHttpServer(
            new HttpServerOptions()
                .setLogActivity(true)
        )
            .requestHandler(router)
            .listen(port, address, listenResponse -> {
                if (listenResponse.succeeded()) {
                    println("Verticle deployed successfully, try typing localhost: " + listenResponse.result().actualPort());

                    // Register to the registry
                    getRegistryApiClient().postServices(serviceAddress, NameService.TAC, "Patients TAC microservice").setHandler(res -> {
                        if (res.succeeded()) {
                            println("TacVerticle registered to registry");
                            startFuture.complete();
                        } else {
                            println("TacVerticle failed to register to registry");
                            startFuture.fail(res.cause());
                        }
                    });
                } else {
                    startFuture.fail(listenResponse.cause());
                }
            });
    }

    @Override
    public void stop(Future<Void> stopFuture) throws Exception {
        println("TacVerticle stopped");

        super.stop(stopFuture);
    }

    private Router createRouter(boolean verbose) {
        // Creating the router
        Router router = initRouter(TacRoute.API_ROOT, verbose);
        // Tac routes
        router.post(TacRoute.API_TAC_INIT).handler(this::handleInit);
        router.get(TacRoute.API_TAC_ACTIONS).handler(this::handleRetrieveActions);
        router.post(TacRoute.API_TAC_DESTROY).handler(this::handleDestroy);
        router.post(TacRoute.API_TAC_PATIENT_SEARCH).handler(this::handlePatientSearch);
        router.get(formatServerPath(TacRoute.API_TAC_PATIENT_TAC, "patientName", "tacId")).handler(this::handleRetrievePatientTac);
        router.get(formatServerPath(TacRoute.API_TAC_PATIENT_TAC_IMAGE, "patientName", "tacId", "imageId")).handler(this::handleRetrievePatientTacImage);

        return router;
    }

    private void handleInit(RoutingContext rc) {
        @Nullable JsonObject body = rc.getBodyAsJson();
        if (body == null) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        try {
            // Retrieve the init data
            ComponentInitParams componentInitParams = body.mapTo(ComponentInitParams.class);
            inputManagerId = componentInitParams.getInputManagerId();
            componentId = componentInitParams.getComponentId();
            patientId = componentInitParams.getPatientId();

            if (inputManagerId.isEmpty() || componentId.isEmpty() || patientId.isEmpty()) {
                rc.response().setStatusCode(400).setStatusMessage("Fields patientId, inputManagerId and componentId cannot be empty").end();
                return;
            }

            ServiceRoute updateDataRoute = body.getJsonObject("updateDataEndpoint").mapTo(ServiceRoute.class);
            if (updateDataRoute == null) {
                rc.response().setStatusCode(400).setStatusMessage("Incorrect updateDataEndpoint service route: the microservice cannot send its data").end();
                return;
            }

            // Create the smartScreen client
            smartScreenServiceAddress = new ServiceAddress(updateDataRoute.getAddress(), updateDataRoute.getPort());
            smartScreenClient = new SmartScreenApiClientImpl(smartScreenServiceAddress, vertx);

            // Retrieve the patient id resolver client
            getRegistryApiClient().getServices(Collections.singletonList(NameService.PATIENT_ID_RESOLVER)).setHandler((res) -> {
                if (res != null) {
                    Optional<MicroServiceEntity> optionalMicroServiceEntity = res.result().stream().findFirst();
                    if (optionalMicroServiceEntity.isPresent()) {
                        // We retrieve the microservice and create the client to retrieve the patient info
                        ServiceAddress serviceAddress = optionalMicroServiceEntity.get().getServiceAddress();
                        PatientIdResolverApiClient patientIdResolverClient = new PatientIdResolverApiClientImpl(serviceAddress, vertx);
                        patientIdResolverClient.getPatientById(patientId).setHandler(patientRes -> {
                            if (patientRes.succeeded() && patientRes.result() != null) {
                                // Retrieve the patient
                                patient = patientRes.result();

                                // Set the init flag
                                this.isInitialised = true;

                                // Retrieve the generic actions and respond to the client
                                List<Action> actions = createSearchPatientActions();
                                //rc.response().end(Json.encode(actions));

                                // Retrieve the patient name and ps
                                String tacNameRef = patient.getCodes().get("name");
                                String tacPSRef = patient.getCodes().get("ps");

                                // Try to retrieve some tacs based on what it's known
                                findAllPatientTacs(tacNameRef, tacPSRef).setHandler(findAllTacsRes -> {
                                    List<Tac> foundTacs = new ArrayList<>();
                                    if (findAllTacsRes.succeeded()) {
                                        foundTacs = findAllTacsRes.result();
                                        println(String.format("Found %s tacs for the requested patient", foundTacs.size()));
                                    } else {
                                        println("Found no tacs for the requested patient, showing base actions");
                                    }
                                    manageAndSendTacDetailData(rc, foundTacs);
                                });

                            } else {
                                rc.response().setStatusCode(500).setStatusMessage(String.format("TacVerticle failed to retrieve the patient info using the provided patientId [%s]", patientId)).end();
                            }
                        });
                    } else {
                        rc.response().setStatusCode(500).setStatusMessage("The requested PatientIdResolver instance is unavailable").end();
                    }
                } else {
                    rc.response().setStatusCode(500).setStatusMessage("TacVerticle failed to retrieve the PatientIdResolver microservice").end();
                }
            });

        } catch (Exception e) {
            println(e.getMessage());
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
        }
    }

    private void handleDestroy(RoutingContext rc) {
        vertx.undeploy(deploymentID(), res -> {
            if (res.succeeded()) {
                println("TacVerticle closed successfully");
            } else {
                println(res.cause().getMessage());
            }
        });
    }

    private List<TacInfoWrapper> retrieveTacsWrappers(final List<Tac> patientTacs) {
        List<TacInfoWrapper> tacsInfoWrappers = new ArrayList<>();
        tacsInfoWrappers = IntStream.range(0, patientTacs.size())
            .boxed()
            .map(i -> {
                Tac tac = patientTacs.get(i);
                Action action = createRetrieveTacAction(tac);
                action.setName("Select tac " + (i + 1));
                TacInfoWrapper tacInfoWrapper = TacInfoWrapper.create(tac, action);
                tacInfoWrapper.setInternalNumber(i + 1);
                return tacInfoWrapper;
            }).collect(Collectors.toList());
        return tacsInfoWrappers;
    }

    private void handleRetrieveActions(RoutingContext rc) {
        if (!isInitialised) {
            rc.response().setStatusCode(400).setStatusMessage(createNotInitialisedThrowable().getMessage()).end();
            return;
        }

        // Retrieve the generic actions
        List<Action> actions = createSearchPatientActions();
        rc.response().end(Json.encode(actions));
    }

    private void handlePatientSearch(RoutingContext rc) {
        if (!isInitialised) {
            rc.response().setStatusCode(400).setStatusMessage(createNotInitialisedThrowable().getMessage()).end();
            return;
        }
        @Nullable JsonObject body = rc.getBodyAsJson();
        if (body == null) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        if (!body.containsKey("name") && !body.containsKey("ps")) {
            rc.response().setStatusCode(400).setStatusMessage("No name or ps code provided").end();
            return;
        }
        String name = body.getString("name", "");
        String ps = body.getString("ps", "");
        if (name.isEmpty() && ps.isEmpty()) {
            // The search fields are empty
            rc.response().setStatusCode(400).setStatusMessage("Name and ps code provided are empty").end();
            return;
        }

        final String patientTacName = !name.isEmpty() ? name : ps;

        // Retrieve patient tacs
        findPatientTacs(patientTacName).setHandler(res -> {
            if (res.succeeded()) {
                // The db request succeeded
                List<Tac> patientTacs = res.result();
                manageAndSendTacDetailData(rc, patientTacs);
            } else {
                // The db request failed
                println(res.cause().getMessage());
                rc.response().setStatusCode(400).setStatusMessage("Could not retrieve the patient TACs").end();
            }
        });
    }

    private void handleRetrievePatientTac(RoutingContext rc) {
        if (!isInitialised) {
            rc.response().setStatusCode(400).setStatusMessage(createNotInitialisedThrowable().getMessage()).end();
            return;
        }
        String patientName = rc.pathParam("patientName");
        String tacId = rc.pathParam("tacId");
        if (patientName.isEmpty() || tacId.isEmpty()) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }

        findPatientTac(patientName, tacId).setHandler(res -> {
            if (res.succeeded()) {
                // The db request succeeded
                Tac tac = res.result();
                if (tac == null) {
                    // The tac does not exist
                    rc.response().setStatusCode(404).setStatusMessage("No tac found").end();
                    return;
                }

                // Build the tac response, showing only the first image
                JsonObject tacData = buildTacData(tac, 0);
                manageAndSendTacData(rc, tacData);
            } else {
                // The db request failed
                println(res.cause().getMessage());
                rc.response().setStatusCode(400).setStatusMessage("Could not retrieve the requested patient TAC").end();
            }
        });
    }

    private void handleRetrievePatientTacImage(RoutingContext rc) {
        if (!isInitialised) {
            rc.response().setStatusCode(400).setStatusMessage(createNotInitialisedThrowable().getMessage()).end();
            return;
        }
        String patientName = rc.pathParam("patientName");
        String tacId = rc.pathParam("tacId");
        String imageId = rc.pathParam("imageId");
        if (patientName.isEmpty() || tacId.isEmpty() || imageId.isEmpty()) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }

        findPatientTac(patientName, tacId, imageId).setHandler(res -> {
            if (res.succeeded()) {
                // The db request succeeded
                Tac tac = res.result();
                if (tac == null) {
                    // The tac does not exist
                    rc.response().setStatusCode(404).setStatusMessage("No tac found").end();
                    return;
                }

                // Retrieve the requested patient image
                Optional<TacImage> patientTacImage = tac.getImages().stream()
                    .filter(tacImage -> tacImage.getId().equals(imageId))
                    .findFirst();
                if (!patientTacImage.isPresent()) {
                    // No tac image found
                    rc.response().setStatusCode(404).setStatusMessage("No tac image found").end();
                    return;
                }

                // Build the tac response, showing only the first image
                JsonObject tacData = buildTacData(tac, patientTacImage.get().getWeight());
                manageAndSendTacData(rc, tacData);
            } else {
                // The db request failed
                println(res.cause().getMessage());
                rc.response().setStatusCode(400).setStatusMessage("Could not retrieve the requested patient TAC with image").end();
            }
        });
    }

    private void manageAndSendTacData(RoutingContext rc, JsonObject tacData) {
        // Creating the actions: the search ones and the tac ones
        JsonArray actionsArray = new JsonArray();
        createSearchPatientActions().forEach(a -> actionsArray.add(JsonObject.mapFrom(a)));
        actionsArray.addAll(tacData.getJsonArray("actions"));

        // Send the data to the smart screen
        println("Sending data to SmartScreen:\n" + tacData.getJsonObject("data").encodePrettily());
        smartScreenClient.updateData(componentId, ViewerType.TAC_DETAIL.getViewerType(), tacData.getJsonObject("data")).setHandler(res -> {
            if (res.succeeded()) {
                // Encode the result actions
                rc.response().end(Json.encode(actionsArray));
            } else {
                println("Error while sending data to SmartScreen: " + res.cause());
                rc.response().setStatusCode(500).setStatusMessage("The Smart Screen failed to accept data").end();
            }
        });
    }

    private void manageAndSendTacDetailData (RoutingContext rc, List<Tac> patientTacs) {
        List<TacInfoWrapper> tacInfoWrappers = retrieveTacsWrappers(patientTacs);

        JsonArray tacInfoArray = new JsonArray();
        JsonArray tacActionsArray = new JsonArray();

        // Adding the base actions
        createSearchPatientActions().forEach(a -> tacActionsArray.add(JsonObject.mapFrom(a)));

        // Creating the data structures for both input and output
        tacInfoWrappers.forEach(tif -> {
            tacInfoArray.add(tif.getTacInfoAsJson());
            tacActionsArray.add(tif.getTacActionAsJson());
        });

        JsonObject data = new JsonObject();
        data.put("data", tacInfoArray);

        println("Sending data to SmartScreen:\n" + data.encodePrettily());
        if (rc != null) {
            // Send the tac details to the smart screen and inform the client
            smartScreenClient.updateData(componentId, ViewerType.TAC_SELECTION.getViewerType(), data).setHandler(response -> {
                if (response.succeeded()) {
                    // The smart screen has accepted our data, we send the actions to the input client
                    rc.response().end(Json.encode(tacActionsArray));
                } else {
                    // The smart screen has not accepted our data
                    println(response.cause().getMessage());
                    rc.response().setStatusCode(400).setStatusMessage("The Smart Screen failed to accept data").end();
                }
            });
        } else {
            // Send the tac details and don't inform the client
            smartScreenClient.updateData(componentId, ViewerType.TAC_SELECTION.getViewerType(), data);
        }
    }

    private Future<Tac> findPatientTac(final String patientName, final String tacId) {
        return findPatientTac(patientName, tacId, "");
    }

    private Future<Tac> findPatientTac(final String patientName, final String tacId, final String tacImageId) {
        JsonObject patientQuery = new JsonObject();
        if (!patientName.isEmpty()) {
            patientQuery.put("patient.name", patientName);
        }
        if (!tacImageId.isEmpty()) {
            patientQuery.put("images.id", tacImageId);
        }
        patientQuery.put("_id", tacId);
        return findPatientTac(patientQuery);
    }

    private Future<Tac> findPatientTac(final JsonObject query) {
        Future<Tac> future = Future.future();
        this.mongoClient.findOne(TACS_COLLECTION, query, null, res -> {
            if (res.succeeded()) {
                Tac tac = null;
                if (res.result() != null) {
                    // Found a tac
                    tac = res.result().mapTo(Tac.class);
                }
                future.complete(tac);
            } else {
                future.fail(res.cause());
            }
        });
        return future;
    }

    private Future<List<Tac>> findAllPatientTacs(final String nameCode, final String psCode) {
        if ((nameCode == null || nameCode.isEmpty()) &&
            (psCode == null || psCode.isEmpty())) {
            return Future.failedFuture("Patient identifiers were empty");
        }
        JsonArray orConditions = new JsonArray();
        if (nameCode != null && !nameCode.isEmpty()) {
            orConditions.add(new JsonObject().put("patient.name", nameCode));
        }
        if (psCode != null && !psCode.isEmpty()) {
            orConditions.add(new JsonObject().put("patient.name", psCode));
        }

        JsonObject patientQuery = new JsonObject();
        patientQuery.put("$or", orConditions);
        return findPatientTacs(patientQuery);
    }

    private Future<List<Tac>> findPatientTacs(final String patientCode) {
        if (patientCode == null || patientCode.isEmpty()) {
            return Future.failedFuture("Patient identifier was empty");
        }
        JsonObject patientQuery = new JsonObject();
        patientQuery.put("patient.name", patientCode);
        return findPatientTacs(patientQuery);
    }

    private Future<List<Tac>> findPatientTacs(final JsonObject query) {
        Future<List<Tac>> future = Future.future();

        // Ordering by date
        FindOptions findOptions = new FindOptions().setSort(
            new JsonObject().put("date", -1)
        );

        this.mongoClient.findWithOptions(TACS_COLLECTION, query, findOptions, res -> {
            if (res.succeeded()) {
                List<Tac> patientTacs = res.result().stream()
                    .map(pat -> pat.mapTo(Tac.class))
                    .collect(Collectors.toList());
                future.complete(patientTacs);
            } else {
                future.fail(res.cause());
            }
        });
        return future;
    }

    private List<Action> createSearchPatientActions() {
        List<Action> actions = new ArrayList<>();
        final String href = new StringBuilder()
            //.append(String.format("http://%s:%s%s", serviceAddress.getAddress(), serviceAddress.getPort())
            .append(TacRoute.API_TAC_PATIENT_SEARCH)
            .toString();

        String tacNameRef = patient.getCodes().get("name");
        String tacPSRef = patient.getCodes().get("ps");

        if (tacNameRef != null && !tacNameRef.isEmpty()) {
            Action actionName = new Action();
            actionName.setId(generateRandomUUID());
            actionName.setMethod("post");
            actionName.setName("Ricerca paziente per nome");
            actionName.setDescription("Ricerca paziente nella forma [nome]-[cognome]");
            actionName.setHref(href);
            actionName.setParams(Collections.singletonList(
                new ActionParam(
                    "name",
                    "",
                    "string",
                    tacNameRef,
                    false
                )
            ));
            actions.add(actionName);
        }

        if (tacPSRef != null && !tacPSRef.isEmpty()) {
            Action actionPs = new Action();
            actionPs.setId(generateRandomUUID());
            actionPs.setMethod("post");
            actionPs.setName("Ricerca paziente per PS");
            actionPs.setDescription("Ricerca paziente utilizzando il suo PS");
            actionPs.setHref(href);
            actionPs.setParams(Collections.singletonList(
                new ActionParam(
                    "ps",
                    "",
                    "string",
                    tacPSRef,
                    false
                )
            ));
            actions.add(actionPs);
        }
        return actions;
    }

    private Action createRetrieveTacAction(final Tac tac) {
        final String href = new StringBuilder()
            //.append(String.format("http://%s:%s", serviceAddress.getAddress(), serviceAddress.getPort()))
            .append(String.format(TacRoute.API_TAC_PATIENT_TAC, tac.getPatient().getName(), tac.getId()))
            .toString();

        Action action = new Action();
        action.setId(generateRandomUUID());
        action.setMethod("get");
        action.setHref(href);
        action.setName(tac.getName());
        action.setDescription(tac.getDate());
        action.setParams(Collections.emptyList());
        return action;
    }

    private Action createRetrieveTacImageAction(final String patientName, final String tacId, final String tacImageId) {
        final String href = new StringBuilder()
            //.append(String.format("http://%s:%s", serviceAddress.getAddress(), serviceAddress.getPort()))
            .append(String.format(TacRoute.API_TAC_PATIENT_TAC_IMAGE, patientName, tacId, tacImageId))
            .toString();

        Action action = new Action();
        action.setId(generateRandomUUID());
        action.setMethod("get");
        action.setHref(href);
        action.setParams(Collections.emptyList());
        return action;
    }

    private Optional<TacImage> getTacImageByPosition(Tac tac, int weight) {
        int size = tac.getImages().size();
        if (weight < 0 || weight >= size) {
            return Optional.empty();
        }
        return tac.getImages().stream().filter(tacImage -> tacImage.getWeight() == weight).findFirst();
    }

    private JsonObject buildTacData(Tac tac, int requestedImageWeight) {
        JsonObject tacJson = JsonObject.mapFrom(tac);
        Optional<TacImage> requestedTacImage = getTacImageByPosition(tac, requestedImageWeight);
        Optional<TacImage> prevTacImage = getTacImageByPosition(tac, requestedImageWeight - 1);
        Optional<TacImage> nextTacImage = getTacImageByPosition(tac, requestedImageWeight + 1);
        // Check the current image
        if (requestedTacImage.isPresent()) {
            // We found the current image
            tacJson.remove("images");
            tacJson.put("images", JsonObject.mapFrom(requestedTacImage.get()));
        } else {
            // We show no images at all
            tacJson.put("images", new JsonObject());
        }
        // Prepare the tac actions
        JsonArray actionArray = new JsonArray();
        // Check the next image
        if (nextTacImage.isPresent()) {
            Action nextImageAction = createRetrieveTacImageAction(tac.getPatient().getName(), tac.getId(), nextTacImage.get().getId());
            nextImageAction.setName("Prossima");
            nextImageAction.setDescription("Richiedi la prossima immagine");
            actionArray.add(JsonObject.mapFrom(nextImageAction));
        }
        // Check the prev image
        if (prevTacImage.isPresent()) {
            Action prevImageAction = createRetrieveTacImageAction(tac.getPatient().getName(), tac.getId(), prevTacImage.get().getId());
            prevImageAction.setName("Precedente");
            prevImageAction.setDescription("Richiedi l'immagine precedente");
            actionArray.add(JsonObject.mapFrom(prevImageAction));
        }
        // Build the resulting json
        JsonObject result = new JsonObject();
        result.put("data", tacJson);
        result.put("actions", actionArray);
        return result;
    }

    private Throwable createNotInitialisedThrowable() {
        return new Throwable("Tac microservice has not been initialised, call /init first");
    }
}
