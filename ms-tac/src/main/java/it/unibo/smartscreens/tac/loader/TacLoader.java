package it.unibo.smartscreens.tac.loader;


import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class TacLoader {

    private static final String MONGODB_CONFIG = "/mongodbTac.json";
    private static final String TACS_COLLECTION = "tacs";
    private static final String DEFAULT_TAC_FILE = "/tac.json";

    public static void main(String[] args) throws InterruptedException {

        String filePath = (args.length == 1) ? args[0] : DEFAULT_TAC_FILE;
        System.out.println("load tac from file: " + filePath);

        MongoClient mongoClient;
        JsonArray tacs;
        //load file with tac mock to insert in mongo db
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
            TacLoader.class.getResourceAsStream(filePath), StandardCharsets.UTF_8))) {

            String fileContent = reader.lines().reduce(String::concat).orElseThrow(IllegalStateException::new);
            tacs = new JsonArray(fileContent);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        //create mongo client
        try (BufferedReader mongoConfigInputStream = new BufferedReader(new InputStreamReader(
            TacLoader.class.getResourceAsStream(MONGODB_CONFIG), StandardCharsets.UTF_8))) {

            JsonObject mongoConfig = new JsonObject(mongoConfigInputStream.lines().reduce(String::concat).orElse(""));
            mongoClient = MongoClient.createShared(Vertx.vertx(), mongoConfig);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        System.out.println("remove previous tac");
        mongoClient.removeDocuments(TACS_COLLECTION, new JsonObject(), resRem -> {
            if (resRem.succeeded()) {
                System.out.println("Tacs removed \nInsert new tac...");
                //load tac in mongo
                for (int i = 0; i < tacs.size(); i++) {
                    mongoClient.insert(TACS_COLLECTION, tacs.getJsonObject(i), res -> {
                        if (res.succeeded()) {
                            System.out.println("tac with id: " + res.result() + " correctly loaded");
                        } else {
                            System.out.println("fail load tac with: " + res.cause().getMessage());
                        }
                    });
                }
            } else {
                System.out.println("error in remove tac with:" + resRem.cause().getMessage());
            }
        });

        Thread.sleep(5000);

        mongoClient.find(TACS_COLLECTION, new JsonObject(), res -> {
           if (res.succeeded()) {
               System.out.println("find " + res.result().size() + " tac after insert");
           } else {
               System.out.println("error in get tac: " + res.cause().getMessage());
           }
        });
    }
}
