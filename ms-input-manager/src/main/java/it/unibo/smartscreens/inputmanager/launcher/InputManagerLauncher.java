package it.unibo.smartscreens.inputmanager.launcher;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import it.unibo.smartscreens.common.model.InputManager;
import it.unibo.smartscreens.common.model.ServiceAddress;
import it.unibo.smartscreens.common.verticles.BaseVerticle;
import it.unibo.smartscreens.inputmanager.base.InputManagerVerticle;

public class InputManagerLauncher {

    public static void main(String[] args) {
        ServiceAddress serviceAddress = new ServiceAddress("localhost", 8080);

        // Launching the verticle, with default id, address and port
        final InputManager inputManager = new InputManager(
            "inputManagerId",
            "room1",
            "2",
            serviceAddress.getAddress(),
            serviceAddress.getPort()
        );

        final BaseVerticle verticle = new InputManagerVerticle(inputManager);
        final DeploymentOptions deploymentOptions = new DeploymentOptions();

        Vertx.vertx().deployVerticle(verticle, deploymentOptions, (deployHandler) -> {
            // The verticle has been deployed
            if (deployHandler.succeeded()) {
                System.out.println("The verticle has been successfully deployed, with id: " + deployHandler.result());
            } else {
                printCauseAndExit(deployHandler.cause());
            }
        });
    }

    private static void printCauseAndExit(Throwable throwable) {
        System.out.println("Cause: " + throwable.getMessage());
        System.exit(0);
    }

}
