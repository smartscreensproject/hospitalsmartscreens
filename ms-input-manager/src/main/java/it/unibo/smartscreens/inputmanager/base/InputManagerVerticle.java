package it.unibo.smartscreens.inputmanager.base;

import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.ext.web.handler.LoggerHandler;
import it.unibo.smartscreens.tac.base.TacVerticle;
import it.unibo.smartscreens.common.apiclient.impl.SmartScreenApiClientImpl;
import it.unibo.smartscreens.common.apiclient.impl.TacApiClientImpl;
import it.unibo.smartscreens.common.apiclient.impl.VideoStreamsApiClientImpl;
import it.unibo.smartscreens.common.apiclient.impl.VitalSignsApiClientImpl;
import it.unibo.smartscreens.common.apiclient.interfaces.DataManagerApiClient;
import it.unibo.smartscreens.common.apiclient.interfaces.SmartScreenApiClient;
import it.unibo.smartscreens.common.enums.SourceType;
import it.unibo.smartscreens.common.model.*;
import it.unibo.smartscreens.common.model.registry.SmartScreenEntity;
import it.unibo.smartscreens.common.msroute.InputManagerRoute;
import it.unibo.smartscreens.common.msroute.SmartScreenRoute;
import it.unibo.smartscreens.common.utils.AvailablePortsScanner;
import it.unibo.smartscreens.common.verticles.BaseVerticle;
import it.unibo.smartscreens.inputmanager.model.DataManagerInfo;
import it.unibo.smartscreens.videostreams.manager.VideoStreamsManagerVerticle;
import it.unibo.smartscreens.vitalsigns.manager.VitalSignsManagerVerticle;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Verticle representing the InputManager microservice
 */
public class InputManagerVerticle extends BaseVerticle {

    private final String INPUT_ID_REF = "inputId";
    private final String COMPONENT_ID_REF = "componentId";
    private final String ACTION_ID_REF = "actionId";

    private final InputManager inputManager;

    private WebClient webClient;
    // A map between Component instances and their api clients
    private Map<String, DataManagerInfo> componentDataManagers;

    // References to the SmartScreen microservice
    private SmartScreenEntity smartScreenEntity;
    private SmartScreenApiClient smartScreenApiClient;

    public InputManagerVerticle(final InputManager inputManager) {
        this.inputManager = inputManager;
        this.componentDataManagers = new HashMap<>();
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        println("InputManager started");

        JsonObject config = config();
        boolean isVerbose = config.getBoolean("verbose", true);

        // Creating the webclient
        webClient = WebClient.create(vertx);

        // Creating the router
        Router router = createRouter(isVerbose);

        // Creating the server, using the router, without specifying the address
        vertx.createHttpServer(
            new HttpServerOptions()
                .setLogActivity(true)
        )
            .requestHandler(router)
            .listen(inputManager.getPort(), listenResponse -> {
                if (listenResponse.succeeded()) {
                    println("Verticle deployed successfully, try typing localhost: " + listenResponse.result().actualPort());
                    // Declare the registry client, in order to register itself and retrieve the smart screen microservice info
                    // Retrieve the smart screen
                    getRegistryApiClient().getSmartScreens(Collections.singletonList(inputManager.getRoom()), Collections.singletonList(inputManager.getDeviceId()))
                        .compose((smartScreenRes) -> {
                            Optional<SmartScreenEntity> smartScreenRef = smartScreenRes.stream().findFirst();
                            if (smartScreenRef.isPresent()) {
                                // No smart screen found
                                smartScreenEntity = smartScreenRef.get();
                                println(String.format("Found Smart Screen at room [%s] and deviceId [%s]", inputManager.getRoom(), inputManager.getDeviceId()));
                                println("Smart Screen at " + smartScreenEntity.getServiceAddress());

                                // Create SmartScreen microservice client
                                smartScreenApiClient = new SmartScreenApiClientImpl(smartScreenEntity.getServiceAddress(), vertx);

                                // Register ourselves and return the future
                                return getRegistryApiClient().postInputManagers(inputManager.getPort(), inputManager.getRoom(), inputManager.getDeviceId());
                            }

                            // No smart screen found
                            return Future.failedFuture(String.format("No Smart Screen service found at room [%s] and deviceId [%s]", inputManager.getRoom(), inputManager.getDeviceId()));
                        })
                        .compose((inputManagerEntity) -> {
                            // Registered the input manager successfully and update the info
                            if (inputManagerEntity != null) {
                                inputManager.setId(inputManagerEntity.getId());
                                inputManager.setAddress(inputManagerEntity.getServiceAddress().getAddress());
                                inputManager.setPort(inputManagerEntity.getServiceAddress().getPort());
                                println(String.format("Registered InputManager with id [%s] at room [%s] and deviceId [%s]", inputManager.getId(), inputManager.getRoom(), inputManager.getDeviceId()));
                                println(String.format("InputManager at address [%s] and port [%s]", inputManager.getAddress(), inputManager.getPort()));

                                // Chain with smart screen state request
                                return smartScreenApiClient.getState();
                            }

                            return Future.failedFuture(String.format("InputManager found at room [%s] and deviceId [%s] could not register to the Registry", inputManager.getRoom(), inputManager.getDeviceId()));

                        })
                        .setHandler((smartScreenResult) -> {
                            if (smartScreenResult.succeeded()) {
                                // We retrieved the smart screen state
                                SmartScreen smartScreen = smartScreenResult.result();
                                println("SmartScreen retrieved: " + smartScreen.toString());
                                if (isSmartScreenValid(smartScreen)) {
                                    println("InputManager correctly configured from input Smart Screen");
                                    startFuture.complete();
                                } else {
                                    // The input smart screen is not valid
                                    startFuture.tryFail(String.format("SmartScreen found at room [%s] and deviceId [%s] is not valid for the current InputManager", inputManager.getRoom(), inputManager.getDeviceId()));
                                }
                            } else {
                                // Could not retrieve the smart screen
                                println(smartScreenResult.cause().getMessage());
                                startFuture.tryFail(smartScreenResult.cause());
                            }
                        });
                } else {
                    println("Error in http server: " + listenResponse.cause().getMessage());
                    startFuture.tryFail(listenResponse.cause());
                }
            });
    }

    private boolean isSmartScreenValid(final SmartScreen smartScreen) {
        if (smartScreen == null) {
            println("Input Smart Screen is null");
            return false;
        }
        String deviceId = smartScreen.getDeviceIdInRoom();
        if (deviceId == null || !deviceId.equals(inputManager.getDeviceId())){
            println("Input Smart Screen deviceId is not equal to the InputManager deviceId");
            return false;
        }
        String roomId = smartScreen.getRoom();
        if (roomId == null || !roomId.equals(inputManager.getRoom())){
            println("Input Smart Screen roomId is not equal to the InputManager roomId");
            return false;
        }
        return true;
    }

    @Override
    public void stop(Future<Void> stopFuture) throws Exception {
        println("InputManager stopped");

        stopFuture.complete();
    }

    private Router createRouter(boolean verbose) {
        // Creating the router
        Router router = initRouter(InputManagerRoute.API_ROOT, verbose);
        router.route().handler(LoggerHandler.create());
        router.route(formatServerPath(InputManagerRoute.API_INPUT_ID, INPUT_ID_REF) + "/*").handler(this::handleAnyInputManagerRoute);
        // Input manager related routes
        router.post(formatServerPath(InputManagerRoute.API_INPUT_CHANGE_GRID, INPUT_ID_REF)).handler(this::handleChangeGrid);
        router.post(formatServerPath(InputManagerRoute.API_INPUT_SAVE_PROFILE, INPUT_ID_REF)).handler(this::handleSaveProfile);
        router.post(formatServerPath(InputManagerRoute.API_INPUT_LOAD_PROFILE, INPUT_ID_REF)).handler(this::handleLoadProfile);
        router.delete(formatServerPath(InputManagerRoute.API_INPUT_CLEAR, INPUT_ID_REF)).handler(this::handleClear);
        router.get(formatServerPath(InputManagerRoute.API_INPUT_ACTIONS, INPUT_ID_REF)).handler(this::handleRetrieveInputManagerActions);
        router.get(formatServerPath(InputManagerRoute.API_INPUT_INPUT_STATE, INPUT_ID_REF)).handler(this::handleRetrieveInputState);
        // Components related routes
        //router.get(formatServerPath(InputManagerRoute.API_INPUT_COMPONENTS, INPUT_ID_REF)).handler(this::handleRetrieveComponents);
        router.post(formatServerPath(InputManagerRoute.API_INPUT_COMPONENTS, INPUT_ID_REF)).handler(this::handleAddComponent);
        router.put(formatServerPath(InputManagerRoute.API_COMPONENT_ID, INPUT_ID_REF, COMPONENT_ID_REF)).handler(this::handleUpdateComponent);
        router.delete(formatServerPath(InputManagerRoute.API_COMPONENT_ID, INPUT_ID_REF, COMPONENT_ID_REF)).handler(this::handleRemoveComponent);
        router.get(formatServerPath(InputManagerRoute.API_COMPONENT_ACTIONS, INPUT_ID_REF, COMPONENT_ID_REF)).handler(this::handleRetrieveComponentActions);
        router.post(formatServerPath(InputManagerRoute.API_ACTIONS_ID, INPUT_ID_REF, COMPONENT_ID_REF, ACTION_ID_REF)).handler(this::handleReceiveAction);
        return router;
    }

    private void handleAnyInputManagerRoute(RoutingContext rc) {
        String inputId = rc.pathParam(INPUT_ID_REF);
        if (inputId == null || inputId.isEmpty()) {
            rc.response().setStatusCode(400).setStatusMessage("The InputManager id is empty").end();
            return;
        }
        if (!inputManager.getId().equals(inputId)) {
            rc.response().setStatusCode(400).setStatusMessage("The InputManager id is incorrect").end();
            return;
        }
        // Use the next handler
        rc.next();
    }

    private void handleChangeGrid(RoutingContext rc) {
        @Nullable JsonObject body = rc.getBodyAsJson();
        if (body == null) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        try {
            Grid grid = body.mapTo(Grid.class);
            if (grid != null) {
                smartScreenApiClient.changeGrid(grid).setHandler(res -> {
                    if (res.succeeded()) {
                        println("Successfully retrieved the new grid");

                        Profile profile = res.result();

                        // Check if some component has been removed
                        List<String> profileComponents = profile.getComponents().stream().map(Component::getComponentId).collect(Collectors.toList());
                        List<String> deletedComponentsIds = componentDataManagers.keySet().stream().filter(id -> !profileComponents.contains(id)).collect(Collectors.toList());

                        // Remove the data manager for the deleted data managers
                        deletedComponentsIds.forEach(this::removeAndDestroyDataManager);

                        profile.getComponents().stream()
                            .filter(c -> componentDataManagers.keySet().contains(c.getComponentId()))
                            .forEach(c -> componentDataManagers.get(c.getComponentId()).getComponentWithActions().getComponent().setPositions(c.getPositions()));

                        InputConfiguration inputConfiguration = new InputConfiguration();
                        inputConfiguration.setGrid(profile.getGrid());
                        List<ComponentWithActions> componentWithActions = updateComponentWithActions(profile.getComponents());

                        inputConfiguration.setComponentsWithActions(componentWithActions);

                        rc.response().end(Json.encodePrettily(inputConfiguration));
                    } else {
                        rc.response().setStatusCode(400).setStatusMessage(res.cause().getMessage()).end();
                    }
                });
            } else {
                rc.response().setStatusCode(400).setStatusMessage("The grid is empty").end();
            }
        } catch (IllegalArgumentException e) {
            rc.response().setStatusCode(400).setStatusMessage("Incompatible grid").end();
        }
    }

    private void handleSaveProfile(RoutingContext rc) {
        @Nullable JsonObject body = rc.getBodyAsJson();
        if (body == null || !body.containsKey("name")) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        String name = body.getString("name");
        if (name.isEmpty()) {
            rc.response().setStatusCode(400).setStatusMessage("New profile name cannot be empty").end();
            return;
        }

        //TODO: retrieve the current configuration and create a Profile instance
        throw new UnsupportedOperationException();
    }

    private void handleLoadProfile(RoutingContext rc) {
        @Nullable JsonObject body = rc.getBodyAsJson();
        if (body == null || !body.containsKey("id")) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        String id = body.getString("id");
        if (id.isEmpty()) {
            rc.response().setStatusCode(400).setStatusMessage("Cannot load a profile without its id").end();
            return;
        }

        //TODO: retrieve the current configuration and send it to the Smart Screen
        throw new UnsupportedOperationException();
    }

    private void handleClear(RoutingContext rc) {
        // Delete the smart screen configuration
        smartScreenApiClient.clearScreen().setHandler(clearRes -> {
            if (clearRes.succeeded()) {
                List<Component> currentSmartScreenComponents = clearRes.result();

                // Clean the dataManager map and destroy the clients
                removeAndDestroyAllDataManagers();

                rc.response().end(Json.encodePrettily(currentSmartScreenComponents));
            } else {
                rc.response().setStatusCode(400).setStatusMessage(clearRes.cause().getMessage()).end();
            }
        });
    }

    private void handleRetrieveInputState(RoutingContext rc) {
        smartScreenApiClient.getProfile().setHandler(handler -> {
            if (handler.succeeded()) {
                // Retrieve the input state
                Profile profile = handler.result();
                InputConfiguration inputConfiguration = profile.toInputConfiguration();
                // Update the component with the related actions
                List<ComponentWithActions> componentWithActions = updateComponentWithActions(profile.getComponents());
                inputConfiguration.setComponentsWithActions(componentWithActions);

                rc.response().end(Json.encodePrettily(inputConfiguration));
            } else {
                rc.response().setStatusCode(400).setStatusMessage(handler.cause().getMessage()).end();
            }
        });
    }

    /*
    private void handleRetrieveComponents(RoutingContext rc) {
        //TODO: discuss implementation
        throw new UnsupportedOperationException();
    }
    */

    private void handleAddComponent(RoutingContext rc) {
        @Nullable JsonObject body = rc.getBodyAsJson();
        if (body == null || !body.containsKey("positions") || !body.containsKey("sourceType")) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        // Retrieve the info
        String sourceTypeParam = body.getString("sourceType");
        Optional<SourceType> sourceTypeOpt = SourceType.getSourceTypeFromName(sourceTypeParam);
        if (sourceTypeParam.isEmpty() || !sourceTypeOpt.isPresent()) {
            rc.response().setStatusCode(404).setStatusMessage("Cannot find a component with the requested sourcetype").end();
            return;
        }
        try {
            SourceType sourceType = sourceTypeOpt.get();
            // Retrieve the component
            Component component = rc.getBodyAsJson().mapTo(Component.class);

            // Add the component to the smart screen
            smartScreenApiClient.addComponent(component).setHandler(addComponentHandler -> {
                if (addComponentHandler.succeeded()) {
                    println(String.format("New component added to SmartScreen, with sourceType [%s]", sourceType.getName()));

                    Component addedComponent = addComponentHandler.result();

                    // Initialize the Component Data Manager
                    initializeComponentDataManager(sourceType, addedComponent).setHandler((initActions) -> {
                        if (initActions.succeeded()) {
                            println(String.format("New component with sourceType [%s] initialized", sourceType.getName()));

                            // Retrieve the actions we will send to the input client
                            List<Action> retrievedActions = initActions.result();

                            // Retrieve the component we just created
                            DataManagerInfo dataManagerInfo = componentDataManagers.get(addedComponent.getComponentId());
                            if (dataManagerInfo == null || dataManagerInfo.getComponentWithActions() == null) {
                                println("The component data manager could not be initialised properly");
                                rc.response().setStatusCode(400).setStatusMessage("The component data manager could not be initialised properly").end();
                                return;
                            }

                            // Respond with the component and its actions
                            rc.response().end(Json.encodePrettily(dataManagerInfo.getComponentWithActions()));
                        } else {
                            println("Cause: " + initActions.cause().getMessage());

                            // The init has failed, we must delete the smartscreen component
                            smartScreenApiClient.removeComponent(addedComponent.getComponentId());

                            // Remove the component manager from the cache (if present)
                            removeAndDestroyDataManager(addedComponent.getComponentId());

                            rc.response().setStatusCode(500).setStatusMessage(initActions.cause().getMessage()).end();
                        }
                    });
                } else {
                    println("Cause: " + addComponentHandler.cause().getMessage());
                    rc.response().setStatusCode(400).setStatusMessage(addComponentHandler.cause().getMessage()).end();
                }
            });

        } catch (IllegalArgumentException e) {
            rc.response().setStatusCode(400).setStatusMessage("Incompatible component").end();
        } catch (Exception ex) {
            rc.response().setStatusCode(500).setStatusMessage(ex.getMessage()).end();
        }
    }

    private void handleRetrieveInputManagerActions(RoutingContext rc) {
        //TODO: discuss implementation
        throw new UnsupportedOperationException();
    }

    private void handleUpdateComponent(RoutingContext rc) {
        @Nullable JsonObject body = rc.getBodyAsJson();
        if (body == null || !body.containsKey("positions")) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        String componentId = rc.pathParam(COMPONENT_ID_REF);
        if (componentId.isEmpty()) {
            rc.response().setStatusCode(400).setStatusMessage("Cannot find a component with the requested id").end();
            return;
        }
        try {
            // Check if the component exists
            Component sentComponent = body.mapTo(Component.class);

            // Send the new component to the smart screen
            smartScreenApiClient.updateComponent(sentComponent).setHandler(updateHandler -> {
                if (updateHandler.succeeded()) {
                    // Retrieve the update component info and respond to the client. No actions are currently added
                    UpdateComponentResponse updateComponentResponse = updateHandler.result();

                    // Update the cache with the updated component
                    DataManagerInfo dataManagerInfo = componentDataManagers.get(updateComponentResponse.getComponentUpdated().getComponentId());
                    if (dataManagerInfo != null) {
                        dataManagerInfo.getComponentWithActions().setComponent(updateComponentResponse.getComponentUpdated());
                    }

                    rc.response().end(Json.encodePrettily(updateComponentResponse));
                } else {
                    println("Cause: " + updateHandler.cause().getMessage());
                    rc.response().setStatusCode(400).setStatusMessage(updateHandler.cause().getMessage()).end();
                }
            });

        } catch (IllegalArgumentException e) {
            rc.response().setStatusCode(400).setStatusMessage("Incompatible component").end();
        }
    }

    private void handleRemoveComponent(RoutingContext rc) {
        String componentId = rc.pathParam(COMPONENT_ID_REF);
        if (componentId.isEmpty()) {
            rc.response().setStatusCode(400).setStatusMessage("Cannot find a component with the requested id").end();
            return;
        }

        // We found the component, we remove it from the configuration
        smartScreenApiClient.removeComponent(componentId).setHandler(deleteHandler -> {
            if (deleteHandler.succeeded()) {
                List<Component> currentSmartScreenComponents = deleteHandler.result();

                // Remove the component manager from the cache
                removeAndDestroyDataManager(componentId);

                rc.response().end(Json.encodePrettily(currentSmartScreenComponents));
            } else {
                rc.response().setStatusCode(400).setStatusMessage(deleteHandler.cause().getMessage()).end();
            }
        });
    }

    private void handleRetrieveComponentActions(RoutingContext rc) {
        String componentId = rc.pathParam(COMPONENT_ID_REF);
        if (componentId.isEmpty()) {
            rc.response().setStatusCode(400).setStatusMessage("Cannot find a component with the requested id").end();
            return;
        }

        // Don't cache the actions, we retrieve the new one
        if (componentDataManagers.containsKey(componentId)) {
            DataManagerInfo info = componentDataManagers.get(componentId);
            DataManagerApiClient client = info.getDataManagerApiClient();
            // Retrieving actions
            client.getActions().setHandler(res -> {
                if (res.succeeded()) {
                    // Retrieve the actions and update the cache
                    List<Action> actions = res.result();
                    info.getComponentWithActions().setActions(actions);

                    // Respond with the actions
                    rc.response().end(Json.encodePrettily(actions));
                } else {
                    // Could not retrieve the actions
                    rc.response().setStatusCode(400).setStatusMessage(res.cause().getMessage()).end();
                }
            });
        } else {
            // We failed to find the right component
            rc.response().setStatusCode(400).setStatusMessage("Cannot retrieve the component actions with the requested id").end();
        }
    }

    private void handleReceiveAction(RoutingContext rc) {
        // Check if the data is well composed
        @Nullable JsonObject body = rc.getBodyAsJson();
        if (body == null) {
            rc.response().setStatusCode(400).setStatusMessage("Incorrect params").end();
            return;
        }
        String componentId = rc.pathParam(COMPONENT_ID_REF);
        if (componentId.isEmpty()) {
            rc.response().setStatusCode(404).setStatusMessage("Cannot find a component with the requested id").end();
            return;
        }
        String actionId = rc.pathParam(ACTION_ID_REF);
        if (actionId.isEmpty()) {
            rc.response().setStatusCode(404).setStatusMessage("Cannot find an action with the requested id").end();
            return;
        }
        // Retrieve the data received by a input client and forward the request to the right microservice
        try {
            Action action = body.mapTo(Action.class);
            forwardReceivedAction(action, componentId).setHandler(forwardRes -> {
                if (forwardRes.succeeded()) {
                    List<Action> newActions = forwardRes.result();

                    // Updating the actions in the cache
                    DataManagerInfo dataManagerInfo = componentDataManagers.get(componentId);
                    if (dataManagerInfo != null) {
                        dataManagerInfo.getComponentWithActions().setActions(newActions);
                    }

                    // Forward success
                    rc.response().end(Json.encode(newActions));
                } else {
                    // Forward fail
                    rc.response().setStatusCode(400).setStatusMessage(forwardRes.cause().getMessage()).end();
                }
            });
        } catch (IllegalArgumentException e) {
            rc.response().setStatusCode(400).setStatusMessage("Incompatible action").end();
        }
    }

    private Future<List<Action>> initializeComponentDataManager(final SourceType sourceType, final Component component) {
        Future<List<Action>> actionsFuture = Future.future();

        String smartScreenAddress = smartScreenEntity.getServiceAddress().getAddress();
        int smartScreenPort = smartScreenEntity.getServiceAddress().getPort();

        // Set the component params for the clients
        ComponentInitParams microserviceInitParam = new ComponentInitParams(inputManager.getId(),
            component.getComponentId(), component.getPatientId(),
            new ServiceRoute(smartScreenAddress, smartScreenPort, SmartScreenRoute.API_UPDATE_DATA),
            new ServiceRoute(smartScreenAddress, smartScreenPort, SmartScreenRoute.API_SOURCE_SOCKET.replace("/*", "/websocket")));

        // Find a valid free port on the same machine
        AvailablePortsScanner availablePortsScanner = new AvailablePortsScanner();
        String microServiceAddress = inputManager.getAddress();
        int microServicePort = availablePortsScanner.findFreePort(8010);

        DeploymentOptions microServiceOptions = new DeploymentOptions().setConfig(
            new JsonObject().put(ADDRESS_REF, microServiceAddress)
                            .put(PORT_REF, microServicePort)
                            .put(VERBOSE_REF, true)
        );

        ServiceAddress serviceAddress = new ServiceAddress(microServiceAddress, microServicePort);

        // Using a common interface for any Component Data Manager microservice
        Verticle dataComponentVerticle;
        DataManagerApiClient dataComponentClient;

        // Creating the correct Verticle and api client
        switch (sourceType) {
            case TAC:
                dataComponentVerticle = new TacVerticle();
                dataComponentClient = new TacApiClientImpl(serviceAddress, vertx);
                break;
            case VITAL_SIGNS:
                dataComponentVerticle = new VitalSignsManagerVerticle();
                dataComponentClient = new VitalSignsApiClientImpl(serviceAddress, vertx);
                break;
            case VIDEO_STREAMS:
                dataComponentVerticle = new VideoStreamsManagerVerticle();
                dataComponentClient = new VideoStreamsApiClientImpl(serviceAddress, vertx);
                break;
            default:
                actionsFuture.fail(String.format("Could not find the proper microservice for the requested sourceType [%s]", sourceType));
                return actionsFuture;
        }

        DataManagerApiClient finalDataComponentClient = dataComponentClient;

        // Deploy the verticle
        vertx.deployVerticle(dataComponentVerticle, microServiceOptions, (deployHandler) -> {
            if (deployHandler.succeeded()) {
                // The verticle has been deployed
                println(String.format("Deployed [%s] type verticle with id [%s]", sourceType, deployHandler.result()));
                // Init the microservice
                finalDataComponentClient.init(microserviceInitParam).setHandler((initResponse) -> {
                    if (initResponse.succeeded()) {
                        // The microservice has been initialised, we retrieve the actions
                        List<Action> actions = initResponse.result();
                        ComponentWithActions componentWithActions = new ComponentWithActions(component, actions);

                        // Add the client to the managers list
                        componentDataManagers.put(component.getComponentId(), DataManagerInfo.create(componentWithActions, finalDataComponentClient, serviceAddress));

                        // Complete with the actions
                        actionsFuture.complete(actions);
                    } else {
                        actionsFuture.tryFail(initResponse.cause());
                    }
                });
            } else {
                actionsFuture.fail(deployHandler.cause());
            }
        });

        return actionsFuture;
    }

    private Future<List<Action>> forwardReceivedAction(final Action action, final String componentId) {
        Future<List<Action>> future = Future.future();

        // Check if the component client exists
        DataManagerInfo dataManagerInfo = componentDataManagers.get(componentId);
        if (dataManagerInfo == null) {
            future.fail(new Throwable(String.format("Could not find the request data manager client with id [%s]", componentId)));
            return future;
        }
        ServiceAddress serviceAddress = dataManagerInfo.getDataManagerServiceAddress();

        // Request handler
        Handler<AsyncResult<HttpResponse<Buffer>>> requestHandler = event -> {
            if (event.succeeded()) {
                // Retrieve the actions
                try {
                    JsonArray jsonArray = event.result().bodyAsJsonArray();
                    List<Action> actions = convertJsonArray(jsonArray, Action.class);
                    future.complete(actions);
                } catch (Exception e) {
                    println(e.getCause().getMessage());
                    future.fail("The microservice has responded with malformed actions");
                }
            } else {
                future.fail(event.cause());
            }
        };
        String method = action.getMethod().toLowerCase();
        String href = action.getHref();
        List<ActionParam> actionParams = action.getParams();
        if (method.equalsIgnoreCase("get")) {
            // Get method, we add any param we found to the query
            HttpRequest<Buffer> requestBuffer = webClient
                .get(serviceAddress.getPort(), serviceAddress.getAddress(), href)
                .expect(ResponsePredicate.SC_SUCCESS);
            if (actionParams != null && !actionParams.isEmpty()) {
                for (ActionParam actionParam : actionParams) {
                    requestBuffer.addQueryParam(actionParam.getName(), actionParam.getValue());
                }
            }
            requestBuffer.send(requestHandler);
        } else if (method.equalsIgnoreCase("post")){
            // Post method, we add any param we found in the body
            HttpRequest<Buffer> requestBuffer = webClient
                .post(serviceAddress.getPort(), serviceAddress.getAddress(), href)
                .expect(ResponsePredicate.SC_SUCCESS);
            JsonObject requestBody = new JsonObject();
            if (actionParams != null && !actionParams.isEmpty()) {
                for (ActionParam actionParam : actionParams) {
                    requestBody.put(actionParam.getName(), actionParam.getValue());
                }
            }
            requestBuffer.sendJsonObject(requestBody, requestHandler);
        }
        return future;
    }

    private void removeAndDestroyDataManager(final String componentId) {
        if (componentDataManagers.containsKey(componentId)) {
            DataManagerInfo dataManagerInfo = componentDataManagers.get(componentId);
            DataManagerApiClient dataManagerApiClient = dataManagerInfo.getDataManagerApiClient();
            if (dataManagerApiClient != null) {
                dataManagerApiClient.destroy();
            }
            componentDataManagers.remove(componentId);
        }
    }

    private void removeAndDestroyAllDataManagers() {
        componentDataManagers.values().forEach(info -> info.getDataManagerApiClient().destroy());
        componentDataManagers.clear();
    }

    private List<ComponentWithActions> updateComponentWithActions(List<Component> components) {
        return components.stream().map((e) -> {
            if (componentDataManagers.containsKey(e.getComponentId())) {
                // The component exists in our cache, we retrieve it and retrieve the actions
                return componentDataManagers.get(e.getComponentId()).getComponentWithActions();
            } else {
                // The component dows not exist in our cache, we just wrap it
                return new ComponentWithActions(e, Collections.emptyList());
            }
        }).collect(Collectors.toList());
    }
}
