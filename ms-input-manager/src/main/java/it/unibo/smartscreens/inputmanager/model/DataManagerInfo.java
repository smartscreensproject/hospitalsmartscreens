package it.unibo.smartscreens.inputmanager.model;

import it.unibo.smartscreens.common.apiclient.interfaces.DataManagerApiClient;
import it.unibo.smartscreens.common.model.ComponentWithActions;
import it.unibo.smartscreens.common.model.ServiceAddress;

public class DataManagerInfo {

    private final ComponentWithActions componentWithActions;
    private final DataManagerApiClient dataManagerApiClient;
    private final ServiceAddress dataManagerServiceAddress;

    public DataManagerInfo(final ComponentWithActions componentWithActions, final DataManagerApiClient dataManagerApiClient, final ServiceAddress dataManagerServiceAddress) {
        this.componentWithActions = componentWithActions;
        this.dataManagerApiClient = dataManagerApiClient;
        this.dataManagerServiceAddress = dataManagerServiceAddress;
    }

    public static DataManagerInfo create(final ComponentWithActions componentWithActions, final DataManagerApiClient dataManagerApiClient, final ServiceAddress dataManagerServiceAddress) {
        return new DataManagerInfo(componentWithActions, dataManagerApiClient, dataManagerServiceAddress);
    }

    public ComponentWithActions getComponentWithActions() {
        return componentWithActions;
    }

    public DataManagerApiClient getDataManagerApiClient() {
        return dataManagerApiClient;
    }

    public ServiceAddress getDataManagerServiceAddress() {
        return dataManagerServiceAddress;
    }
}
